package com.epam.newsmanagement.service;

import java.util.List;


import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;



public interface TagService {
	/**
	 * 
	 * @param tagId tag id to link the news
	 * @param newsId news id to link the tag
	 * @throws ServiceException
	 */
	public void addTagToNews(Long tagId, Long newsId) throws ServiceException;
	/**
	 * 
	 * @param tagIds tag ids to link the news
	 * @param newsId news id to link the tags
	 * @throws ServiceException
	 */
	public void addTagsToNews(List<Long> tagIds, Long newsId) throws ServiceException;
	/**
	 * 
	 * @param tagId tag id to delete from news
	 * @param newsId news id to delete from tag
	 * @throws ServiceException
	 */
	public void deleteTagFromNews (Long tagId, Long newsId) throws ServiceException;
	/**
	 * 
	 * @param tagIds tag ids to delete from news
	 * @param newsId news id to delete from tags
	 * @throws ServiceException
	 */
	public void deleteTagsFromNews(List<Long> tagIds, Long newsId) throws ServiceException;
	
	public List<Tag> getTagsList() throws ServiceException;

}
