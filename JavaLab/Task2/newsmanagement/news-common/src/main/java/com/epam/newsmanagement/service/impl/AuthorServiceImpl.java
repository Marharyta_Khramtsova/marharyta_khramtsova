package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.PersistanceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;


public class AuthorServiceImpl implements AuthorService{
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	AuthorDAO authorDAO = null;
	
	
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	public AuthorServiceImpl(AuthorDAO authorDAO) {
		
		this.authorDAO = authorDAO;
	}
	
	public void addAuthor(Author author) throws ServiceException {
		try {
			authorDAO.insert(author);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot add author!", e);
		}
		
	}

	public void expireAuthor(Author author) throws ServiceException{
		try {
			authorDAO.update(author);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot expire author!", e);
		}		
	}

	public List<Author> getAuthorsList() throws ServiceException{
		try {
			return authorDAO.getAll();
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot get authors list!", e);
		}
	}
	
	

}
