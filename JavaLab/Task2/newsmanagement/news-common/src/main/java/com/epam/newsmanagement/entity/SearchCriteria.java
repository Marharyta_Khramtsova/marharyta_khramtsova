package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SearchCriteria implements Serializable{
	
	private Long authorId;
	
	private List<Long> tagIds = null;
	
	public SearchCriteria() {
		tagIds = new ArrayList<Long>();
	}	
	
	public SearchCriteria(Long authorId, List<Long> tagIds) {
		
		this.authorId = authorId;
		this.tagIds = tagIds;
	}	

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}
	
	public List<Long> getTagIds() {
		return tagIds;
	}

}
