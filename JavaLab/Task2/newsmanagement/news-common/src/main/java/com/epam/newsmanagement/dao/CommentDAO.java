package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.PersistanceException;

public interface CommentDAO extends GeneralDAO<Comment> {
	
	/**
	 * 
	 * @param newsId id of the news to get number of comments
	 * @return count of comments to the news
	 * @throws PersistanceException
	 */
	public int getNumberOfComments(Long newsId) throws PersistanceException;
	/**
	 * 
	 * @param newsId id of the news
	 * @return list of the coments to the news
	 * @throws PersistanceException
	 */
	public List<Comment> getCommentsToNews(Long newsId) throws PersistanceException;

}
