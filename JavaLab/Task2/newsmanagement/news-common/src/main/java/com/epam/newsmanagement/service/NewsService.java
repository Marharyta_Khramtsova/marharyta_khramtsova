package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.CombinedItem;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;



public interface NewsService {
	/**
	 * 
	 * @param news news to edit
	 * @throws ServiceException
	 */
	public void editNews (News news) throws ServiceException;
	/**
	 * 
	 * @param newsId id of the news to delete
	 * @throws ServiceException
	 */
	public void deleteNews (Long newsId) throws ServiceException;
	/**
	 * 
	 * @return list of the news
	 * @throws ServiceException
	 */
	public List<CombinedItem> getNewsList() throws ServiceException;
	/**
	 * 
	 * @param newsId id of the news to get
	 * @return news by id
	 * @throws ServiceException
	 */
	public CombinedItem getNews (Long newsId) throws ServiceException;
	/**
	 * 
	 * @return count of all news
	 * @throws ServiceException
	 */
	public int countNews() throws ServiceException;
	/**
	 * 
	 * @param combinedItem {@link CombinedItem} object to add
	 * @throws ServiceException
	 */
	public void addNews(CombinedItem combinedItem) throws ServiceException;
	/**
	 * 
	 * @param criteria {@link SearchCriteria} object for finding news
	 * @return list of the news that respond search criteria
	 * @throws ServiceException
	 */
	public List<CombinedItem> findNews(SearchCriteria criteria) throws ServiceException;
	
}