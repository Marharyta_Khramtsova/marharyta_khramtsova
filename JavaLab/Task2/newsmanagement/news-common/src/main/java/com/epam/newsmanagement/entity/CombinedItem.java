package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CombinedItem implements Serializable{
	
	private News news;
	private Author author;
	private List<Tag> tags;
	private List<Comment> comments;
	
	public CombinedItem() {
		comments = new ArrayList<Comment>();
	}	

	public CombinedItem(News news, Author author, List<Tag> tags, List<Comment> comments) {
		super();
		this.news = news;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}	

	public List<Tag> getTags() {
		return tags;
	}

	public void setTagIds(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	public List<Long> getTagIds() {
		List<Long> ids = new ArrayList<Long>();
		for (Tag tag : tags) {
			ids.add(tag.getId());
		}
		return ids;
	}

}
