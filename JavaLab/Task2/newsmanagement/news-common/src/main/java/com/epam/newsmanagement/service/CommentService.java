package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;



public interface CommentService {
	/**
	 * 
	 * @param comment comment to add
	 * @throws ServiceException
	 */
	public void addComment(Comment comment) throws ServiceException;
	/**
	 * 
	 * @param comments to add
	 * @throws ServiceException
	 */
	public void addComments(List<Comment> comments) throws ServiceException;
	/**
	 * 
	 * @param comment comment to delete
	 * @throws ServiceException
	 */
	public void deleteComment(Comment comment) throws ServiceException;
	/**
	 * 
	 * @param comments comments to delete
	 * @throws ServiceException
	 */
	public void deleteComments(List<Comment> comments) throws ServiceException;
	
	public int getCommentsCount(Long newsId) throws ServiceException;

}
