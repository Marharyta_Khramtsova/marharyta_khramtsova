package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.PersistanceException;

public interface NewsDAO extends GeneralDAO<News>{
	
	/**
	 * 
	 * @return
	 * @throws PersistanceException
	 */
	
	public int countNews() throws PersistanceException;	
	/**
	 * 
	 * @param authorId id of author
	 * @return
	 * @throws PersistanceException
	 */
	public List<News> findNews(SearchCriteria criteria) throws PersistanceException;
	

}
