package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Author implements Serializable{
	
	private Long id;	
	
	private String name;
	
	private Timestamp expired;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getExpired() {
		return expired;
	}
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}
	
	public Author() {
		id = 0L;
	}
	
	public Author(Long id, String name) {
		this.id = id;
		this.name = name;
		this.expired = null;
	}
	
	public Author(Long id, String name, Timestamp expired) {
		this.id = id;
		this.name = name;
		this.expired = expired;
	}
	
	public Author(Long id, String name, String expired) throws ParseException {
		this.id = id;
		this.name = name;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date date = dateFormat.parse(expired);
        this.expired = new Timestamp(date.getTime());
	}
	
	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
}
