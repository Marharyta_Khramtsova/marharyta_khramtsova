package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

public class DAOUtilities {
	
	public static Connection getConnection(DataSource ds) throws SQLException {
		return DataSourceUtils.getConnection(ds);
    }
	
	public static void close(Connection con, DataSource ds, PreparedStatement ps) throws SQLException {
		DataSourceUtils.releaseConnection(con, ds);
		ps.close();
	}
	public static void close(Connection con, DataSource ds, PreparedStatement ps, ResultSet rs) throws SQLException {
		close(con, ds, ps);
		rs.close();
	}

}
