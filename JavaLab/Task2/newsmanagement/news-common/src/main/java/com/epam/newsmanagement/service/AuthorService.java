package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

public interface AuthorService {
	/**
	 * 
	 * @param author author to add
	 * @throws ServiceException
	 */
	public void addAuthor(Author author) throws ServiceException;
	/**
	 * 
	 * @param author author to expire
	 * @throws ServiceException
	 */
	public void expireAuthor(Author author) throws ServiceException;
	/**
	 * 
	 * @return list of authors
	 * @throws ServiceException
	 */
	public List<Author> getAuthorsList() throws ServiceException;

}
