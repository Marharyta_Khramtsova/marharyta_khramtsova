package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Comment implements Serializable{
	
	private Long id;
	
	private Long newsId;
	
	private String commentText;
	
	private Timestamp creationDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	public Comment() {
		id = 0L;
	}	

	public Comment(Long newsId, String commentText, Timestamp creationDate) {
		
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}
	
	public Comment(Long id, Long newsId, String commentText, String creationDate) throws ParseException {
		
		this.id = id;
		this.newsId = newsId;
		this.commentText = commentText;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date date = dateFormat.parse(creationDate);
        this.creationDate = new Timestamp(date.getTime());	
	}
	
	public Comment(Long newsId, String commentText, String creationDate) throws ParseException {
		
		this.newsId = newsId;
		this.commentText = commentText;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date date = dateFormat.parse(creationDate);
        this.creationDate = new Timestamp(date.getTime());	
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", newsId=" + newsId + ", commentText=" + commentText + ", creationDate="
				+ creationDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	
	
	

}
