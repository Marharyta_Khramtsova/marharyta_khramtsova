package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.PersistanceException;

public class TagDAOImpl extends GeneralDAOImpl implements TagDAO{
	
	private final static String INSERT_TAG = "insert into Tag (tag_id, tag_name) values (INC_TAG_ID.nextval,?)";
	private final static String GET_TAG_BY_ID = "select tag_name from Tag where tag_id = ?";
	private final static String UPDATE_TAG = "update Tag set tag_name = ? where tag_id = ?";
	private final static String DELETE_TAG = "delete from Tag where tag_id = ?";
	private final static String GET_ALL_TAGS = "select tag_id, tag_name from Tag";
	private final static String LINK_TAG_TO_NEWS = "insert into News_Tag (tag_id, news_id) values (?,?)";
	private final static String UNLINK_TAG_TO_NEWS = "delete from News_Tag where (tag_id = ? and news_id = ?)";
	private final static String GET_TAGS = "select Tag.tag_id, Tag.tag_name from (Tag inner join News_Tag on Tag.tag_id=News_Tag.tag_id) where News_Tag.news_id = ?";
	private final static String GET_ID_BY_NAME = "select tag_id from Tag where tag_name = ?";
	private final static String LINK_TAGS_TO_NEWS = "insert into News_Tag (tag_id, news_id)";
	private final static String UNLINK_TAGS_TO_NEWS = "delete from News_Tag where (tag_id = ? and news_id = ?)";
	
	public TagDAOImpl(DataSource dataSource) {
		super(dataSource);		
	}

	public Long insert(Tag tag) throws PersistanceException {		
		Connection con = null;
		PreparedStatement ps = null;
		Long generatedId = 0L;
		ResultSet generatedKeys = null;
		try {
			con = DAOUtilities.getConnection(dataSource);
			ps = con.prepareStatement(INSERT_TAG, new String[] { "tag_id" });
			ps.setString(1, tag.getName());
			ps.executeUpdate();

			generatedKeys = ps.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				generatedId = generatedKeys.getLong(1);
			}
		} catch (SQLException e) {
			throw (new PersistanceException("Cannot insert tag into database!", e));
		} finally {
			try {
				DAOUtilities.close(con, dataSource, ps, generatedKeys);
			} catch (SQLException e) {
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return generatedId;

	}

	public Tag getById(Long id) throws PersistanceException {
		 
	        Tag tag = null;
	        Connection con = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(GET_TAG_BY_ID);
	            ps.setLong(1, id);
	            rs = ps.executeQuery();
	            if(rs.next()){
	                tag = new Tag();
	                tag.setId(id);
	                tag.setName(rs.getString("tag_name"));	
	            }
	        }catch(SQLException e){
	        	throw (new PersistanceException("Cannot get tag by id from database!", e));
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps, rs);
	            } catch (SQLException e) {	        
	            	throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
	        return tag;
	}

	public void update(Tag tag) throws PersistanceException {
		    Connection con = null;
	        PreparedStatement ps = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(UPDATE_TAG);
	            ps.setString(1, tag.getName());
	            ps.setLong(2, tag.getId());
	            ps.executeUpdate();
	            
	        }catch(SQLException e){
	        	throw (new PersistanceException("Cannot update tag in database!", e));
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps);
	            } catch (SQLException e) {
	            	throw (new PersistanceException("Cannot close connection with database!", e));	            	
	            }
	        }
		
	}

	public void deleteById(Long id) throws PersistanceException {
		 
			Connection con = null;
	        PreparedStatement ps = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(DELETE_TAG);
	            ps.setLong(1, id);
	            ps.executeUpdate();
	        }catch(SQLException e){
	        	throw (new PersistanceException("Cannot delete tag by id from database!", e));
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps);
	            } catch (SQLException e) {
	            	throw (new PersistanceException("Cannot close connection with database!", e));
	            	
	            }
	        }
		
	}

	public List<Tag> getAll() throws PersistanceException {
		 	
	        List<Tag> tagList = new ArrayList<Tag>();
	        Connection con = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(GET_ALL_TAGS);
	            rs = ps.executeQuery();
	            while(rs.next()){
	                Tag tag = new Tag();
	                tag.setId(rs.getLong("tag_id"));
	                tag.setName(rs.getString("tag_name"));	               
	                tagList.add(tag);
	            }
	        }catch(SQLException e){
	        	throw (new PersistanceException("Cannot get all tags from database!", e));
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps, rs);
	            } catch (SQLException e) {
	            	throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
	        return tagList;
	}


	public void linkTagToNews(Long tagId, Long newsId) throws PersistanceException {
		
        Connection con = null;
        PreparedStatement ps = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(LINK_TAG_TO_NEWS);
            ps.setLong(1, tagId);
            ps.setLong(2, newsId);
            ps.executeUpdate();	           
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot link tag to news!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		
	}

	public void unlinkTagToNews(Long tagId, Long newsId) throws PersistanceException {
		 
	        Connection con = null;
	        PreparedStatement ps = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(UNLINK_TAG_TO_NEWS);
	            ps.setLong(1, tagId);
	            ps.setLong(2, newsId);
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	        	throw (new PersistanceException("Cannot unlink tag from news!", e));
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps);
	            } catch (SQLException e) {
	            	throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
		
	}
	
	
	public List<Tag> getTagsToNews(Long newsId) throws PersistanceException {
		
        List<Tag> tags = new ArrayList<Tag>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(GET_TAGS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            while(rs.next()){
            	Tag tag = new Tag(rs.getLong("tag_id"), rs.getString("tag_name"));
            	tags.add(tag);
            }
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot get tags by news_id!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, rs);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		return tags;
	} 
	
	public List<Long> getTagsIdsToNews(Long newsId) throws PersistanceException {
		
        List<Long> tagIds = new ArrayList<Long>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(GET_TAGS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            while(rs.next()){ 
            	tagIds.add(rs.getLong("tag_id"));
            }
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot get tags by news_id!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, rs);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		return tagIds;
	}

	public Long getIdByName(String tagName) throws PersistanceException {
		
		Long id = 0L;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(GET_ID_BY_NAME);
            ps.setString(1, tagName);
            rs = ps.executeQuery();
            if(rs.next()){                
                id = rs.getLong("tag_id");	
            }
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot get tag_id by tag_name!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return id;
	}

	public void linkTagsToNews(List<Long> tagIds, Long newsId) throws PersistanceException {
		StringBuilder stringBuilder = new StringBuilder(LINK_TAGS_TO_NEWS);
		if (!tagIds.isEmpty())
		{
			stringBuilder.append(" select ?, ? from dual");
			int size = tagIds.size()-1;
			while (size != 0)
			{
				stringBuilder.append(" union all select ?, ? from dual");
				size--;
			}
			String query = stringBuilder.toString();
			Connection con = null;
	        PreparedStatement ps = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(query);
	            for (int i = 1, j = 0; j < tagIds.size(); i += 2, j++)
	            {
	            	ps.setLong(i, tagIds.get(j));
	            	ps.setLong(i+1, newsId);
	            }	           
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	        	throw (new PersistanceException("Cannot link tags to news!", e));
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps);
	            } catch (SQLException e) {
	            	throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
		}
		
		
	}

	public void unlinkTagsToNews(List<Long> tagIds, Long newsId) throws PersistanceException {
		StringBuilder stringBuilder = new StringBuilder(UNLINK_TAGS_TO_NEWS);
		if (!tagIds.isEmpty())
		{			
			int size = tagIds.size()-1;
			while (size != 0)
			{
				stringBuilder.append(" or (tag_id = ? and news_id = ?)");
				size--;
			}
			String query = stringBuilder.toString();
			Connection con = null;
	        PreparedStatement ps = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(query);
	            for (int i = 1, j = 0; j < tagIds.size(); i += 2, j++)
	            {
	            	ps.setLong(i, tagIds.get(j));
	            	ps.setLong(i+1, newsId);
	            }	           
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	        	throw (new PersistanceException("Cannot unlink tags to news!", e));
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps);
	            } catch (SQLException e) {
	            	throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
		}
		
	}

	
}
