package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.PersistanceException;



public class AuthorDAOImpl extends GeneralDAOImpl implements AuthorDAO{
	
	private final static String INSERT_AUTHOR_SQL = "insert into Author (author_id, author_name, expired)"
	 		+ " values (INC_AUTHOR_ID.nextval,?,?)";
	private final static String GET_AUTHOR_BY_ID_SQL = "select author_name, expired from Author where author_id = ?";
	private final static String UPDATE_AUTHOR_SQL = "update Author set author_name = ?, expired = ? where author_id = ?";
	private final static String DELETE_AUTHOR_SQL = "delete from Author where author_id = ?";
	private final static String GET_ALL_AUTHORS_SQL = "select author_id, author_name, expired from Author";
	private final static String LINK_AUTHOR_TO_NEWS_SQL = "insert into News_Author (author_id, news_id) values (?,?)";
	private final static String UNLINK_AUTHOR_TO_NEWS_SQL = "delete from News_Author where (news_id = ?)";
	private final static String GET_AUTHOR_ID_BY_NAME_SQL = "select author_id from Author where author_name = ?";
	private final static String GET_AUTHOR_TO_NEWS = "select Author.author_id, Author.author_name, Author.expired "
			+ "from (Author inner join News_Author on News_Author.author_id = Author.author_id) "
			+ "where news_author.news_id = ?";
	
	public AuthorDAOImpl(DataSource dataSource) {
		
		super(dataSource);
	}

	public Long insert(Author author) {
		
	        Connection con = null;
	        PreparedStatement ps = null;
	        Long generatedId = 0L;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(INSERT_AUTHOR_SQL, new String[] { "author_id" });
	            ps.setString(1, author.getName());
	            ps.setTimestamp(2, author.getExpired());
	            ps.executeUpdate();
	            
	            ResultSet generatedKeys = ps.getGeneratedKeys();
	            if (null != generatedKeys && generatedKeys.next()) {
	                 generatedId = generatedKeys.getLong(1);
	            }	            
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps);
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
			return generatedId;
		
	}

	public Author getById(Long authorId) {
		 	Author author = null;
	        Connection con = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(GET_AUTHOR_BY_ID_SQL);
	            ps.setLong(1, authorId);
	            rs = ps.executeQuery();
	            if(rs.next()){
	            	author = new Author();
	            	author.setId(authorId);
	            	author.setName(rs.getString("author_name"));
	            	author.setExpired(rs.getTimestamp("expired"));	               
	            }
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	                rs.close();
	                DAOUtilities.close(con, dataSource, ps, rs);
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	        return author;
	}

	public void update(Author author) {
		
        Connection con = null;
        PreparedStatement ps = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(UPDATE_AUTHOR_SQL);
            ps.setString(1, author.getName());
            ps.setTimestamp(2, author.getExpired());
            ps.setLong(3, author.getId());
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		
	}

	public void deleteById(Long authorId) {
		Connection con = null;
        PreparedStatement ps = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(DELETE_AUTHOR_SQL);
            ps.setLong(1, authorId);
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		
	}

	public List<Author> getAll() {
		
        List<Author> authorList = new ArrayList<Author>();
        Connection con = null;
        PreparedStatement ps = null;        
        ResultSet rs = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(GET_ALL_AUTHORS_SQL);
            rs = ps.executeQuery();
            while(rs.next()){
            	Author author = new Author();
            	author.setId(rs.getLong("author_id"));
            	author.setName(rs.getString("author_name"));
            	author.setExpired(rs.getTimestamp("expired"));
            	authorList.add(author);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, rs);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return authorList;
	}

	public void linkAuthorToNews(Long authorId, Long newsId) {
		 
	        Connection con = null;
	        PreparedStatement ps = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(LINK_AUTHOR_TO_NEWS_SQL);
	            ps.setLong(1, authorId);
	            ps.setLong(2, newsId);
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps);
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
		
	}
	
	public void unlinkAuthorToNews(Long newsId) {
		
	        Connection con = null;
	        PreparedStatement ps = null;
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(UNLINK_AUTHOR_TO_NEWS_SQL);
	            ps.setLong(1, newsId);
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps);
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
		
	}

	public Long getIdByName(String authorName) {
		
		Long id = 0L;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(GET_AUTHOR_ID_BY_NAME_SQL);
            ps.setString(1, authorName);
            rs = ps.executeQuery();
            if(rs.next()){                
                id = rs.getLong("author_id");	
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, rs);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
	}

	public Author getAuthorToNews(Long newsId) throws PersistanceException {
		 	Connection con = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        Author author = null;
	        
	        try{
	        	con = DAOUtilities.getConnection(dataSource);
	            ps = con.prepareStatement(GET_AUTHOR_TO_NEWS);
	            ps.setLong(1, newsId);
	            rs = ps.executeQuery();
	            if(rs.next()){                
	                author = new Author(rs.getLong("author_id"), rs.getString("author_name"), rs.getTimestamp("expired"));
	            }
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	            	DAOUtilities.close(con, dataSource, ps, rs);
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	        return author;
	}

	





	

}
