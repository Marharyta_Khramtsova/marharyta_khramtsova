package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.PersistanceException;



public class CommentDAOImpl extends GeneralDAOImpl implements CommentDAO{
	
	private final static String INSERT_COMMENT = "insert into Comments (comment_id, news_id, comment_text, creation_date)"
			+ " values (INC_COMMENT_ID.nextval,?,?,?)";
	private final static String GET_COMMENT_BY_ID = "select news_id, comment_text, creation_date from Comments where comment_id = ?";
	private final static String UPDATE_COMMENT = "update Comments set news_id = ?, comment_text = ?, creation_date = ?"
			+ " where comment_id = ?";
	private final static String DELETE_COMMENT = "delete from Comments where comment_id = ?";
	private final static String GET_ALL_COMMENTS = "select comment_id, news_id, comment_text, creation_date from Comments";
	private final static String GET_NUMBER_OF_COMMENTS = "select count(news_id) AS numberOfComments from Comments where news_id = ?";
	private final static String GET_COMMENTS_TO_NEWS = "select comment_id, comment_text, creation_date"
			+ " from Comments where news_id = ?";
	
	public CommentDAOImpl(DataSource dataSource) {
		
		super(dataSource);
	}

	public Long insert(Comment comment) throws PersistanceException {
		
        Connection con = null;
        PreparedStatement ps = null;
        Long generatedId = 0L;
        ResultSet generatedKeys = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(INSERT_COMMENT, new String[] { "comment_id" });
            ps.setLong(1, comment.getNewsId());
            ps.setString(2, comment.getCommentText());
            ps.setTimestamp(3, comment.getCreationDate());
            ps.executeUpdate();
            generatedKeys = ps.getGeneratedKeys();
            if (null != generatedKeys && generatedKeys.next()) {
                 generatedId = generatedKeys.getLong(1);
            }
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot insert comment into database!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, generatedKeys);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		return generatedId;
		
	}

	public Comment getById(Long commentId) throws PersistanceException {
		Comment comment = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(GET_COMMENT_BY_ID);
            ps.setLong(1, commentId);
            rs = ps.executeQuery();
            if(rs.next()){
            	comment = new Comment();
            	comment.setId(commentId);
            	comment.setNewsId(rs.getLong("news_id"));
            	comment.setCommentText(rs.getString("comment_text"));
            	comment.setCreationDate(rs.getTimestamp("creation_date"));
            	}            
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot get comment by id from database!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, rs);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return comment;
	}

	public void update(Comment comment) throws PersistanceException {
		
        Connection con = null;
        PreparedStatement ps = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(UPDATE_COMMENT);
            ps.setLong(1, comment.getNewsId());
            ps.setString(2, comment.getCommentText());
            ps.setTimestamp(3, comment.getCreationDate());
            ps.setLong(4, comment.getId());
            ps.executeUpdate();
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot update comment in database!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		
	}

	public void deleteById(Long commentId) throws PersistanceException {
		Connection con = null;
        PreparedStatement ps = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(DELETE_COMMENT);
            ps.setLong(1, commentId);
            ps.executeUpdate();
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot delete comment by id from database!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		
	}

	public List<Comment> getAll() throws PersistanceException {		
        List<Comment> commentList = new ArrayList<Comment>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(GET_ALL_COMMENTS);
            rs = ps.executeQuery();
            while(rs.next()){
            	Comment comment = new Comment();
            	comment.setId(rs.getLong("comment_id"));
            	comment.setNewsId(rs.getLong("news_id"));
            	comment.setCommentText(rs.getString("comment_text"));
            	comment.setCreationDate(rs.getTimestamp("creation_date"));
            	commentList.add(comment);
            }
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot get all comments from database!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, rs);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return commentList;
	
	}
	
	public int getNumberOfComments(Long newsId) throws PersistanceException {
		int numberOfComments = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try {
			con = DAOUtilities.getConnection(dataSource);
			ps = con.prepareStatement(GET_NUMBER_OF_COMMENTS);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next())
			{
				numberOfComments = rs.getInt("numberOfComments");
			}
		} catch (SQLException e) {
			throw (new PersistanceException("Cannot get number of comments by news_id from database!", e));
		}
		finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, rs);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		
		return numberOfComments;
	}

	public  List<Comment> getCommentsToNews(Long newsId) throws PersistanceException {
		List<Comment> commentList = new ArrayList<Comment>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
        	con = DAOUtilities.getConnection(dataSource);
            ps = con.prepareStatement(GET_COMMENTS_TO_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            while(rs.next()){
            	Comment comment = new Comment();
            	comment.setId(rs.getLong("comment_id"));
            	comment.setNewsId(newsId);
            	comment.setCommentText(rs.getString("comment_text"));
            	comment.setCreationDate(rs.getTimestamp("creation_date"));
            	commentList.add(comment);
            }
        }catch(SQLException e){
        	throw (new PersistanceException("Cannot get comments by news_id!", e));
        }finally{
            try {
            	DAOUtilities.close(con, dataSource, ps, rs);
            } catch (SQLException e) {
            	throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return commentList;
	}

}
