package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.PersistanceException;

public interface TagDAO extends GeneralDAO<Tag>{
	/**
	 * 
	 * @param tagName name of the tag
	 * @return tag id
	 * @throws PersistanceException
	 */
	public Long getIdByName(String tagName) throws PersistanceException;
	
	/**
	 * 
	 * @param tagId tag id to link the news
	 * @param newsId news id to link the tag
	 * @throws PersistanceException
	 */
	public void linkTagToNews(Long tagId, Long newsId) throws PersistanceException;
	/**
	 * 
	 * @param tagId tag id to unlink the news
	 * @param newsId news id to unlink the tag
	 * @throws PersistanceException
	 */
	public void unlinkTagToNews(Long tagId, Long newsId) throws PersistanceException;
	/**
	 * 
	 * @param tagIds tag ids to link the news
	 * @param newsId news id to link the tags
	 * @throws PersistanceException
	 */
	public void linkTagsToNews(List<Long> tagIds, Long newsId) throws PersistanceException;
	/**
	 * 
	 * @param tagIds tag ids to unlink the news
	 * @param newsId news id to unlink the tags
	 * @throws PersistanceException
	 */
	public void unlinkTagsToNews(List<Long> tagIds, Long newsId) throws PersistanceException;

	/**
	 * 
	 * @param newsId news id to find tags
	 * @return list of the tags linked to news
	 * @throws PersistanceException
	 */
	public List<Tag> getTagsToNews(Long newsId) throws PersistanceException;
	public List<Long> getTagsIdsToNews(Long newsId) throws PersistanceException;

}
