package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.PersistanceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;


public class TagServiceImpl implements TagService {
	
	final static Logger LOGGER = Logger.getRootLogger();
			
	private TagDAO tagDAO;	
	
	public TagServiceImpl(TagDAO tagDAO) {
		
		this.tagDAO = tagDAO;
	}

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	public void addTagToNews(Long tagId, Long newsId) throws ServiceException {
		try {
			tagDAO.linkTagToNews(tagId, newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot add tag to news!", e);
		}		
	}

	public void addTagsToNews(List<Long> tagIds, Long newsId) throws ServiceException {
		try {
			tagDAO.linkTagsToNews(tagIds, newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot add tags to news!", e);
		}
		
	}
	
	public void deleteTagFromNews(Long tagId, Long newsId) throws ServiceException {
		try {
			tagDAO.unlinkTagToNews(tagId, newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot delete tag from news!", e);
		}		
	}

	public void deleteTagsFromNews(List<Long> tagIds, Long newsId) throws ServiceException {
		try {
			tagDAO.unlinkTagsToNews(tagIds, newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot delete tags from news!", e);
		}
		
	}

	public List<Tag> getTagsList() throws ServiceException {
		try {
			return tagDAO.getAll();
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot get all tags!", e);
		}
	}

	

}