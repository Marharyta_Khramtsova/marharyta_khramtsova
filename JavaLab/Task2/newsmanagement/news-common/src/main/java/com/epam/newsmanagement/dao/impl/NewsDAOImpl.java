package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.PersistanceException;



public class NewsDAOImpl extends GeneralDAOImpl implements NewsDAO {
	
	private final static String INSERT_NEWS = "insert into News (news_id, title, short_text, full_text,"
			+ "creation_date, modification_date) values (INC_NEWS_ID.nextval,?,?,?,?,?)";
	private final static String GET_NEWS_BY_ID = "select title, short_text, full_text,"
			+ "creation_date, modification_date  from News where news_id = ?";
	private final static String UPDATE_NEWS = "update News set title = ?, short_text = ?, full_text = ?,"
			+ "creation_date = ?, modification_date = ?  where news_id = ?";
	private final static String DELETE_NEWS = "delete from News where news_id = ?";
	private final static String GET_ALL_NEWS = "select News.news_id, News.title, News.short_text, News.full_text," 
			+ " News.creation_date, News.modification_date, count(Comments.comment_id) as totalCommentsCount"
			+ " from News left join Comments on News.news_id = Comments.news_id "
			+ " group by News.news_id, News.title, News.short_text, News.full_text, News.creation_date, News.modification_date "
			+ " order by totalCommentsCount desc, News.modification_date desc";
	private final static String FIND_NEWS = "select News.news_id, News.title, News.short_text, News.full_text," 
			+ " News.creation_date, News.modification_date, count(Comments.comment_id) as totalCommentsCount"
			+ " from News left join Comments on News.news_id = Comments.news_id ";			
	private final static String COUNT_NEWS = "select count(*) AS numberOfNews from News";
	
	
	public NewsDAOImpl(DataSource dataSource) {
		super(dataSource);
	}
	
	public Long insert(News news) throws PersistanceException {
		
		Connection con = null;
		PreparedStatement ps = null;
		Long generatedId = 0L;
		ResultSet generatedKeys = null;
		try {
			con = DAOUtilities.getConnection(dataSource);
			ps = con.prepareStatement(INSERT_NEWS, new String[] { "news_id" });
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, new Date(news.getModificationDate().getTime()));
			ps.executeUpdate();
			generatedKeys = ps.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				generatedId = generatedKeys.getLong(1);
			}

		} catch (SQLException e) {
			throw (new PersistanceException("Cannot insert news into database!", e));
		} finally {
			try {
				DAOUtilities.close(con, dataSource, ps, generatedKeys);
			} catch (SQLException e) {
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return generatedId;

	}

	public News getById(Long newsId) throws PersistanceException {
		
		News news = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DAOUtilities.getConnection(dataSource);
			ps = con.prepareStatement(GET_NEWS_BY_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				news = new News();
				news.setId(newsId);
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getTimestamp("creation_date"));
				news.setModificationDate(rs.getDate("modification_date"));
			}

		} catch (SQLException e) {
			throw (new PersistanceException("Cannot get news by id from database!", e));
		} finally {
			try {
				DAOUtilities.close(con, dataSource, ps, rs);
			} catch (SQLException e) {
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return news;
	}

	public void update(News news) throws PersistanceException {
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DAOUtilities.getConnection(dataSource);
			ps = con.prepareStatement(UPDATE_NEWS);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, new Date(news.getModificationDate().getTime()));
			ps.setLong(6, news.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw (new PersistanceException("Cannot update news in database!", e));
		} finally {
			try {
				DAOUtilities.close(con, dataSource, ps);
			} catch (SQLException e) {
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}

	}

	public void deleteById(Long newsId) throws PersistanceException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DAOUtilities.getConnection(dataSource);
			ps = con.prepareStatement(DELETE_NEWS);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw (new PersistanceException("Cannot delete news by id from database!", e));
		} finally {
			try {
				DAOUtilities.close(con, dataSource, ps);
			} catch (SQLException e) {
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}

	}

	public List<News> getAll() throws PersistanceException {
		
		List<News> newsList = new ArrayList<News>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DAOUtilities.getConnection(dataSource);
			ps = con.prepareStatement(GET_ALL_NEWS);
			rs = ps.executeQuery();
			while (rs.next()) {
				
				News news = new News();
				news.setId(rs.getLong("news_id"));
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getTimestamp("creation_date"));
				news.setModificationDate(rs.getDate("modification_date"));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw (new PersistanceException("Cannot get all news from database!", e));
		} finally {
			try {
				DAOUtilities.close(con, dataSource, ps, rs);
			} catch (SQLException e) {
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return newsList;
	}

	public int countNews() throws PersistanceException {		
		int numberOfNews = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DAOUtilities.getConnection(dataSource);
			ps = con.prepareStatement(COUNT_NEWS);
			rs = ps.executeQuery();
			if (rs.next()) {
				numberOfNews = rs.getInt("numberOfNews");
			} else {
				System.out.println("No news in the table!");
			}
		} catch (SQLException e) {
			throw (new PersistanceException("Cannot count news in database!", e));
		} finally {
			try {
				DAOUtilities.close(con, dataSource, ps, rs);
			} catch (SQLException e) {
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}

		return numberOfNews;
	}

	public List<News> findNews(SearchCriteria criteria) throws PersistanceException {
		
		List<News> newsList = new ArrayList<News>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DAOUtilities.getConnection(dataSource);
			if (criteria == null) {
				criteria = new SearchCriteria();
			}
			StringBuffer stringBuffer = new StringBuffer(FIND_NEWS);
			stringBuffer.append(SearchUtils.appendAuthorCriteria(criteria));
			stringBuffer.append(SearchUtils.appendTagsCriteria(criteria));
			ps = con.prepareStatement(stringBuffer.toString());
			SearchUtils.setQueryValues(ps, criteria);
			rs = ps.executeQuery();
			while (rs.next()) {
				News news = new News();
				news.setId(rs.getLong("news_id"));
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getTimestamp("creation_date"));
				news.setModificationDate(rs.getDate("modification_date"));
				newsList.add(news);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw (new PersistanceException("Cannot get list of news from database!", e));
		} finally {
			try {
				DAOUtilities.close(con, dataSource, ps, rs);
			} catch (SQLException e) {
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return newsList;
	}

}
