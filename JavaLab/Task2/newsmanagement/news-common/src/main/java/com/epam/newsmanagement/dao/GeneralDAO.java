package com.epam.newsmanagement.dao;


import java.util.List;

import com.epam.newsmanagement.exception.PersistanceException;

//CRUD operations
/**
* 
* @author Marharyta_Khramtsova

*/
public interface GeneralDAO<T> {
	
	//Create
	/**
	 * 
	 * @param item to insert
	 * @return id of inserted item
	 * @throws PersistanceException
	 */
  public Long insert(T item) throws PersistanceException;
  //Read
  /**
   * 
   * @param id of the item to get
   * @return item from database
   * @throws PersistanceException
   */
  public T getById(Long id) throws PersistanceException;
  //Update
  /**
   * 
   * @param item to update
   * @throws PersistanceException
   */
  public void update(T item) throws PersistanceException;
  //Delete
  /**
   * 
   * @param id of item to delete
   * @throws PersistanceException
   */
  public void deleteById(Long id) throws PersistanceException;
  //Get All
  /**
   * 
   * @return list of items
   * @throws PersistanceException
   */
  public List<T> getAll() throws PersistanceException;

}

