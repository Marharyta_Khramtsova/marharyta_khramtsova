package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.PersistanceException;

public interface AuthorDAO extends GeneralDAO<Author>{
	
	
	/**
	 * 
	 * @param authorId id of author that attach to news
	 * @param newsId id of news that attach to author
	 * @throws PersistanceException
	 */
	public void linkAuthorToNews(Long authorId, Long newsId) throws PersistanceException;
	
	/**
	 * 
	 * @param newsId id of news that unattach from author
	 * @throws PersistanceException
	 */
	
	public void unlinkAuthorToNews(Long newsId) throws PersistanceException;

	/**
	 * 
	 * @param authorName name of author to find
	 * @return id of author
	 * @throws PersistanceException
	 */
	public Long getIdByName(String authorName) throws PersistanceException;
	
	public Author getAuthorToNews(Long newsId) throws PersistanceException;
	
	

}

