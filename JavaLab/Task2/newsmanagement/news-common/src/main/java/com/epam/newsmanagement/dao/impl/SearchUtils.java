package com.epam.newsmanagement.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import com.epam.newsmanagement.entity.SearchCriteria;

public class SearchUtils {
	
	public static String appendAuthorCriteria(SearchCriteria searchCriteria) {
		
		Long authorId = searchCriteria.getAuthorId();
		if ( authorId != null && !(new Long(0L).equals(authorId))) {
			return " inner join News_Author on News.news_id = News_Author.news_id and News_Author.author_id = ?";
		}
		return "";
	}
	
	public static String appendTagsCriteria(SearchCriteria searchCriteria) {
		StringBuffer query = new StringBuffer("");
		List<Long> tagIds = searchCriteria.getTagIds();
		
		if (tagIds != null && !tagIds.isEmpty()) {
			
			int tagsCount = tagIds.size();
			if (!(tagsCount == 1 && tagIds.get(0) == 0)){
				query.append(" inner join News_Tag on News.news_id = News_Tag.news_id and (News_Tag.tag_id = ?");
				Iterator<Long> it = tagIds.iterator();
				it.next();
				while (it.hasNext()) {
					query.append(" or News_Tag.tag_id = ?");
					it.next();
				}		
				query.append(")");
			}
			
		}
		query.append(" group by News.news_id, News.title, News.short_text,"
					+ " News.full_text, News.creation_date, News.modification_date"
					+ " order by totalCommentsCount desc nulls last, News.modification_date desc");
		
		
		
		return query.toString();
		
	}
	
	public static void setQueryValues(PreparedStatement preparedStatement, SearchCriteria searchCriteria) {
		
		try {
			int paramIndex = 1;
			Long authorId = searchCriteria.getAuthorId();
			List<Long> tagIds = searchCriteria.getTagIds();
			
			if ( authorId != null && !(new Long(0L).equals(authorId))) {
				preparedStatement.setLong(paramIndex, authorId);
				paramIndex++;
			}
			if (tagIds != null && !tagIds.isEmpty()) {
				int tagsCount = tagIds.size();
				if (!(tagsCount == 1 && tagIds.get(0) == 0)){
					for (Long tagId : tagIds) {
						preparedStatement.setLong(paramIndex, tagId);
						paramIndex++;
					}
				}
				
			}
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
