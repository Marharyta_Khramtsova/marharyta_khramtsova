package com.epam.newsmanagement.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.CombinedItem;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.PersistanceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;



public class NewsServiceImpl implements NewsService{
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	private NewsDAO newsDAO = null;
	private AuthorDAO authorDAO = null;
	private TagDAO tagDAO = null;
	private CommentDAO commentDAO = null;
	
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
	
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}
	
	public NewsServiceImpl(NewsDAO newsDAO, AuthorDAO authorDAO, TagDAO tagDAO, CommentDAO commentDAO) {
		
		this.newsDAO = newsDAO;
		this.authorDAO = authorDAO;
		this.tagDAO = tagDAO;
		this.commentDAO = commentDAO;
	}
	
	@Transactional(rollbackFor = ServiceException.class)
	public void addNews(CombinedItem combinedItem) throws ServiceException {
		News news = combinedItem.getNews();
		try {
			newsDAO.insert(news);
			authorDAO.linkAuthorToNews(combinedItem.getAuthor().getId(), news.getId());		
			tagDAO.linkTagsToNews(combinedItem.getTagIds(), news.getId());
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot add news!", e);
		}		
		
	}

	public void editNews(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot edit news!", e);
		}		
	}

	@Transactional(rollbackFor = ServiceException.class)
	public void deleteNews(Long newsId) throws ServiceException {
		try {
			newsDAO.deleteById(newsId);
			authorDAO.unlinkAuthorToNews(newsId);
			tagDAO.unlinkTagsToNews((tagDAO.getTagsIdsToNews(newsId)), newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot delete news!", e);
		}	
		
	}

	public List<CombinedItem> getNewsList() throws ServiceException {
		
		List<CombinedItem> combinedItems = new ArrayList<CombinedItem>();
		List<News> listOfNews;
		try {
			listOfNews = newsDAO.getAll();
			for (News news : listOfNews)
			{
				combinedItems.add(new CombinedItem(news, 
						authorDAO.getAuthorToNews(news.getId()), 
						tagDAO.getTagsToNews(news.getId()),
						commentDAO.getCommentsToNews(news.getId())));
			}
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot get news list!", e);
		}
		return combinedItems;
	}

	public CombinedItem getNews(Long id) throws ServiceException {
		CombinedItem combinedItem = null;
		News news = null;		
		try {
			news = newsDAO.getById(id);
			combinedItem = new CombinedItem(news, 
					authorDAO.getAuthorToNews(news.getId()), 
					tagDAO.getTagsToNews(news.getId()),
					commentDAO.getCommentsToNews(news.getId()));
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot get news!", e);
		}
		return combinedItem;
	}

	public int countNews() throws ServiceException {
		int count = 0;
		try {
			count =  newsDAO.countNews();
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot get news count!", e);
		}
		return count;
	}

	public List<CombinedItem> findNews(SearchCriteria criteria) throws ServiceException {
		List<News> newsList = null;
		List<CombinedItem> combinedItems = new ArrayList<CombinedItem>();
		try {
			newsList = newsDAO.findNews(criteria);
			for (News news : newsList) {
				combinedItems.add(new CombinedItem(news,
						authorDAO.getAuthorToNews(news.getId()),
						tagDAO.getTagsToNews(news.getId()),
						commentDAO.getCommentsToNews(news.getId())));
			}
			return combinedItems;
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot find news!", e);
		}		
	}

}
