package com.epam.newsmanagement.service.impl;


import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.PersistanceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;


public class CommentServiceImpl implements CommentService{
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	private CommentDAO commentDAO = null;
	
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}	

	public CommentServiceImpl(CommentDAO commentDAO) {
		
		this.commentDAO = commentDAO;
	}

	public void addComment(Comment comment) throws ServiceException {
		try {
			commentDAO.insert(comment);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot add comment!", e);
		}
		
	}

	public void addComments(List<Comment> comments) throws ServiceException {
		for (Comment comment : comments)
		{
			try {
				this.addComment(comment);
			} catch (ServiceException e) {
				LOGGER.error(e.getMessage());
	            throw new ServiceException("Cannot add comments!", e);
			}
		}
		
	}

	public void deleteComment(Comment comment) throws ServiceException {
		try {
			commentDAO.deleteById(comment.getId());
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot delete comment!", e);
		}
		
	}

	public void deleteComments(List<Comment> comments) throws ServiceException {
		for (Comment comment : comments)
		{
			try {
				this.deleteComment(comment);
			} catch (ServiceException e) {
				LOGGER.error(e.getMessage());
	            throw new ServiceException("Cannot delete comments!", e);
			}
		}
		
	}

	
	public int getCommentsCount(Long newsId) throws ServiceException {
		try {
			return commentDAO.getNumberOfComments(newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw new ServiceException("Cannot get comments count!", e);
		}
	}
}
