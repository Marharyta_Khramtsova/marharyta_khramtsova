package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.CombinedItem;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.PersistanceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;



public class TestNewsService {
	
	@Mock 
	private NewsDAO newsDAO;
	@Mock 
	private AuthorDAO authorDAO = null;
	@Mock 
	private TagDAO tagDAO = null;
	@Mock 
	private CommentDAO commentDAO = null;
	
	@Autowired
	private NewsService newsService; 
	
	@Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks( this );
        newsService = new NewsServiceImpl(newsDAO, authorDAO, tagDAO, commentDAO);
    }
	
	@Test
    public void testAddNews() throws PersistanceException, ServiceException
    {
		News news = new News(); 
		List<Tag> tags = new ArrayList<Tag>(); 
		List<Long> tagIds = new ArrayList<Long>(); 
		Author author = new Author(); 
		when(newsDAO.insert(news)).thenReturn(1L); 
		
		CombinedItem combinedItem = new CombinedItem(news, author, tags, new ArrayList<Comment>());
		newsService.addNews(combinedItem);
		
		verify(newsDAO, times(1)).insert(news); 
		verify(authorDAO, times(1)).linkAuthorToNews(author.getId(), news.getId()); 
		verify(tagDAO, times(1)).linkTagsToNews(tagIds, news.getId()); 	
    }
	
	@Test
    public void testEditNews() throws ServiceException, PersistanceException
    {
		News news = new News(); 
		newsService.editNews(news);
		verify(newsDAO, times(1)).update(news);
    }
	
	@Test
    public void testDeleteNews() throws ServiceException, PersistanceException
    {		
		newsService.deleteNews(1L);
		verify(newsDAO, times(1)).deleteById(1L);
		verify(authorDAO, times(1)).unlinkAuthorToNews(1L);
		verify(tagDAO, times(1)).unlinkTagsToNews(new ArrayList<Long>(), 1L);
    }
	
	@Test
    public void testGetNewsList() throws PersistanceException, ServiceException
    {		
		List<News> newsList = new ArrayList<News>();
		newsList.add(new News());
		when(newsDAO.getAll()).thenReturn(newsList);
		when(commentDAO.getNumberOfComments(0L)).thenReturn((int)Math.random()*10);
		newsService.getNewsList();
		verify(newsDAO, times(1)).getAll();
    }
	
	@Test
    public void testGetNews() throws PersistanceException, ServiceException
    {		
		News news = new News();
		when(newsDAO.getById(1L)).thenReturn(news);
		newsService.getNews(1L);
		verify(newsDAO, times(1)).getById(1L);
		
    }
	
	@Test
    public void testCountNews() throws PersistanceException, ServiceException
    {		
		when(newsDAO.countNews()).thenReturn(1);
		newsService.countNews();
		verify(newsDAO, times(1)).countNews();
		
    }
	
	
	
	

}
