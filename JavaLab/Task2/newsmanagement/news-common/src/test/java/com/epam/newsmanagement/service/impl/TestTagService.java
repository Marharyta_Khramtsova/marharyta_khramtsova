package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.exception.PersistanceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;



@ContextConfiguration(locations = "spring-config.xml")
public class TestTagService {
	
	@Mock 
	private TagDAO tagDAO;	
	@Autowired
	private TagService tagService; 
	
	@Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        tagService = new TagServiceImpl(tagDAO);
    }
	
	@Test
    public void testAddTagToNews() throws ServiceException, PersistanceException
    {
		tagService.addTagToNews(1L, 1L);	
		verify(tagDAO).linkTagToNews(1L, 1L);		
    }
	
	@Test
    public void testAddTagsToNews() throws ServiceException, PersistanceException
    {
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1L);
		tagIds.add(2L);		
		tagService.addTagsToNews(tagIds, 1L);		
		verify(tagDAO).linkTagsToNews(tagIds, 1L);	
    }
	@Test
    public void testDeleteTagFromNews() throws ServiceException, PersistanceException
    {
		tagService.deleteTagFromNews(1L, 1L);	
		verify(tagDAO).unlinkTagToNews(1L, 1L);		
    }
	
	@Test
    public void testDeleteTagsFromNews() throws ServiceException, PersistanceException
    {
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1L);
		tagIds.add(2L);		
		tagService.deleteTagsFromNews(tagIds, 1L);		
		verify(tagDAO).unlinkTagsToNews(tagIds, 1L);	
    }

}
