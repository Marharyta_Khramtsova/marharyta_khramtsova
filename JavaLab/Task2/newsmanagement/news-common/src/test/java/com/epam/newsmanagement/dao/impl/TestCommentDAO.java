package com.epam.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;


@ContextConfiguration(locations = "spring-config.xml")
@DataSet(value = "CommentDAOTests/TestCommentDAO.xml")
public class TestCommentDAO extends UnitilsJUnit4{
	
	@TestDataSource 
	private DataSource dataSource;	
	private CommentDAO commentDAO;
	
	@Before
	public void initializeDao() 
	{ 
		commentDAO = new CommentDAOImpl(dataSource); 
	}
	
	@Test	
	public void testInsert() throws Exception {
		Comment comment = new Comment(1L,"comment_text","2015-01-01 02:10:00");			
		Long generatedId = commentDAO.insert(comment);
		comment.setId(generatedId);
		assertEquals(comment, commentDAO.getById(generatedId)); 
	}
	
	@Test	
	public void testGetById() throws Exception {
		Comment comment = commentDAO.getById(1L); 
		Long id = 1L;
		assertEquals(id, comment.getNewsId()); 
		assertEquals("comment_text1", comment.getCommentText());  
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date creationDate = dateFormat.parse("2013-01-01 02:10:00");
        assertEquals(new Timestamp(creationDate.getTime()), comment.getCreationDate());  
	}
	
	@Test 
	@ExpectedDataSet("CommentDAOTests/TestCommentDAO.testDelete.xml") 
	public void testDelete() throws Exception 
	{ 		
		commentDAO.deleteById(1L); 
	}
	
	@Test 
	public void testUpdate() throws Exception 
	{ 
		Comment comment = new Comment(1L,1L,"updated_comment_text","2015-01-01 02:10:00");	
		commentDAO.update(comment); 
		Comment comment2 = commentDAO.getById(1L);
		assertEquals(comment.getCommentText(), comment2.getCommentText());  
	} 
	
	@Test 
	public void testGetAll() throws Exception 
	{ 
		Comment comment1 = new Comment(1L,1L,"comment_text1","2013-01-01 02:10:00");		        
		Comment comment2 = new Comment(2L,1L,"comment_text2","2013-01-01 02:20:00");        
		List<Comment> comments = commentDAO.getAll();
		List<Comment> expectedComments = new ArrayList<Comment>();
		expectedComments.add(comment1);
		expectedComments.add(comment2);		
		assertEquals(expectedComments, comments);
	}

	@Test 
	public void testGetNumberOfComments() throws Exception 
	{ 
		Integer numberOfComments = commentDAO.getNumberOfComments(1L);
		Integer number = 2;
		assertEquals(number,numberOfComments);  
	} 
	
	@Test 
	public void testGetCommentsToNews() throws Exception 
	{ 
		Comment comment1 = new Comment(1L,1L,"comment_text1","2013-01-01 02:10:00");		        
		Comment comment2 = new Comment(2L,1L,"comment_text2","2013-01-01 02:20:00");   	
        
		List<Comment> comments = commentDAO.getCommentsToNews(1L);
		List<Comment> expectedComments = new ArrayList<Comment>();
		expectedComments.add(comment1);
		expectedComments.add(comment2);		
		assertEquals(expectedComments, comments);
	} 
	
}
