package com.epam.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;


import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;

@ContextConfiguration(locations = "spring-config.xml")
@DataSet(value = "AuthorDAOTests/TestAuthorDAO.xml")
public class TestAuthorDAO extends UnitilsJUnit4{
	
	@TestDataSource 
	private DataSource dataSource;	
	private AuthorDAO authorDAO;
	
	@Before
	public void initializeDao() 
	{ 
		authorDAO = new AuthorDAOImpl(dataSource); 
	}
	
	@Test	
	public void testInsert() throws Exception {
		Author author = new Author();
		author.setName("insertedAuthor");
		Long generatedID = authorDAO.insert(author);
		assertEquals(author.getName(), authorDAO.getById(generatedID).getName());		
	}
	
	@Test	
	public void testGetById() throws Exception {
		Author author = authorDAO.getById(1L); 
		assertEquals("author1", author.getName());  
	}
	
	@Test 
	@ExpectedDataSet("AuthorDAOTests/TestAuthorDAO.testDelete.xml") 
	public void testDelete() throws Exception 
	{ 		
		authorDAO.unlinkAuthorToNews(1L);
		authorDAO.deleteById(1L); 
	}
	
	@Test 
	public void testUpdate() throws Exception 
	{ 
		Author author = new Author(2L, "updatedAuthor2", "2015-01-01 02:10:00");
		authorDAO.update(author); 
		Author author2 = authorDAO.getById(2L); 
		assertEquals(author.getName(), author2.getName());  
	} 
	
	@Test 
	@ExpectedDataSet("AuthorDAOTests/TestAuthorDAO.testLink.xml") 
	public void testLinkAuthorToNews() throws Exception 
	{ 
		authorDAO.linkAuthorToNews(1L, 2L); 
	} 
	
	@Test 
	@ExpectedDataSet("AuthorDAOTests/TestAuthorDAO.testUnLink.xml") 
	public void testUnLinkAuthorToNews() throws Exception 
	{ 
		authorDAO.unlinkAuthorToNews(1L); 
	}
	
	@Test 
	public void testGetAll() throws Exception 
	{ 
		List<Author> authors = authorDAO.getAll();
		List<Author> expectedAuthors = new ArrayList<Author>();
		expectedAuthors.add(new Author(1L, "author1"));
		expectedAuthors.add(new Author(2L, "author2"));
		assertEquals(expectedAuthors, authors);
	}
	
	
	
	@Test 
	public void testGetIdByName() throws Exception 
	{ 
		Long id = 2L;
		assertEquals(authorDAO.getIdByName("author2"), id);
	}

}
