package com.epam.newsmanagement.dao.impl;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;


@DataSet(value = "TagDAOTests/TestTagDAO.xml")
@ContextConfiguration(locations = "spring-config.xml")

public class TestTagDAO extends UnitilsJUnit4{
	
	@TestDataSource 
	private DataSource dataSource;	
	private TagDAO tagDAO;
	
	@Before
	public void initializeDao()	{ 
		tagDAO = new TagDAOImpl(dataSource); 
	}
	
	@Test	
	public void testInsert() throws Exception {
		Tag tag = new Tag("insertedTag");
		Long generatedId = tagDAO.insert(tag);
		tag.setId(generatedId);
		assertEquals(tagDAO.getById(generatedId), tag); 
	}
	
	@Test	
	public void testGetById() throws Exception {
		Tag tag = tagDAO.getById(1L); 
		assertEquals("tag1", tag.getName());  
	}
	
	@Test	
	public void testGetIdByName() throws Exception {
		Long id = tagDAO.getIdByName("tag1"); 
		Long id2 = 1L;
		assertEquals(id2, id);  
	}
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testDelete.xml") 
	public void testDelete() throws Exception { 		
		tagDAO.unlinkTagToNews(1L, 1L); 
		tagDAO.deleteById(1L); 
	}
	
	@Test 
	public void testUpdate() throws Exception { 
		Tag tag = new Tag(2L, "updatedTag2");
		tagDAO.update(tag); 
		Tag tag2 = tagDAO.getById(2L); 
		assertEquals(tag.getName(), tag2.getName());  
	} 
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testLink.xml") 
	public void testLinkTagToNews() throws Exception { 
		tagDAO.linkTagToNews(1L, 2L); 
	} 
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testUnLink.xml") 
	public void testUnLinkTagToNews() throws Exception { 
		tagDAO.unlinkTagToNews(1L, 1L); 
	}
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testLinkTags.xml") 
	public void testLinkTagsToNews() throws Exception {  
		List<Long> ids = new ArrayList<Long>();
		ids.add(1L);
		ids.add(2L);
		tagDAO.linkTagsToNews(ids, 2L); 
	} 
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testUnLinkTags.xml") 
	public void testUnLinkTagsToNews() throws Exception { 
		List<Long> ids = new ArrayList<Long>();
		ids.add(1L);
		ids.add(2L);
		tagDAO.unlinkTagsToNews(ids, 2L); 
	}
	
	@Test 
	public void testGetAll() throws Exception { 
		List<Tag> tags = tagDAO.getAll();
		List<Tag> expectedTags = new ArrayList<Tag>();
		expectedTags.add(new Tag(1L, "tag1"));
		expectedTags.add(new Tag(2L, "tag2"));
		assertEquals(expectedTags, tags);
	}
	
	
	@Test 
	public void testGetTags() throws Exception { 
		List<Long> tags = tagDAO.getTagsIdsToNews(1L);
		assertEquals(tags.get(0),new Long(1L));
	}
	
	
}

