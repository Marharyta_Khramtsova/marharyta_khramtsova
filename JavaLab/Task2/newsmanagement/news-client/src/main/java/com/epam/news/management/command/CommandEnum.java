package com.epam.news.management.command;

public enum CommandEnum {
	
	SHOWNEWS
	{
		{
			this.command = new ShowNewsCommand();
		}
	},
	FILTERNEWS
	{
		{
			this.command = new FilterNewsCommand();
		}
	},
	POSTCOMMENT
	{
		{
			this.command = new PostCommentCommand();
		}
	};
	Command command;
	public Command getCurrentCommand()
	{
		return command;
	}

}
