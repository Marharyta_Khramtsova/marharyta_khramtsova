package com.epam.news.management.command;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.epam.newsmanagement.exception.ServiceException;

public interface Command {
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException;

}
