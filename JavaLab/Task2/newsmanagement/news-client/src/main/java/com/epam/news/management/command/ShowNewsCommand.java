package com.epam.news.management.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.entity.CombinedItem;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

public class ShowNewsCommand implements Command{	
	
	//@Autowired
	private NewsService newsService;

	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
		
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("spring-context.xml");
		newsService = (NewsService) applicationContext.getBean("newsService");
		HttpSession session = request.getSession(true);
		Long newsId = Long.parseLong(request.getParameter("newsId"));
		CombinedItem combinedItem = newsService.getNews(newsId);
		session.setAttribute("combinedItem", combinedItem);	
		session.setAttribute("newsId", newsId);
		return "/pages/news.jsp";
	}

}
