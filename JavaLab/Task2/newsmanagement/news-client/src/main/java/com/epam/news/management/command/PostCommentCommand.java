package com.epam.news.management.command;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.entity.CombinedItem;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;

public class PostCommentCommand implements Command {

	//@Autowired
		private CommentService commentService;
		private NewsService newsService;
		
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {
		
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("spring-context.xml");
		newsService = (NewsService) applicationContext.getBean("newsService");
		commentService = (CommentService) applicationContext.getBean("commentService");
		HttpSession session = request.getSession(true);
		Long newsId = (Long) session.getAttribute("newsId");
		Comment comment = new Comment(newsId, request.getParameter("commentText"), 
				new Timestamp(new java.util.Date().getTime()));
		commentService.addComment(comment);
		CombinedItem combinedItem = newsService.getNews(newsId);
		session.setAttribute("combinedItem", combinedItem);	
		session.setAttribute("newsId", newsId);
		response.sendRedirect("?command=shownews&newsId=" + newsId);
		return null;
	}

}
