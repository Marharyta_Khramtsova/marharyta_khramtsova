package com.epam.news.management.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.CombinedItem;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

public class FilterNewsCommand implements Command{

	//@Autowired
	private NewsService newsService;
	private AuthorService authorService;
	private TagService tagService;
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
		
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("spring-context.xml");
		newsService = (NewsService) applicationContext.getBean("newsService");
		authorService = (AuthorService) applicationContext.getBean("authorService");
		tagService = (TagService) applicationContext.getBean("tagService");	
		SearchCriteria searchCriteria = null;
		HttpSession session = request.getSession();
		if (request.getParameter("fromNews") != null) {
			searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
		}
		else if (request.getParameter("reset") == null) {
			String [] tagStringIds = request.getParameterValues("tagId");
			List<Long> tagIds = null;
			if (tagStringIds != null) {
				tagIds = new ArrayList<Long>();
				for (String s : tagStringIds) {
					tagIds.add(Long.parseLong(s));
				}
			}
			Long authorId = null;
			if (request.getParameter("authorId") != null) {
				authorId = Long.parseLong(request.getParameter("authorId"));
			}
			searchCriteria = new SearchCriteria(authorId, tagIds);			
		}
		List<CombinedItem> combinedItemsList = newsService.findNews(searchCriteria);
		List<Author> authorsList = authorService.getAuthorsList();
		List<Tag> tagsList = tagService.getTagsList();		
		session.setAttribute("combinedItemsList", combinedItemsList);
		session.setAttribute("authorsList", authorsList);
		session.setAttribute("tagsList", tagsList);
		session.setAttribute("searchCriteria", searchCriteria);
		return "pages/news-list.jsp";
	}

}
