package com.epam.news.management.command;

import javax.servlet.http.HttpServletRequest;

public class CommandFactory {
	
	private static final String COMMAND_PARAMETER_NAME = "command";
	
	public static Command defineCommand(HttpServletRequest request) {
		String command = request.getParameter(COMMAND_PARAMETER_NAME);
		if (command != null) {
			return CommandEnum.valueOf(command.toUpperCase()).getCurrentCommand();
		}
		else
		{
			return CommandEnum.FILTERNEWS.getCurrentCommand();
		}
		
	}

}
