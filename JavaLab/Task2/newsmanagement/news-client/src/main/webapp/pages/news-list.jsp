<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ include file="include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<title>News list</title>
</head>

<body class="container">
	<jsp:include page="header.jsp"></jsp:include>
	<h3>News List!</h3>
	<form action="Controller" class="line">
		<select name="authorId" >
			<option value="0" selected>Please select the author</option>
			<c:forEach var="author" items="${authorsList}">
				<option value=<c:out value="${author.id}" /> 
					<c:if test="${author.id == searchCriteria.authorId}">
								selected
					</c:if>		
					>
					<c:out value="${author.name}"/>
				</option>
			</c:forEach>
		</select> 
		<select name="tagId" multiple>
			<option value="0">Please select the tag</option>
			<c:forEach var="tag" items="${tagsList}">
				<option value=<c:out value="${tag.id}" />
				<c:forEach items="${searchCriteria.tagIds}" var="tagId">
							<c:if test="${tag.id == tagId}">
								selected
							</c:if>
						</c:forEach>
				>
				<c:out value="${tag.name}" />
				</option>
			</c:forEach>
		</select> 
		<input type="submit" name="command" value="filternews">
		</form>
		<form action="Controller" class="line">
			<input  type="submit" name="command" value="filternews">
			<input type="hidden" name="reset">
		</form>
		
	


	<div class="news-list">
		<c:forEach var="combinedItem" items="${combinedItemsList}">
			<form>
				<div class="news-item">
					<input type="hidden" name="command" value="shownews">
					<input type="hidden" name="newsId" value="<c:out value="${combinedItem.news.id}" />">
					<button class="news-title" type="submit">
						<c:out value="${combinedItem.news.title}" />
					</button>
					<div class="author">
						(by&nbsp
						<c:out value="${combinedItem.author.name}" />
						)
					</div>
					<div class="news-creation-date">
						<c:out value="${combinedItem.news.creationDate}" />
					</div>
					<div class="news-short-text">
						<c:out value="${combinedItem.news.shortText}" />
					</div>
					<c:forEach var="tag" items="${combinedItem.tags}">
						<span class="tag"><c:out value="${tag.name}" /></span>
					</c:forEach>
					<br>
					<c:set var="commentsCount" value="0" />
					<c:forEach items="${combinedItem.comments}">
						<c:set var="commentsCount" value="${commentsCount + 1}" />
					</c:forEach>
					<div class="toggle" >Comments (<c:out value="${commentsCount}" />)</div>
					
					<div class="comments">
						<c:forEach var="comment" items="${combinedItem.comments}">
						<div class="comment">
							<div class="comment-date">
								<c:out value="${comment.creationDate}" />
							</div>
							<div class="comment-text">
								<c:out value="${comment.commentText}" />
							</div>
							
						</div>
						</c:forEach>
					</div>
				</div>
			</form>
		</c:forEach>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>

</html>