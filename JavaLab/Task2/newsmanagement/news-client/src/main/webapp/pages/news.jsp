<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ include file="include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>News page</title>
</head>
<body class="container">
		<jsp:include page="header.jsp"></jsp:include>		
		<form action="Controller">			
			<input type="submit" name="command" value="filternews">
			<input type="hidden" name="fromNews">
		</form>
		<div><c:out value="${combinedItem.news.title}" /></div>
		<br>
		<div class="author">
			(by&nbsp
			<c:out value="${combinedItem.author.name}" />
			)
		</div>
		<br>
		<div class="news-creation-date">
			<c:out value="${combinedItem.news.creationDate}" />
		</div>
		<br>
		<div><c:out value="${combinedItem.news.shortText}" /></div>
		<br>
		<div><c:out value="${combinedItem.news.fullText}" /></div>
		<br>
		<c:set var="commentsCount" value="0" />
			<c:forEach items="${combinedItem.comments}">
				<c:set var="commentsCount" value="${commentsCount + 1}" />
			</c:forEach>
		<a class="toggle" type="button">Comments (<c:out value="${commentsCount}" />)</a>
		<div class="comments">
			<c:forEach var="comment" items="${combinedItem.comments}">
			<div class="comment">
			<div class="comment-date">
					<c:out value="${comment.creationDate}" />
				</div>	
				<div class="comment-text">
					<c:out value="${comment.commentText}" />
				</div>
							
			</div>
			</c:forEach>
		</div>
		<form action="Controller">
			<div class="post-comment-form">
				<textarea rows="5" cols="50" placeholder="Enter comment here..." name="commentText"></textarea>		
				<div class="post-comment-button"><button type="submit" name="command" value="postcomment">Post comment</button></div>
			</div>		
		</form>
		<button class="move-button previous-button">previous</button>
		<button class="move-button next-button">next</button>		
		<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>