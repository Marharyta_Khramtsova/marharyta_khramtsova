package com.epam.lab.marharytakhramtsova.javalab.task1.serviceimpl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.CommentDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Comment;
import com.epam.lab.marharytakhramtsova.javalab.task1.service.CommentService;

public class TestCommentService {
	
	@Mock
	private CommentDAO commentDAO;
	private CommentService commentService;
	
	@Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks( this );
        commentService = new CommentServiceImpl(commentDAO);
    }
	
	@Test
	public void testAddComment() throws Exception
	{
		Comment comment = new Comment();
		commentService.addComment(comment);
		verify(commentDAO, times(1)).insert(comment);
	}
	
	@Test
	public void testAddComments() throws Exception
	{
		Comment comment = new Comment();
		List<Comment> comments = new ArrayList<Comment>();
		comments.add(comment);
		comments.add(comment);
		commentService.addComments(comments);
		verify(commentDAO, times(comments.size())).insert(comment);
	}
	
	@Test
	public void testDeleteComment() throws Exception
	{
		Comment comment = new Comment();
		commentService.deleteComment(comment);
		verify(commentDAO, times(1)).deleteById(comment.getId());
	}
	
	@Test
	public void testDeleteComments() throws Exception
	{
		Comment comment = new Comment();
		List<Comment> comments = new ArrayList<Comment>();
		comments.add(comment);
		comments.add(comment);
		commentService.deleteComments(comments);
		verify(commentDAO, times(comments.size())).deleteById(comment.getId());
	}

}
