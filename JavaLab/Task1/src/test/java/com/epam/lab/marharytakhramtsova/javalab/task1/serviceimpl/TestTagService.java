package com.epam.lab.marharytakhramtsova.javalab.task1.serviceimpl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.TagDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.ServiceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.service.TagService; 


@ContextConfiguration(locations = "spring-config.xml")
public class TestTagService {
	
	@Mock 
	private TagDAO tagDAO;	
	@Autowired
	private TagService tagService; 
	
	@Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks( this );
        tagService = new TagServiceImpl(tagDAO);
    }
	
	@Test
    public void testAddTagToNews() throws ServiceException, PersistanceException
    {
		tagService.addTagToNews(1, 1);	
		verify(tagDAO).linkTagToNews(1, 1);		
    }
	
	@Test
    public void testAddTagsToNews() throws ServiceException, PersistanceException
    {
		List<Integer> tagIds = new ArrayList<Integer>();
		tagIds.add(1);
		tagIds.add(2);		
		tagService.addTagsToNews(tagIds, 1);		
		verify(tagDAO).linkTagsToNews(tagIds, 1);	
    }
	@Test
    public void testDeleteTagFromNews() throws ServiceException, PersistanceException
    {
		tagService.deleteTagFromNews(1, 1);	
		verify(tagDAO).unlinkTagToNews(1, 1);		
    }
	
	@Test
    public void testDeleteTagsFromNews() throws ServiceException, PersistanceException
    {
		List<Integer> tagIds = new ArrayList<Integer>();
		tagIds.add(1);
		tagIds.add(2);		
		tagService.deleteTagsFromNews(tagIds, 1);		
		verify(tagDAO).unlinkTagsToNews(tagIds, 1);	
    }

}
