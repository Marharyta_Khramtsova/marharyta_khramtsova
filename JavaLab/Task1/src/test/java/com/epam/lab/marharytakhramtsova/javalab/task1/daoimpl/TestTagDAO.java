package com.epam.lab.marharytakhramtsova.javalab.task1.daoimpl;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.TagDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Tag;



@DataSet(value = "TagDAOTests/TestTagDAO.xml")
@ContextConfiguration(locations = "spring-config.xml")

public class TestTagDAO extends UnitilsJUnit4{
	
	@TestDataSource 
	private DataSource dataSource;	
	private TagDAO tagDAO;
	
	@Before
	public void initializeDao() 
	{ 
	tagDAO = new TagDAOImpl(dataSource); 
	}
	
	@Test	
	public void testInsert() throws Exception {
		Tag tag = new Tag("insertedTag");
		int generatedId = tagDAO.insert(tag);
		tag.setId(generatedId);
		assertEquals(tagDAO.getById(generatedId), tag); 
	}
	
	@Test	
	public void testGetById() throws Exception {
		Tag tag = tagDAO.getById(1); 
		assertEquals("tag1", tag.getName());  
	}
	
	@Test	
	public void testGetIdByName() throws Exception {
		int id = tagDAO.getIdByName("tag1"); 
		assertEquals(1, id);  
	}
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testDelete.xml") 
	public void testDelete() throws Exception 
	{ 		
		tagDAO.unlinkTagToNews(1, 1); 
		tagDAO.deleteById(1); 
	}
	
	@Test 
	public void testUpdate() throws Exception 
	{ 
		Tag tag = new Tag(2, "updatedTag2");
		tagDAO.update(tag); 
		Tag tag2 = tagDAO.getById(2); 
		assertEquals(tag.getName(), tag2.getName());  
	} 
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testLink.xml") 
	public void testLinkTagToNews() throws Exception 
	{ 
		tagDAO.linkTagToNews(1, 2); 
	} 
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testUnLink.xml") 
	public void testUnLinkTagToNews() throws Exception 
	{ 
		tagDAO.unlinkTagToNews(1, 1); 
	}
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testLinkTags.xml") 
	public void testLinkTagsToNews() throws Exception 
	{  
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		tagDAO.linkTagsToNews(ids, 2); 
	} 
	
	@Test 
	@ExpectedDataSet("TagDAOTests/TestTagDAO.testUnLinkTags.xml") 
	public void testUnLinkTagsToNews() throws Exception 
	{ 
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		tagDAO.unlinkTagsToNews(ids, 2); 
	}
	
	@Test 
	public void testGetAll() throws Exception 
	{ 
		List<Tag> tags = tagDAO.getAll();
		List<Tag> expectedTags = new ArrayList<Tag>();
		expectedTags.add(new Tag(1, "tag1"));
		expectedTags.add(new Tag(2, "tag2"));
		assertEquals(expectedTags, tags);
	}
	
	@Test 
	public void testContains() throws Exception 
	{ 
		Tag tag = new Tag("tag2");
		assertEquals(true, tagDAO.contains(tag));
	}
	
	@Test 
	public void testGetTags() throws Exception 
	{ 
		List<Integer> tags = tagDAO.getTags(1);
		assertEquals(tags.get(0),new Integer(1));
	}
	
	
}

