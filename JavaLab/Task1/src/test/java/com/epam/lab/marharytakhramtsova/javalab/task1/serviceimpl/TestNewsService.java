package com.epam.lab.marharytakhramtsova.javalab.task1.serviceimpl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.AuthorDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dao.CommentDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dao.NewsDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dao.TagDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Author;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.CombinedItem;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Comment;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.News;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.ServiceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.search.SearchCriteria;
import com.epam.lab.marharytakhramtsova.javalab.task1.service.NewsService;

public class TestNewsService {
	
	@Mock 
	private NewsDAO newsDAO;
	@Mock 
	private AuthorDAO authorDAO = null;
	@Mock 
	private TagDAO tagDAO = null;
	@Mock 
	private CommentDAO commentDAO = null;
	
	@Autowired
	private NewsService newsService; 
	
	@Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks( this );
        newsService = new NewsServiceImpl(newsDAO, authorDAO, tagDAO, commentDAO);
    }
	
	@Test
    public void testAddNews() throws PersistanceException, ServiceException
    {
		News news = new News(); 
		List<Integer> tagIds = new ArrayList<Integer>(); 
		Author author = new Author(); 
		when(newsDAO.insert(news)).thenReturn(1); 
		
		CombinedItem combinedItem = new CombinedItem(news, author, tagIds, new ArrayList<Comment>());
		newsService.addNews(combinedItem);
		
		verify(newsDAO, times(1)).insert(news); 
		verify(authorDAO, times(1)).linkAuthorToNews(author.getId(), news.getId()); 
		verify(tagDAO, times(1)).linkTagsToNews(tagIds, news.getId()); 	
    }
	
	@Test
    public void testEditNews() throws ServiceException, PersistanceException
    {
		News news = new News(); 
		newsService.editNews(news);
		verify(newsDAO, times(1)).update(news);
    }
	
	@Test
    public void testDeleteNews() throws ServiceException, PersistanceException
    {		
		newsService.deleteNews(1);
		verify(newsDAO, times(1)).deleteById(1);
		verify(authorDAO, times(1)).unlinkAuthorToNews(1);
		verify(tagDAO, times(1)).unlinkTagsToNews(new ArrayList<Integer>(), 1);
    }
	
	@Test
    public void testGetNewsList() throws PersistanceException, ServiceException
    {		
		List<News> newsList = new ArrayList<News>();
		newsList.add(new News());
		when(newsDAO.getAll()).thenReturn(newsList);
		when(commentDAO.getNumberOfComments(0)).thenReturn((int)Math.random()*10);
		newsService.getNewsList();
		verify(newsDAO, times(1)).getAll();
		verify(commentDAO, times(1)).getNumberOfComments(0);
    }
	
	@Test
    public void testGetNews() throws PersistanceException, ServiceException
    {		
		News news = new News();
		when(newsDAO.getById(1)).thenReturn(news);
		newsService.getNews(1);
		verify(newsDAO, times(1)).getById(1);
		
    }
	
	@Test
    public void testCountNews() throws PersistanceException, ServiceException
    {		
		when(newsDAO.countNews()).thenReturn(1);
		newsService.countNews();
		verify(newsDAO, times(1)).countNews();
		
    }
	
	@Test
    public void testFindNews() throws PersistanceException, ServiceException
    {		
		SearchCriteria criteria = new SearchCriteria(new Author(), new ArrayList<Integer>()); 
		List<News> newsList = new ArrayList<News>();
		newsList.add(new News());
		when(newsDAO.getListOfNews(0)).thenReturn(newsList);
		when(tagDAO.getTags(0)).thenReturn(new ArrayList<Integer>());
		newsService.findNews(criteria);
		verify(newsDAO, times(1)).getListOfNews(0);
		verify(tagDAO, times(1)).getTags(0);
		
    }
	
	

}
