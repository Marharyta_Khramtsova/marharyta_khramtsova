package com.epam.lab.marharytakhramtsova.javalab.task1.daoimpl;

import static org.junit.Assert.assertEquals;


import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.AuthorDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Author;

@ContextConfiguration(locations = "spring-config.xml")
@DataSet(value = "AuthorDAOTests/TestAuthorDAO.xml")
public class TestAuthorDAO extends UnitilsJUnit4{
	
	@TestDataSource 
	private DataSource dataSource;	
	private AuthorDAO authorDAO;
	
	@Before
	public void initializeDao() 
	{ 
		authorDAO = new AuthorDAOImpl(dataSource); 
	}
	
	@Test	
	public void testInsert() throws Exception {
		Author author = new Author();
		author.setName("insertedAuthor");
		int generatedID = authorDAO.insert(author);
		assertEquals(author.getName(), authorDAO.getById(generatedID).getName());		
	}
	
	@Test	
	public void testGetById() throws Exception {
		Author author = authorDAO.getById(1); 
		assertEquals("author1", author.getName());  
	}
	
	@Test 
	@ExpectedDataSet("AuthorDAOTests/TestAuthorDAO.testDelete.xml") 
	public void testDelete() throws Exception 
	{ 		
		authorDAO.unlinkAuthorToNews(1);
		authorDAO.deleteById(1); 
	}
	
	@Test 
	public void testUpdate() throws Exception 
	{ 
		Author author = new Author(2, "updatedAuthor2", "2015-01-01 02:10:00");
		authorDAO.update(author); 
		Author author2 = authorDAO.getById(2); 
		assertEquals(author.getName(), author2.getName());  
	} 
	
	@Test 
	@ExpectedDataSet("AuthorDAOTests/TestAuthorDAO.testLink.xml") 
	public void testLinkAuthorToNews() throws Exception 
	{ 
		authorDAO.linkAuthorToNews(1, 2); 
	} 
	
	@Test 
	@ExpectedDataSet("AuthorDAOTests/TestAuthorDAO.testUnLink.xml") 
	public void testUnLinkAuthorToNews() throws Exception 
	{ 
		authorDAO.unlinkAuthorToNews(1); 
	}
	
	@Test 
	public void testGetAll() throws Exception 
	{ 
		List<Author> authors = authorDAO.getAll();
		List<Author> expectedAuthors = new ArrayList<Author>();
		expectedAuthors.add(new Author(1, "author1"));
		expectedAuthors.add(new Author(2, "author2"));
		assertEquals(expectedAuthors, authors);
	}
	
	@Test 
	public void testContains() throws Exception 
	{ 
		Author author = new Author(2, "author2");
		assertEquals(true, authorDAO.contains(author));
	}
	
	@Test 
	public void testGetIdByName() throws Exception 
	{ 
		assertEquals(2, authorDAO.getIdByName("author2"));
	}

}
