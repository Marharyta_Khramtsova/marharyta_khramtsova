package com.epam.lab.marharytakhramtsova.javalab.task1.serviceimpl;


import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.AuthorDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Author;
import com.epam.lab.marharytakhramtsova.javalab.task1.service.AuthorService;


public class TestAuthorService {
	
	@Mock
	private AuthorDAO authorDAO;
	
	private AuthorService authorService;
	
	@Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks( this );
        authorService = new AuthorServiceImpl(authorDAO);
    }
	
	@Test
	public void testAddAuthor() throws Exception
	{
		Author author = new Author();
		authorService.addAuthor(author);
		verify(authorDAO, times(1)).insert(author);
	}
	
	@Test
	public void testExpireAuthor() throws Exception
	{
		Author author = new Author();
		authorService.expireAuthor(author);
		verify(authorDAO, times(1)).update(author);
	}
	
	@Test
	public void testGetAuthorList() throws Exception
	{
		List<Author> authors = new ArrayList<Author>();
		when(authorDAO.getAll()).thenReturn(authors);
		authorService.getAuthorsList();
		verify(authorDAO, times(1)).getAll();
	}
	
	

}
