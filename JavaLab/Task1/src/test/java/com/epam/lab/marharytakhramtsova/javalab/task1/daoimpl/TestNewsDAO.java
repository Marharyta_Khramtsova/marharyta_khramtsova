package com.epam.lab.marharytakhramtsova.javalab.task1.daoimpl;

import static org.junit.Assert.assertEquals;


import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.NewsDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.News;



@ContextConfiguration(locations = "spring-config.xml")
@DataSet(value = "NewsDAOTests/TestNewsDAO.xml")
public class TestNewsDAO extends UnitilsJUnit4{
	
	@TestDataSource 
	private DataSource dataSource;	
	private NewsDAO newsDAO;
	
	@Before
	public void initializeDao() 
	{ 
		newsDAO = new NewsDAOImpl(dataSource); 
	}
	
	@Test	
	public void testInsert() throws Exception {
		News news  = new News("title", "shorttext", "fullText", "12-11-2015 02:00:00", "13-11-2015");               
		int generatedId = newsDAO.insert(news);
		news.setId(generatedId);
		assertEquals(newsDAO.getById(generatedId), news); 
	}
	
	@Test	
	public void testGetById() throws Exception {
		News news = newsDAO.getById(1);
		News news1  = new News(1, "title1", "short_text1", "full_text1", "2011-01-01 02:00:00", "01-01-2012");
		assertEquals(news1,news);
	}
	
	@Test	
	public void testUpdate() throws Exception {
		News news  = new News(1, "updated_title", "shorttext", "fullText", "2011-01-01 02:00:00", "01-01-2012");
		newsDAO.update(news);
        News news1 = newsDAO.getById(1);
        assertEquals(news.getTitle(), news1.getTitle());
	}
	
	@Test	
	
	@ExpectedDataSet("NewsDAOTests/TestNewsDAO.testDelete.xml") 
	public void testDeleteById() throws Exception {
		
        newsDAO.deleteById(1);
	}
	
	@Test 
	public void testGetAll() throws Exception 
	{ 
		News news1  = new News(1, "title1", "short_text1", "full_text1", "2011-01-01 02:00:00", "01-01-2012");
		News news2  = new News(2, "title2", "short_text2", "full_text2", "2011-01-03 02:00:00", "04-01-2012");
		List<News> news = newsDAO.getAll();
		List<News> expectedNews = new ArrayList<News>();
		expectedNews.add(news1);
		expectedNews.add(news2);
		assertEquals(expectedNews, news);
	}
	
	@Test 
	public void testCountNews() throws Exception 
	{ 
		int count = newsDAO.countNews();
		assertEquals(2, count);
	}
	
	
	@Test 
	public void testGetNews() throws Exception 
	{ 
		List<News> news = newsDAO.getListOfNews(1);
		News news1  = new News(1, "title1", "short_text1", "full_text1", "2011-01-01 02:00:00", "01-01-2012");
		assertEquals(news.get(0), news1);
	}
	

}
