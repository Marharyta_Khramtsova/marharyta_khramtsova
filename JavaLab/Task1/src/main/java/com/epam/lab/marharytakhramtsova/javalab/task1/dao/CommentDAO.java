package com.epam.lab.marharytakhramtsova.javalab.task1.dao;

import java.util.List;

import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Comment;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;

public interface CommentDAO extends GeneralDAO<Comment> {
	
	public int getNumberOfComments(int newsId) throws PersistanceException;
	
	public List<Comment> getCommentsToNews(int newsId) throws PersistanceException;

}
