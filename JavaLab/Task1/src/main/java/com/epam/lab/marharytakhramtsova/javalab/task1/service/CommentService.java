package com.epam.lab.marharytakhramtsova.javalab.task1.service;

import java.util.List;

import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Comment;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.ServiceException;

public interface CommentService {
	
	public void addComment(Comment comment) throws ServiceException;
	
	public void addComments(List<Comment> comments) throws ServiceException;
	
	public void deleteComment(Comment comment) throws ServiceException;
	
	public void deleteComments(List<Comment> comments) throws ServiceException;

}
