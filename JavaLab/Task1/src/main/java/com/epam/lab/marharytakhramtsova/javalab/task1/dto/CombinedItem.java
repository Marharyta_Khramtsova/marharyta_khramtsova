package com.epam.lab.marharytakhramtsova.javalab.task1.dto;

import java.util.ArrayList;
import java.util.List;

public class CombinedItem {
	
	private News news;
	private Author author;
	private List<Integer> tagIds;
	private List<Comment> comments;
	
	public CombinedItem() {
		comments = new ArrayList<Comment>();
	}

	public CombinedItem(News news, Author author, List<Integer> tagIds, List<Comment> comments) {
		super();
		this.news = news;
		this.author = author;
		this.tagIds = tagIds;
		this.comments = comments;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Integer> getTagIds() {
		return tagIds;
	}

	public void setTagIds(List<Integer> tagIds) {
		this.tagIds = tagIds;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	

	
	
	


}
