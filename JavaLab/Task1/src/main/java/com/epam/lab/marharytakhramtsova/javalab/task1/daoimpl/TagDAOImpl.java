package com.epam.lab.marharytakhramtsova.javalab.task1.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.TagDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Tag;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;

public class TagDAOImpl implements TagDAO{
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	
	private DataSource dataSource;	
	
	
	public TagDAOImpl(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}

	public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

	public int insert(Tag tag) throws PersistanceException {

		String query = "insert into Tag (tag_id, tag_name) values (INC_TAG_ID.nextval,?)";

		Connection con = null;
		PreparedStatement ps = null;
		int generatedId = 0;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query, new String[] { "tag_id" });
			ps.setString(1, tag.getName());
			ps.executeUpdate();

			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				generatedId = generatedKeys.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw (new PersistanceException("Cannot insert tag into database!", e));
		} finally {
			try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return generatedId;

	}

	public Tag getById(int id) throws PersistanceException {
		 String query = "select tag_name from Tag where tag_id = ?";
	        Tag tag = null;
	        Connection con = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            ps.setInt(1, id);
	            rs = ps.executeQuery();
	            if(rs.next()){
	                tag = new Tag();
	                tag.setId(id);
	                tag.setName(rs.getString("tag_name"));	
	            }
	        }catch(SQLException e){
	        	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot get tag by id from database!", e));
	        }finally{
	            try {
	                rs.close();
	                ps.close();
	                con.close();
	            } catch (SQLException e) {	        
	            	LOGGER.error(e.getMessage());
		            throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
	        return tag;
	}

	public void update(Tag tag) throws PersistanceException {
		 String query = "update Tag set tag_name = ? where tag_id = ?";
	        Connection con = null;
	        PreparedStatement ps = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            ps.setString(1, tag.getName());
	            ps.setInt(2, tag.getId());
	            ps.executeUpdate();
	            
	        }catch(SQLException e){
	        	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot update tag in database!", e));
	        }finally{
	            try {
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	            	LOGGER.error(e.getMessage());
		            throw (new PersistanceException("Cannot close connection with database!", e));	            	
	            }
	        }
		
	}

	public void deleteById(int id) throws PersistanceException {
		 String query = "delete from Tag where tag_id = ?";
	        Connection con = null;
	        PreparedStatement ps = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            ps.setInt(1, id);
	            ps.executeUpdate();
	        }catch(SQLException e){
	        	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot delete tag by id from database!", e));
	        }finally{
	            try {
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	            	LOGGER.error(e.getMessage());
		            throw (new PersistanceException("Cannot close connection with database!", e));
	            	
	            }
	        }
		
	}

	public List<Tag> getAll() throws PersistanceException {
		 	String query = "select tag_id, tag_name from Tag";
	        List<Tag> tagList = new ArrayList<Tag>();
	        Connection con = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            rs = ps.executeQuery();
	            while(rs.next()){
	                Tag tag = new Tag();
	                tag.setId(rs.getInt("tag_id"));
	                tag.setName(rs.getString("tag_name"));	               
	                tagList.add(tag);
	            }
	        }catch(SQLException e){
	        	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot get all tags from database!", e));
	        }finally{
	            try {
	                rs.close();
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	            	LOGGER.error(e.getMessage());
		            throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
	        return tagList;
	}

	public boolean contains(Tag tag) throws PersistanceException {
		String query = "select tag_name from Tag";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()){
               if (rs.getString("tag_name").equals(tag.getName()))
               {
            	   return true;
               }
            }
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot check tag contatining in database!", e));
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return false;
	}

	public void linkTagToNews(int tag_id, int news_id) throws PersistanceException {
		String query = "insert into News_Tag (tag_id, news_id) values (?,?)";
        Connection con = null;
        PreparedStatement ps = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, tag_id);
            ps.setInt(2, news_id);
            ps.executeUpdate();	           
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot link tag to news!", e));
        }finally{
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		
	}

	public void unlinkTagToNews(int tag_id, int news_id) throws PersistanceException {
		 String query = "delete from News_Tag where (tag_id = ? and news_id = ?)";
	        Connection con = null;
	        PreparedStatement ps = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            ps.setInt(1, tag_id);
	            ps.setInt(2, news_id);
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	        	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot unlink tag from news!", e));
	        }finally{
	            try {
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	            	LOGGER.error(e.getMessage());
		            throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
		
	}
	
	public List<Integer> getTags(int newsId) throws PersistanceException {
		String query = "select tag_id from News_Tag where news_id = ?";
        List<Integer> tagIds = new ArrayList<Integer>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, newsId);
            rs = ps.executeQuery();
            while(rs.next()){ 
            	tagIds.add(rs.getInt("tag_id"));
            }
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot get tags by news_id!", e));
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		return tagIds;
	}

	public int getIdByName(String tagName) throws PersistanceException {
		String query = "select tag_id from Tag where tag_name = ?";
        int id = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, tagName);
            rs = ps.executeQuery();
            if(rs.next()){                
                id = rs.getInt("tag_id");	
            }
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot get tag_id by tag_name!", e));
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return id;
	}

	public void linkTagsToNews(List<Integer> tagIds, int newsId) throws PersistanceException {
		StringBuilder stringBuilder = new StringBuilder("insert into News_Tag (tag_id, news_id)");
		if (!tagIds.isEmpty())
		{
			stringBuilder.append(" select ?, ? from dual");
			int size = tagIds.size()-1;
			while (size != 0)
			{
				stringBuilder.append(" union all select ?, ? from dual");
				size--;
			}
			String query = stringBuilder.toString();
			Connection con = null;
	        PreparedStatement ps = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            for (int i = 1, j = 0; j < tagIds.size(); i += 2, j++)
	            {
	            	ps.setInt(i, tagIds.get(j));
	            	ps.setInt(i+1, newsId);
	            }	           
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	        	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot link tags to news!", e));
	        }finally{
	            try {
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	            	LOGGER.error(e.getMessage());
		            throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
		}
		
		
	}

	public void unlinkTagsToNews(List<Integer> tagIds, int newsId) throws PersistanceException {
		StringBuilder stringBuilder = new StringBuilder("delete from News_Tag"
				+ " where (tag_id = ? and news_id = ?)");
		if (!tagIds.isEmpty())
		{			
			int size = tagIds.size()-1;
			while (size != 0)
			{
				stringBuilder.append(" or (tag_id = ? and news_id = ?)");
				size--;
			}
			String query = stringBuilder.toString();
			Connection con = null;
	        PreparedStatement ps = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            for (int i = 1, j = 0; j < tagIds.size(); i += 2, j++)
	            {
	            	ps.setInt(i, tagIds.get(j));
	            	ps.setInt(i+1, newsId);
	            }	           
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	        	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot unlink tags to news!", e));
	        }finally{
	            try {
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	            	LOGGER.error(e.getMessage());
		            throw (new PersistanceException("Cannot close connection with database!", e));
	            }
	        }
		}
		
	}

	
}
