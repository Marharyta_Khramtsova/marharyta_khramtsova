package com.epam.lab.marharytakhramtsova.javalab.task1.service;

import java.util.List;

import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Author;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.ServiceException;

public interface AuthorService {
	
	public void addAuthor(Author author) throws ServiceException;
	
	public void expireAuthor(Author author) throws ServiceException;
	
	public List<Author> getAuthorsList() throws ServiceException;

}
