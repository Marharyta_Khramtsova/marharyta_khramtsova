package com.epam.lab.marharytakhramtsova.javalab.task1.dao;
import java.util.List;

import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Tag;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;


public interface TagDAO extends GeneralDAO<Tag>{
	
	public int getIdByName(String tagName) throws PersistanceException;
	
	public void linkTagToNews(int tagId, int newsId) throws PersistanceException;
	
	public void unlinkTagToNews(int tagId, int newsId) throws PersistanceException;
	
	public void linkTagsToNews(List<Integer> tagIds, int newsId) throws PersistanceException;
	
	public void unlinkTagsToNews(List<Integer> tagIds, int newsId) throws PersistanceException;
	
	public boolean contains(Tag tag) throws PersistanceException;
	
	public List<Integer> getTags(int newsId) throws PersistanceException;

}
