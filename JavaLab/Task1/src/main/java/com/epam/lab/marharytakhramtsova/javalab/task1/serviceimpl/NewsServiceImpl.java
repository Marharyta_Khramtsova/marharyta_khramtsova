package com.epam.lab.marharytakhramtsova.javalab.task1.serviceimpl;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.AuthorDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dao.CommentDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dao.NewsDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dao.TagDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.CombinedItem;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.News;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.ServiceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.search.SearchCriteria;
import com.epam.lab.marharytakhramtsova.javalab.task1.service.NewsService;

public class NewsServiceImpl implements NewsService{
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	private NewsDAO newsDAO = null;
	private AuthorDAO authorDAO = null;
	private TagDAO tagDAO = null;
	private CommentDAO commentDAO = null;
	
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
	
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}
	
	public NewsServiceImpl(NewsDAO newsDAO, AuthorDAO authorDAO, TagDAO tagDAO, CommentDAO commentDAO) {
		super();
		this.newsDAO = newsDAO;
		this.authorDAO = authorDAO;
		this.tagDAO = tagDAO;
		this.commentDAO = commentDAO;
	}
	
	public void addNews(CombinedItem combinedItem) throws ServiceException {
		News news = combinedItem.getNews();
		try {
			newsDAO.insert(news);
			authorDAO.linkAuthorToNews(combinedItem.getAuthor().getId(), news.getId());		
			tagDAO.linkTagsToNews(combinedItem.getTagIds(), news.getId());
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot add news!", e));
		}		
		
	}

	public void editNews(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot edit news!", e));
		}		
	}

	public void deleteNews(int newsId) throws ServiceException {
		try {
			newsDAO.deleteById(newsId);
			authorDAO.unlinkAuthorToNews(newsId);
			tagDAO.unlinkTagsToNews((tagDAO.getTags(newsId)), newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot delete news!", e));
		}	
		
	}

	public List<News> getNewsList() throws ServiceException {
		
		List<News> listOfNews, sortedNews = null;
		try {
			listOfNews = newsDAO.getAll();
			Map<Integer,News> map = new TreeMap<Integer, News>(Collections.reverseOrder());
			for (News news : listOfNews)
			{
				map.put(commentDAO.getNumberOfComments(news.getId()), news);
			}		
			sortedNews = new ArrayList<News>(map.values());
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot get news list!", e));
		}
		
		return sortedNews;
	}

	public News getNews(int id) throws ServiceException {
		News news = null;
		try {
			news =  newsDAO.getById(id);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot get news!", e));
		}
		return news;
	}

	public int countNews() throws ServiceException {
		int count = 0;
		try {
			count =  newsDAO.countNews();
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot get news count!", e));
		}
		return count;
	}

	public List<News> findNews(SearchCriteria criteria) throws ServiceException {
		
		List<News> newsToReturn = new ArrayList<News>();
		List<News> newsByAuthor;
		try {
			newsByAuthor = newsDAO.getListOfNews(criteria.getAuthor().getId());
			if (criteria.getTagIds().isEmpty())
			{
				return newsByAuthor;
			}
			else
			{
				List<Integer> foundTagIds = null;
				for (News news : newsByAuthor)
				{
					foundTagIds = tagDAO.getTags(news.getId());
					if (foundTagIds.equals(criteria.getTagIds()))
					{
						newsToReturn.add(news);
					}
				}
			}
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot find news!", e));
		}
		
		return newsToReturn;
	}

}
