package com.epam.lab.marharytakhramtsova.javalab.task1.service;

import java.util.List;


import com.epam.lab.marharytakhramtsova.javalab.task1.exception.ServiceException;

public interface TagService {
	
	public void addTagToNews(int tagId, int newsId) throws ServiceException;
	
	public void addTagsToNews(List<Integer> tagIds, int newsId) throws ServiceException;
	
	public void deleteTagFromNews (int tagId, int newsId) throws ServiceException;
	
	public void deleteTagsFromNews(List<Integer> tagIds, int newsId) throws ServiceException;

}
