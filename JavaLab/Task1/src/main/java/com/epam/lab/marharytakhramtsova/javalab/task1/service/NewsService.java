package com.epam.lab.marharytakhramtsova.javalab.task1.service;

import java.util.List;


import com.epam.lab.marharytakhramtsova.javalab.task1.dto.CombinedItem;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.News;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.ServiceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.search.SearchCriteria;

public interface NewsService {
	
	public void editNews (News news) throws ServiceException;
	
	public void deleteNews (int id) throws ServiceException;
	
	public List<News> getNewsList() throws ServiceException;
	
	public News getNews (int id) throws ServiceException;
	
	public int countNews() throws ServiceException;
	
	public void addNews(CombinedItem combinedItem) throws ServiceException;
	
	public List<News> findNews(SearchCriteria criteria) throws ServiceException;
	
}