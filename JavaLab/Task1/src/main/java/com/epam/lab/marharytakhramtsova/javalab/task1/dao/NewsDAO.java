package com.epam.lab.marharytakhramtsova.javalab.task1.dao;



import java.util.List;

import com.epam.lab.marharytakhramtsova.javalab.task1.dto.News;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;


public interface NewsDAO extends GeneralDAO<News>{
	
	public int countNews() throws PersistanceException;	
	
	public List<News> getListOfNews(int authorId) throws PersistanceException;
	
	
	

}
