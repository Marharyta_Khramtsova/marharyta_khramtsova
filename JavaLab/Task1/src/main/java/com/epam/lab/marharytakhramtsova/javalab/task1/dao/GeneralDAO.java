package com.epam.lab.marharytakhramtsova.javalab.task1.dao;

import java.util.List;

import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;

//CRUD operations
public interface GeneralDAO<T> {
	
	 //Create
    public int insert(T item) throws PersistanceException;
    //Read
    public T getById(int id) throws PersistanceException;
    //Update
    public void update(T item) throws PersistanceException;
    //Delete
    public void deleteById(int id) throws PersistanceException;
    //Get All
    public List<T> getAll() throws PersistanceException;

}
