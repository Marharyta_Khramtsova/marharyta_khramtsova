package com.epam.lab.marharytakhramtsova.javalab.task1.dto;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Author {
	
	private int id;	
	
	private String name;
	
	private Timestamp expired;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getExpired() {
		return expired;
	}
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}
	
	public Author() {
		
	}
	
	public Author(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.expired = null;
	}
	
	public Author(int id, String name, Timestamp expired) {
		super();
		this.id = id;
		this.name = name;
		this.expired = expired;
	}
	
	public Author(int id, String name, String expired) throws ParseException {
		super();
		this.id = id;
		this.name = name;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date date = dateFormat.parse(expired);
        this.expired = new Timestamp(date.getTime());
	}
	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
