package com.epam.lab.marharytakhramtsova.javalab.task1.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.CommentDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Comment;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;

public class CommentDAOImpl implements CommentDAO{
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	private DataSource dataSource;	
	
	public CommentDAOImpl(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}

	public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }	

	public int insert(Comment comment) throws PersistanceException {
		String query = "insert into Comments (comment_id, news_id, comment_text, creation_date)"
				+ " values (INC_COMMENT_ID.nextval,?,?,?)";
        Connection con = null;
        PreparedStatement ps = null;
        int generatedId = 0;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query, new String[] { "comment_id" });
            ps.setInt(1, comment.getNewsId());
            ps.setString(2, comment.getCommentText());
            ps.setTimestamp(3, comment.getCreationDate());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (null != generatedKeys && generatedKeys.next()) {
                 generatedId = generatedKeys.getInt(1);
            }
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot insert comment into database!", e));
        }finally{
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		return generatedId;
		
	}

	public Comment getById(int id) throws PersistanceException {
		String query = "select news_id, comment_text, creation_date from Comments where comment_id = ?";
		Comment comment = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next()){
            	comment = new Comment();
            	comment.setId(id);
            	comment.setNewsId(rs.getInt("news_id"));
            	comment.setCommentText(rs.getString("comment_text"));
            	comment.setCreationDate(rs.getTimestamp("creation_date"));
            	}            
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot get comment by id from database!", e));
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return comment;
	}

	public void update(Comment comment) throws PersistanceException {
		String query = "update Comments set news_id = ?, comment_text = ?, creation_date = ?"
				+ " where comment_id = ?";
        Connection con = null;
        PreparedStatement ps = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, comment.getNewsId());
            ps.setString(2, comment.getCommentText());
            ps.setTimestamp(3, comment.getCreationDate());
            ps.setInt(4, comment.getId());
            ps.executeUpdate();
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot update comment in database!", e));
        }finally{
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		
	}

	public void deleteById(int id) throws PersistanceException {
		String query = "delete from Comments where comment_id = ?";
        Connection con = null;
        PreparedStatement ps = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot delete comment by id from database!", e));
        }finally{
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		
	}

	public List<Comment> getAll() throws PersistanceException {
		String query = "select comment_id, news_id, comment_text, creation_date from Comments";
        List<Comment> commentList = new ArrayList<Comment>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()){
            	Comment comment = new Comment();
            	comment.setId(rs.getInt("comment_id"));
            	comment.setNewsId(rs.getInt("news_id"));
            	comment.setCommentText(rs.getString("comment_text"));
            	comment.setCreationDate(rs.getTimestamp("creation_date"));
            	commentList.add(comment);
            }
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot get all comments from database!", e));
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return commentList;
	
	}
	
	public int getNumberOfComments(int newsId) throws PersistanceException {
		String query = "select count(news_id) AS numberOfComments from Comments where news_id = ?";
		int numberOfComments = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);
			ps.setInt(1, newsId);
			rs = ps.executeQuery();
			if (rs.next())
			{
				numberOfComments = rs.getInt("numberOfComments");
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot get number of comments by news_id from database!", e));
		}
		finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
		
		return numberOfComments;
	}

	public  List<Comment> getCommentsToNews(int newsId) throws PersistanceException {
		String query = "select comment_id, comment_text, creation_date"
				+ " from Comments where news_id = ?";
        List<Comment> commentList = new ArrayList<Comment>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, newsId);
            rs = ps.executeQuery();
            while(rs.next()){
            	Comment comment = new Comment();
            	comment.setId(rs.getInt("comment_id"));
            	comment.setNewsId(newsId);
            	comment.setCommentText(rs.getString("comment_text"));
            	comment.setCreationDate(rs.getTimestamp("creation_date"));
            	commentList.add(comment);
            }
        }catch(SQLException e){
        	LOGGER.error(e.getMessage());
            throw (new PersistanceException("Cannot get comments by news_id!", e));
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
            	LOGGER.error(e.getMessage());
	            throw (new PersistanceException("Cannot close connection with database!", e));
            }
        }
        return commentList;
	}

}
