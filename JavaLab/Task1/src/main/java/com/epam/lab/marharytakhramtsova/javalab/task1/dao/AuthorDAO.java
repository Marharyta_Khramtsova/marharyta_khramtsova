package com.epam.lab.marharytakhramtsova.javalab.task1.dao;



import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Author;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;

public interface AuthorDAO extends GeneralDAO<Author>{
	
	public boolean contains(Author author) throws PersistanceException;

	public void linkAuthorToNews(int authorId, int newsId) throws PersistanceException;
	
	public void unlinkAuthorToNews(int newsId) throws PersistanceException;

	public int getIdByName(String authorName) throws PersistanceException;
	
	
	
	

}
