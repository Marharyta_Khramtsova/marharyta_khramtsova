package com.epam.lab.marharytakhramtsova.javalab.task1.serviceimpl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.TagDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.ServiceException;
import com.epam.lab.marharytakhramtsova.javalab.task1.service.TagService;

public class TagServiceImpl implements TagService {
	
	final static Logger LOGGER = Logger.getRootLogger();
			
	private TagDAO tagDAO;	
	
	public TagServiceImpl(TagDAO tagDAO) {
		super();
		this.tagDAO = tagDAO;
	}

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	public void addTagToNews(int tagId, int newsId) throws ServiceException {
		try {
			tagDAO.linkTagToNews(tagId, newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot add tag to news!", e));
		}		
	}

	public void addTagsToNews(List<Integer> tagIds, int newsId) throws ServiceException {
		try {
			tagDAO.linkTagsToNews(tagIds, newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot add tags to news!", e));
		}
		
	}
	
	public void deleteTagFromNews(int tagId, int newsId) throws ServiceException {
		try {
			tagDAO.unlinkTagToNews(tagId, newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot delete tag from news!", e));
		}		
	}

	public void deleteTagsFromNews(List<Integer> tagIds, int newsId) throws ServiceException {
		try {
			tagDAO.unlinkTagsToNews(tagIds, newsId);
		} catch (PersistanceException e) {
			LOGGER.error(e.getMessage());
            throw (new ServiceException("Cannot delete tags from news!", e));
		}
		
	}

}