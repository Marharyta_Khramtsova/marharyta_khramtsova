package com.epam.lab.marharytakhramtsova.javalab.task1.dto;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Comment {
	
	private int id;
	
	private int newsId;
	
	private String commentText;
	
	private Timestamp creationDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNewsId() {
		return newsId;
	}

	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	public Comment() {
		
	}	

	public Comment(int newsId, String commentText, Timestamp creationDate) {
		
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}
	
	public Comment(int id, int newsId, String commentText, String creationDate) throws ParseException {
		
		this.id = id;
		this.newsId = newsId;
		this.commentText = commentText;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date date = dateFormat.parse(creationDate);
        this.creationDate = new Timestamp(date.getTime());	
	}
	
	public Comment(int newsId, String commentText, String creationDate) throws ParseException {
		
		this.newsId = newsId;
		this.commentText = commentText;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date date = dateFormat.parse(creationDate);
        this.creationDate = new Timestamp(date.getTime());	
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", newsId=" + newsId + ", commentText=" + commentText + ", creationDate="
				+ creationDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + id;
		result = prime * result + newsId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id != other.id)
			return false;
		if (newsId != other.newsId)
			return false;
		return true;
	}
	
	

}
