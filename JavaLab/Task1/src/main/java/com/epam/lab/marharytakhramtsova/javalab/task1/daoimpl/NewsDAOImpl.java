package com.epam.lab.marharytakhramtsova.javalab.task1.daoimpl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.NewsDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.News;
import com.epam.lab.marharytakhramtsova.javalab.task1.exception.PersistanceException;

public class NewsDAOImpl implements NewsDAO {

	final static Logger LOGGER = Logger.getRootLogger();

	private DataSource dataSource;

	public NewsDAOImpl(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public int insert(News news) throws PersistanceException {
		String query = "insert into News (news_id, title, short_text, full_text,"
				+ "creation_date, modification_date) values (INC_NEWS_ID.nextval,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		int generatedId = 0;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query, new String[] { "news_id" });
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, new Date(news.getModificationDate().getTime()));
			ps.executeUpdate();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				generatedId = generatedKeys.getInt(1);
			}

		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw (new PersistanceException("Cannot insert news into database!", e));
		} finally {
			try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return generatedId;

	}

	public News getById(int id) throws PersistanceException {
		String query = "select title, short_text, full_text,"
				+ "creation_date, modification_date  from News where news_id = ?";
		News news = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				news = new News();
				news.setId(id);
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getTimestamp("creation_date"));
				news.setModificationDate(rs.getDate("modification_date"));
			}

		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw (new PersistanceException("Cannot get news by id from database!", e));
		} finally {
			try {
				rs.close();
				ps.close();
				con.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return news;
	}

	public void update(News news) throws PersistanceException {
		String query = "update News set title = ?, short_text = ?, full_text = ?,"
				+ "creation_date = ?, modification_date = ?  where news_id = ?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, new Date(news.getModificationDate().getTime()));
			ps.setInt(6, news.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw (new PersistanceException("Cannot update news in database!", e));
		} finally {
			try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}

	}

	public void deleteById(int id) throws PersistanceException {
		String query = "delete from News where news_id = ?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw (new PersistanceException("Cannot delete news by id from database!", e));
		} finally {
			try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}

	}

	public List<News> getAll() throws PersistanceException {
		String query = "select news_id, title, short_text, full_text," + "creation_date, modification_date from News";
		List<News> newsList = new ArrayList<News>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				News news = new News();
				news.setId(rs.getInt("news_id"));
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getTimestamp("creation_date"));
				news.setModificationDate(rs.getDate("modification_date"));
				newsList.add(news);
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw (new PersistanceException("Cannot get all news from database!", e));
		} finally {
			try {
				rs.close();
				ps.close();
				con.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return newsList;
	}

	public int countNews() throws PersistanceException {
		String query = "select count(*) AS numberOfNews from News";
		int numberOfNews = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				numberOfNews = rs.getInt("numberOfNews");
			} else {
				System.out.println("No news in the table!");
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw (new PersistanceException("Cannot count news in database!", e));
		} finally {
			try {
				rs.close();
				ps.close();
				con.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}

		return numberOfNews;
	}

	public List<News> getListOfNews(int authorId) throws PersistanceException {
		String query = "select News.news_id, News.title, News.short_text,"
				+ " News.full_text, News.creation_date, News.modification_date "
				+ " as news_id, title, short_text, full_text, creation_date, modification_date"
				+ " from (News_Author join News on News_Author.news_id = News.news_id)"
				+ " where News_Author.author_id = ?";
		List<News> newsList = new ArrayList<News>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);
			ps.setInt(1, authorId);
			rs = ps.executeQuery();
			while (rs.next()) {
				News news = new News();
				news.setId(rs.getInt("news_id"));
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getTimestamp("creation_date"));
				news.setModificationDate(rs.getDate("modification_date"));
				newsList.add(news);
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw (new PersistanceException("Cannot get list of news from database!", e));
		} finally {
			try {
				rs.close();
				ps.close();
				con.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
				throw (new PersistanceException("Cannot close connection with database!", e));
			}
		}
		return newsList;
	}

}
