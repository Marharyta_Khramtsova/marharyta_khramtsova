package com.epam.lab.marharytakhramtsova.javalab.task1.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.lab.marharytakhramtsova.javalab.task1.dao.AuthorDAO;
import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Author;

public class AuthorDAOImpl implements AuthorDAO{
	
	private DataSource dataSource;	
	
	
	public AuthorDAOImpl(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}

	public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

	public int insert(Author author) {
		 String query = "insert into Author (author_id, author_name, expired)"
		 		+ " values (INC_AUTHOR_ID.nextval,?,?)";
	        Connection con = null;
	        PreparedStatement ps = null;
	        int generatedId = 0;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query, new String[] { "author_id" });
	            ps.setString(1, author.getName());
	            ps.setTimestamp(2, author.getExpired());
	            ps.executeUpdate();
	            
	            ResultSet generatedKeys = ps.getGeneratedKeys();
	            if (null != generatedKeys && generatedKeys.next()) {
	                 generatedId = generatedKeys.getInt(1);
	            }	            
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
			return generatedId;
		
	}

	public Author getById(int id) {
		 	String query = "select author_name, expired from Author where author_id = ?";
		 	Author author = null;
	        Connection con = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            ps.setInt(1, id);
	            rs = ps.executeQuery();
	            if(rs.next()){
	            	author = new Author();
	            	author.setId(id);
	            	author.setName(rs.getString("author_name"));
	            	author.setExpired(rs.getTimestamp("expired"));	               
	            }
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	                rs.close();
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	        return author;
	}

	public void update(Author author) {
		String query = "update Author set author_name = ?, expired = ? where author_id = ?";
        Connection con = null;
        PreparedStatement ps = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, author.getName());
            ps.setTimestamp(2, author.getExpired());
            ps.setInt(3, author.getId());
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		
	}

	public void deleteById(int id) {
		String query = "delete from Author where author_id = ?";
        Connection con = null;
        PreparedStatement ps = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		
	}

	public List<Author> getAll() {
		String query = "select author_id, author_name, expired from Author where expired is null";
        List<Author> authorList = new ArrayList<Author>();
        Connection con = null;
        PreparedStatement ps = null;        
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()){
            	Author author = new Author();
            	author.setId(rs.getInt("author_id"));
            	author.setName(rs.getString("author_name"));
            	author.setExpired(rs.getTimestamp("expired"));
            	authorList.add(author);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return authorList;
	}

	public boolean contains(Author author) {
		String query = "select author_id from Author";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()){
               if (rs.getInt("author_id") == author.getId())
               {
            	   return true;
               }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
	}

	public void linkAuthorToNews(int authorId, int newsId) {
		 String query = "insert into News_Author (author_id, news_id) values (?,?)";
	        Connection con = null;
	        PreparedStatement ps = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            ps.setInt(1, authorId);
	            ps.setInt(2, newsId);
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
		
	}
	
	public void unlinkAuthorToNews(int newsId) {
		 String query = "delete from News_Author where (news_id = ?)";
	        Connection con = null;
	        PreparedStatement ps = null;
	        try{
	            con = dataSource.getConnection();
	            ps = con.prepareStatement(query);
	            ps.setInt(1, newsId);
	            ps.executeUpdate();	           
	        }catch(SQLException e){
	            e.printStackTrace();
	        }finally{
	            try {
	                ps.close();
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
		
	}

	public int getIdByName(String authorName) {
		String query = "select author_id from Author where author_name = ?";
        int id = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, authorName);
            rs = ps.executeQuery();
            if(rs.next()){                
                id = rs.getInt("author_id");	
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
                rs.close();
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
	}





	

}
