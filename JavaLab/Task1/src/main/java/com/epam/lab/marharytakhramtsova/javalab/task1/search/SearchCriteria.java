package com.epam.lab.marharytakhramtsova.javalab.task1.search;

import java.util.ArrayList;
import java.util.List;

import com.epam.lab.marharytakhramtsova.javalab.task1.dto.Author;

public class SearchCriteria {
	
	private Author author = null;
	
	private List<Integer> tagIds = null;
	
	public SearchCriteria() {
		tagIds = new ArrayList<Integer>();
	}
	
	public SearchCriteria(Author author, List<Integer> tagIds) {
		super();
		this.author = author;
		this.tagIds = tagIds;
	}



	public void setAuthor(Author author) {
		this.author = author;
	}
	
	public Author getAuthor() {
		return author;
	}
	
	public void setTagIds(List<Integer> tagIds) {
		this.tagIds = tagIds;
	}
	
	public List<Integer> getTagIds() {
		return tagIds;
	}

}
