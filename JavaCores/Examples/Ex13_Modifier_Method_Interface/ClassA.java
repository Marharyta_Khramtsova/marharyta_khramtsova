interface InterfaceA {
	private int a = 0;
 	void method();
}

//method can be only public and abstract, no more modifiers
//field must be initialized