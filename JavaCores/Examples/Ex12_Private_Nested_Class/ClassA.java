public class ClassA { 
    
    private static class ClassB { 
         private static int a = 1;
    } 
     
    public static void main(String[] args) { 
         System.out.println(ClassB.a);
    } 
} 