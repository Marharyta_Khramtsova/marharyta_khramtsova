public class ClassA
{
	//void m(long l)
	//{System.out.println("long!");}

	/*void m(BigDecimal bd)
	{System.out.println("Big Decimal!");} */

	//void m(double d)
	//{System.out.println("double!");}

	void m(Integer i)
	{System.out.println("Integer!");}

	void m(int i)
	{System.out.println("int!");}

	void m(float f)
	{System.out.println("float!");}
	

	public static void main(String [] ar)
	{
		ClassA a = new ClassA();
		float f = 1.2f;
		int i = 9;
		double d = 1.5;
		//a.m((float)d);
		a.m(i);
	}
}


// float -> double
// int -> double
// float ! -> int
// (float)double -> float