<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Editing</title>
</head>
<jsp:include page="header.jsp" />
<body>
	<fmt:bundle basename="page">
		<div class="panel panel-default step">
			<div class="panel-heading center-text">
				<h3 class="panel-title">
					<fmt:message key="editAbiturient" />
				</h3>
			</div>
			<div class="panel-body my-panel-body">
				<form action="Servlet">
					<table>
						<tr>
							<td><fmt:message key="surname" /></td>
							<td><input type="text" name="surname" size="30"
								value="${abiturient.surname}"></td>
						</tr>
						<tr>
							<td><fmt:message key="name" /></td>
							<td><input type="text" name="name" size="30"
								value="${abiturient.name}"></td>
						</tr>
						<tr>
							<td><fmt:message key="faculty" /></td>
							<td><select name="faculty">
									<c:forEach items="${faculties}" var="faculty">
										<option value="${faculty.id}"
											<c:if test="${faculty.id} == ${abiturient.faculty.id}">selected</c:if>>
											${faculty.name}</option>
									</c:forEach>
							</select></td>
						</tr>
						<tr>
							<td><fmt:message key="atestate" /></td>
							<td><input type="text" name="atestate" size="30"
								value="${abiturient.atestate}"></td>
						</tr>
						<c:forEach items="${subjects}" var="subject">
							<tr>
								<td>${subject.name}:</td>
								<td><input type="text" id="${subject.id}"
									name="${subject.id}" size="30"> <!-- style="display: none;" --></td>
							</tr>
						</c:forEach>
					</table>
					<div>
						<button type="submit" class="btn btn-success next" name="command"
							value="apply">
							<fmt:message key="addAbiturient" />
						</button>
					</div>

				</form>
			</div>
		</div>
	</fmt:bundle>

</body>
</html>