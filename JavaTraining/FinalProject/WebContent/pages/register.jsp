<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="error.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register</title>
</head>
<jsp:include page="standart.jsp" />
<body class="index container">
<fmt:bundle basename="page">	
<form action="Servlet">
<div class="panel panel-default step">
			<div class="panel-heading center-text">
				<h3 class="panel-title"><fmt:message key="registration"/></h3>
			</div>
			<div class="panel-body my-panel-body">	
			<h2><c:if test="${(message != null) && (login != null)}">${message}</c:if></h2>
			<table>
				<tr>
					<td><fmt:message key="enterLogin"/></td>
					<td><input type="text" name="login" size="30">	</td>
				</tr>
				<tr>
					<td><fmt:message key="password"/></td>
					<td> <input type="password" name="password" size="30"></td>
				</tr>
				<tr>
					<td><fmt:message key="info"/></td>
					<td><input type="text" name="info" size="30"></td>
				</tr>
			</table>
			<div class="row buttons">
				<div class="col-lg-5">
					<button type="submit" class="btn btn-success" name="command" value="register"><fmt:message key="register"/></button>
				</div>
				<div class="col-lg-7">
					<button type="submit" class="btn btn-success" name="command" value="empty"><fmt:message key="cancel"/></button>
				</div>
			</div>					
			</div>		
		</div>	
</form>
</fmt:bundle>
</body>
</html>