<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="error.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="header.jsp"/>
<title>Main page</title>
</head>
<body>
<fmt:bundle basename="page">
	<div class="usual-font center-text"><fmt:message key="mainPageText"/></div>
	<img alt="abiturients" src="resources/images/abiturient.jpg" class="image">
	</fmt:bundle>
</body>
</html>