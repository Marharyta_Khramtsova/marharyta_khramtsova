<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="cust" uri="custom.tld"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="standart.jsp"></jsp:include>
</head>
<body class="container">
<fmt:bundle basename="page">

	<cust:Hello login="${user.login}!"><fmt:message key="hello"/></cust:Hello>
	
	<nav class="navbar navbar-default navbar-fixed-top, navbar navbar-inverse">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
			</button>
			<a class="navbar-brand"
				href="/EpamFinalProject/Servlet?command=goToMainPage"><fmt:message key="main"/> <span
				class="glyphicon glyphicon-home"></span>
			</a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/EpamFinalProject/Servlet?command=apply"><fmt:message key="apply"/>
				 <span class="glyphicon glyphicon-plus"></span>
				</a></li>
				<li><a href="/EpamFinalProject/Servlet?command=showLists"><fmt:message key="showLists"/> 
				<span class="glyphicon glyphicon-list-alt"></span>
				</a></li>
				<li><a
					href="/EpamFinalProject/Servlet?command=showApplications"><fmt:message key="showApplications"/>
					<span class="glyphicon glyphicon-tasks"></span>
				</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false"><fmt:message key="settings"/> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<c:if test="${user.role eq 'ADMIN'}">
							<li><a href="/EpamFinalProject/Servlet?command=addSubject"><fmt:message key="addSubject"/></a></li>
							<li><a href="/EpamFinalProject/Servlet?command=addFaculty"><fmt:message key="addFaculty"/></a></li>
							<li><a href="/EpamFinalProject/Servlet?command=showUsers"><fmt:message key="usersList"/></a></li>
						</c:if>
						<li><a href="/EpamFinalProject/Servlet?command=deleteAbiturient"><fmt:message key="deleteAbiturients"/></a></li>
					</ul>
				</li>
			</ul>
			<form action="Servlet">
			<div class="row">				
					<button type="submit" class="btn btn-warning logoff" name="command" value="logoff"><fmt:message key="loggoff"/></button>				
			</div>							
			</form>
		</div>
	</div>
	</nav>
	</fmt:bundle>
</body>
</html>