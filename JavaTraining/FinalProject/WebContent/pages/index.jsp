<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="standart.jsp"></jsp:include>
<title>Index</title>
</head>
<body class="index">
<fmt:bundle basename="page">	
	<div class="header-font shadowtext ">
	<fmt:message key="welcome"/><br><fmt:message key="loginOrRegister"/>
	</div>
	<div class="panel panel-default main-step">
		<div class="panel-heading center-text">
			<h3 class="panel-title"><fmt:message key="chooseAction"/></h3>
		</div>
		<div class="panel-body my-panel-body">
			<form action="Servlet">
				<div class="row buttons">
					<div class="col-lg-5">
						<button type="submit" class="btn btn-success " name="command"
							value="login"><fmt:message key="login"/></button>
					</div>
					<div class="col-lg-7">
						<button type="submit" class="btn btn-success " name="command"
							value="register"><fmt:message key="register"/></button>
					</div>
				</div>
			</form>
		</div>
	</div>
	</fmt:bundle>

</body>
</html>