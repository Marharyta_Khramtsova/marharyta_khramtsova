<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Adding faculty</title>
</head>
<jsp:include page="header.jsp" />
<body>

	<form action="Servlet">
	<fmt:bundle basename="page">
		<div class="panel panel-default step">
			<div class="panel-heading center-text">
				<h3 class="panel-title">
					<fmt:message key="addFaculty" />
				</h3>
			</div>
			<div class="panel-body my-panel-body">

				<table>
					<tr>
						<td><fmt:message key="enterTitle" /></td>
						<td><input type="text" name="faculty_name"></td>
					</tr>
					<tr>
						<td><fmt:message key="enterCapacity" /></td>
						<td><input type="text" name="faculty_capacity"></td>
					</tr>
					<c:forEach items="${subjects}" var="subj">
						<tr>
							<td><input type="checkbox" name="subject" value="${subj.id}"></td>
							<td>${subj.name}</td>
						</tr>
					</c:forEach>
				</table>
				<button type="submit" class="btn btn-info next" name="command"
					value="addFaculty">
					<fmt:message key="addFaculty" />
				</button>
			</div>
		</div>
	</fmt:bundle>
	</form>

</body>
</html>