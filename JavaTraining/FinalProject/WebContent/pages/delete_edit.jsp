<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Deleting</title>
</head>
<jsp:include page="header.jsp" />
<body>
<fmt:bundle basename="page">
	<form action="Servlet">
	
		<table class="table table-bordered table-condensed  my-table">
			<tr class="header">
				<th><fmt:message key="surname"/></th>
				<th><fmt:message key="name"/></th>
				<th><fmt:message key="atestate"/></th>
				<c:forEach items="${abiturientsToDelete[0].marks}" var="entry">
					<td>${entry.key.name}</td>
				</c:forEach>
				<th><fmt:message key="sum"/></th>
				<th><fmt:message key="deleteAbiturients"/></th>
				<th><fmt:message key="editAbiturients"/></th>
			</tr>
			<c:forEach items="${abiturientsToDelete}" var="abiturient">
				<tr>
					<td>${abiturient.surname}</td>
					<td>${abiturient.name}</td>
					<td>${abiturient.atestate}</td>
					<c:forEach items="${abiturient.marks}" var="entry">
						<td>${entry.value}</td>
					</c:forEach>
					<td>${abiturient.sum}</td>
					<td><input type="checkbox" name="isDeleted"
						value="${abiturient.id}"></td>
					<td><input type="radio" name="isEdited"
						value="${abiturient.id}"></td>
				</tr>
			</c:forEach>

		</table>
		<button type="submit" class="btn btn-success delete" name="command"
			value="deleteAbiturient"><fmt:message key="deleteAbiturients"/></button>
		<button type="submit" class="btn btn-success delete" name="command"
			value="editAbiturient"><fmt:message key="editAbiturient"/></button>
	</form>
</fmt:bundle>
</body>
</html>