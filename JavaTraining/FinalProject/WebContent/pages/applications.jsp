<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="error.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Applications</title>
</head>
<jsp:include page="header.jsp"/>
<script type="text/javascript">	
		if(document.getElementById('isApplied').checked) {
		    $("#acceptApplications").show();
		} else {
		    $("#acceptApplications").hide();			
	}
</script>
<body>
<fmt:bundle basename="page">
	<form action="Servlet">
		<div class="row buttons-list">
			<select name="faculty">
				<c:forEach items="${faculties}" var="faculty">
					<option value="${faculty.id}">${faculty.name}</option>
				</c:forEach>
			</select>
			<button type="submit" class="btn btn-success" name="command"
				value="showApplications"><fmt:message key="showApplications"/></button>
				<c:if test="${role eq 'ADMIN'}">
			<button type="submit" class="btn btn-success" name="command"
				value="acceptApplication" id="acceptApplications">
				<fmt:message key="acceptApplications"/></button>
				</c:if>
		</div>
		<table class="table table-bordered table-condensed  my-table">
	<tr class="header">
		<th><fmt:message key="surname"/></th>
		<th><fmt:message key="name"/></th>
		<th><fmt:message key="atestate"/></th>
		<c:forEach items="${notenrolled[0].marks}" var="entry">
				<td>${entry.key.name}</td>
			</c:forEach>
		<th><fmt:message key="sum"/></th>
		<c:if test="${user.role eq 'ADMIN'}">
		<th><fmt:message key="acceptApplications"/></th>
		</c:if>
	</tr>	
		<c:forEach items="${notenrolled}" var="abiturient">
		<tr class="abituriens">
			<td>${abiturient.surname}</td>
			<td>${abiturient.name}</td>
			<td>${abiturient.atestate}</td>
			<c:forEach items="${abiturient.marks}" var="entry">
				<td>${entry.value}</td>				
			</c:forEach>
			<td>${abiturient.sum}</td>
			<c:if test="${user.role eq 'ADMIN'}">
			<td><input type="checkbox" name="isApplied" id="isApplied" value="${abiturient.id}"></td>
			</c:if>
			</tr>
		</c:forEach>	
	</table>	
	
	</form>
	</fmt:bundle>
</body>
</html>