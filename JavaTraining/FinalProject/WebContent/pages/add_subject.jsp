<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Adding Subject</title>
</head>
<jsp:include page="header.jsp" />
<body>
<fmt:bundle basename="page">
	<form action="Servlet">
		<div class="panel panel-default step">
			<div class="panel-heading center-text">
				<h3 class="panel-title"><fmt:message key="addSubject" /></h3>
			</div>
			<div class="panel-body my-panel-body">

				<table>
					<tr>
						<td><fmt:message key="enterTitle" /></td>
						<td><input type="text" name="subject_name"></td>
					</tr>
				</table>
				<button type="submit" class="btn btn-info next" name="command"
					value="addSubject"><fmt:message key="addSubject" /></button>
			</div>
		</div>

	</form>
	</fmt:bundle>
</body>
</html>