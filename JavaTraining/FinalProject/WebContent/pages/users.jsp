<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Users</title>
</head>
<jsp:include page="header.jsp" />
<body>
	<fmt:bundle basename="page">
		

			<table class="table table-bordered table-condensed  my-table">
				<tr class="header">
					<th><fmt:message key="login" /></th>
					<th><fmt:message key="password" /></th>
					<th><fmt:message key="info" /></th>
					<th><fmt:message key="lang" /></th>
					<th><fmt:message key="role" /></th>
					<th><fmt:message key="action" /></th>
					
				</tr>
				<c:forEach items="${users}" var="user">
				<form action="Servlet">
					<input type="hidden" name="idUser" value="${user.id}">
					<tr class="abituriens">
						<td>${user.login}</td>
						<td>${user.password}</td>
						<td>${user.info}</td>
						<td>${user.lang}</td>
						<td>${user.role}</td>
						<td>
						<button type="submit" class="btn btn-warning" name="command"
								value="setAdmin"<c:if test="${(user.role eq 'ADMIN')|| (user.login eq 'Margarita')}">
								style="display: none";
							</c:if>>
								<fmt:message key="setAdmin" />
							</button>
							<button type="submit" class="btn btn-warning" name="command"
								value="setNotAdmin"<c:if test="${(user.role eq 'USER') || (user.login eq 'Margarita')}">
								style="display: none";
							</c:if>>
								<fmt:message key="setNotAdmin" />
							</button>
						</td>
					</tr>
					</form>
				</c:forEach>
			</table>
		
	</fmt:bundle>
</body>
</html>