<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<fmt:setLocale value="${locale}" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="resources/js/jquery-2.1.1.min.js"></script>
<link rel="stylesheet" href="resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/script.js"></script>
</head>

<body class="container">
	<form action="Servlet">
		<fmt:bundle basename="page">
			<div class="row lang">
				<div class="col-lg-5">
					<div class="radio">
						<label><input type="radio" name="command" value="ru"
							onclick="updateLang(this);"
							<c:if test="${(sessionScope.locale eq 'ru_RUS') || (sessionScope.locale eq null)}">checked</c:if>>
							<fmt:message key="russian"/></label>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="radio">
						<label><input type="radio" name="command" value="en"
							onclick="updateLang(this);"
							<c:if test="${(sessionScope.locale eq 'en_US')}">checked</c:if>>
							<fmt:message key="english" /></label>
					</div>
				</div>
			</div>


		</fmt:bundle>
	</form>

</body>
</html>