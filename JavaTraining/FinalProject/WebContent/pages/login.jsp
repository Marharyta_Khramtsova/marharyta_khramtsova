<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="error.jsp"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="standart.jsp"></jsp:include>
<title>Login</title>
</head>
<body class="index container">
	<fmt:bundle basename="page">
	<form action="Servlet">
		<div class="panel panel-default step">
			<div class="panel-heading center-text">
				<h3 class="panel-title"><fmt:message key="enterLoginPassword"/></h3>
			</div>
			<div class="panel-body my-panel-body">	
			<table>
				<tr>
					<td><fmt:message key="enterLogin"/></td>
					<td><input type="text" name="login" size="30">	</td>
				</tr>
				<tr>
					<td><fmt:message key="password"/></td>
					<td> <input type="password" name="password" size="30"></td>
				</tr>
			</table>
				<button type="submit" class="btn btn-success next" name="command" value="login"><fmt:message key="login"/></button>				
						
			</div>		
		</div>		
	</form>
	</fmt:bundle>
</body>
</html>