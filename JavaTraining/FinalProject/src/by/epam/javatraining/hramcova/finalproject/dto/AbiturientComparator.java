package by.epam.javatraining.hramcova.finalproject.dto;

import java.util.Comparator;

public class AbiturientComparator implements Comparator<Abiturient>{

	@Override
	public int compare(Abiturient ab1, Abiturient ab2) {
		return ab2.getSum() - ab1.getSum();
	}

}
