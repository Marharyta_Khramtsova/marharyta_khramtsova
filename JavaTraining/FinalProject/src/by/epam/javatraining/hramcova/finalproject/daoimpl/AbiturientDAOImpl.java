package by.epam.javatraining.hramcova.finalproject.daoimpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.dao.AbiturientDAO;
import by.epam.javatraining.hramcova.finalproject.dao.ConnectionWrapper;
import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.dto.Faculty;
import by.epam.javatraining.hramcova.finalproject.dto.Subject;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;

public class AbiturientDAOImpl extends AbiturientDAO{
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	@Override
	public void addItem(Abiturient abiturient) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_ADD_ABIT);
            statement.setString(1, abiturient.getSurname());
            statement.setString(2, abiturient.getName());
            statement.setInt(3, abiturient.getAtestate());      
            statement.setInt(4, abiturient.getFaculty().getId());
            statement.setBoolean(5, false);
            statement.executeUpdate();            
            statement.close();
            statement = connection.getStatement(DAOConstants.SQL_GET_COUNT_OF_ABIT);
            ResultSet set = statement.executeQuery();
            int idAbit = 0;
            if (set.next())
            {     
            	int count = set.getInt("count");
            	statement =  connection.getStatement(DAOConstants.SQL_GET_ID_ABIT);
            	ResultSet abitSet = statement.executeQuery();
            	int ind = 1;
            	while (abitSet.next()) {
            		if (ind == count)
            			idAbit = abitSet.getInt("id_abit");
            		else
	            		ind++;            		
				}
            	Map<Subject, Integer> marksMap = abiturient.getMarks();
	            Set<Subject> subjects = marksMap.keySet();
	            for (Subject subject : subjects)
	            {
	            	statement = connection.getStatement(DAOConstants.SQL_SET_ABIT_MARKS);
	            	statement.setInt(1, idAbit);
	            	statement.setInt(2, subject.getId());
	            	statement.setInt(3, marksMap.get(subject));
	            	statement.executeUpdate();	            	
	            }
            }
        } catch (SQLException e) {
        	e.printStackTrace();
            throw new DAOException("Error while creating a abiturient.", e);
        }        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }		
	}

	@Override
	public List<Abiturient> getAllItems() throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        
        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_GET_ALL_ABITURIENTS);
            ResultSet set = statement.executeQuery();
            List<Abiturient> abiturients = new ArrayList<Abiturient>();
            while (set.next()) {
            	Abiturient abiturient = new Abiturient();
            	abiturient.setId(set.getInt("id_abit"));
            	abiturient.setSurname(set.getString("surname_abit"));
            	abiturient.setName(set.getString("name_abit"));
            	abiturient.setAtestate(set.getInt("atestate"));
            	int idFac = set.getInt("id_fac");
            	statement = connection.getStatement(DAOConstants.SQL_GET_FAC_BY_ID);
            	statement.setInt(1, idFac);
            	ResultSet set2 = statement.executeQuery();
            	if (set2.next())
            	{
            		 
            		Faculty faculty = new Faculty(idFac, set2.getString("name_fac"),
            				set2.getInt("capacity"));
            		statement.close();
            		statement = connection.getStatement(DAOConstants.SQL_GET_SUBJECTS_BY_FAC_ID);
                    statement.setString(1, String.valueOf(idFac));
                    ResultSet set3 = statement.executeQuery();
                    List<Subject> subjects = new ArrayList<Subject>();
                    while (set3.next()) {
                    	subjects.add(new Subject(set3.getInt("id_subj"),set3.getString("name_subj")));                
                    }
                    faculty.setSubjects(subjects); 
                    abiturient.setFaculty(faculty);
                    statement.close();
                    statement = connection.getStatement(DAOConstants.SQL_GET_SUM_ABIT);
                    statement.setInt(1, abiturient.getId());
                    statement.setInt(2, abiturient.getId());
                    ResultSet setSum = statement.executeQuery();                
                    while(setSum.next())
                    {
                    	abiturient.setSum(setSum.getInt("sum"));
                    }           		
            	}
            	abiturients.add(abiturient);
            }
            return abiturients;           

        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while getting all abiturients from database.");
        }         
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}

	@Override
	public Abiturient getItemById(int id) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setApplied(int id) throws DAOException {
		setState(id, true);
		
	}
	
	public void setNotApplied(int id) throws DAOException {
		setState(id, false);
	}
	
	private void setState(int id, boolean state) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_UPDATE_ABITURIENT_STATE);
            statement.setBoolean(1, state);
            statement.setInt(2, id);
            statement.executeUpdate();    
            statement.close();            
            statement = connection.getStatement(DAOConstants.SQL_SELECT_NAME_SURNAME_FROM_ABIT);
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            if (set.next())
            {
            	if (state == true)
            	{
            	LOGGER.info("Abiturient " + set.getString("surname_abit") + 
            			" " +set.getString("name_abit") + " is accepted" );
            	}
            	else
            	{
            		LOGGER.info("Abiturient " + set.getString("surname_abit") + 
                			" " +set.getString("name_abit") + " is not accepted" );
            	}
            }
          
        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while changing abiturient acception state");
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}
	
	public List<Abiturient> getAbiturientsToDelete(int idUser) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        
        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_GET_ABITURIENTS_BY_USER);
            statement.setInt(1, idUser);
            ResultSet set = statement.executeQuery();
            List<Abiturient> abiturients = new ArrayList<Abiturient>();
            while (set.next()) {
            	Abiturient abiturient = new Abiturient();
            	abiturient.setId(set.getInt("id_abit"));
            	abiturient.setSurname(set.getString("surname_abit"));
            	abiturient.setName(set.getString("name_abit"));
            	abiturient.setAtestate(set.getInt("atestate"));
            	int idFac = set.getInt("id_fac");
            	statement = connection.getStatement(DAOConstants.SQL_GET_FAC_BY_ID);
            	statement.setInt(1, idFac);
            	ResultSet set2 = statement.executeQuery();
            	if (set2.next())
            	{            		 
            		Faculty faculty = new Faculty(idFac, set2.getString("name_fac"),
            				set2.getInt("capacity"));
            		statement.close();
            		statement = connection.getStatement(DAOConstants.SQL_GET_SUBJECTS_BY_FAC_ID);
                    statement.setString(1, String.valueOf(idFac));
                    ResultSet set3 = statement.executeQuery();
                    List<Subject> subjects = new ArrayList<Subject>();
                    while (set3.next()) {
                    	subjects.add(new Subject(set3.getInt("id_subj"),set3.getString("name_subj")));                
                    }
                    faculty.setSubjects(subjects); 
                    abiturient.setFaculty(faculty);            		
            	}
                statement.close();
                statement = connection.getStatement(DAOConstants.SQL_GET_MARKS_BY_ABIT);
                statement.setInt(1, abiturient.getId());
                ResultSet setMarks = statement.executeQuery();
                Map<Subject, Integer> mapMarks = new HashMap<Subject, Integer>();
                while(setMarks.next())
                {
                	mapMarks.put(new Subject(setMarks.getInt("id_subj"),
                			setMarks.getString("name_subj")), setMarks.getInt("mark"));
                }
                abiturient.setMarks(mapMarks);  
                statement.close();
                statement = connection.getStatement(DAOConstants.SQL_GET_SUM_ABIT);
                statement.setInt(1, abiturient.getId());
                statement.setInt(2, abiturient.getId());
                ResultSet setSum = statement.executeQuery();                
                while(setSum.next())
                {
                	abiturient.setSum(setSum.getInt("sum"));
                }
            	abiturients.add(abiturient);
            }
            return abiturients;           

        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while getting abiturients from database.");
        }         
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}
	
	@Override
	public void addItem(Abiturient abiturient, int idUser) throws DAOException {

		addItem(abiturient);
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_GET_COUNT_OF_ABIT);
            ResultSet set = statement.executeQuery();
            int idAbit = 0;
            if (set.next())
            { 
            	int count = set.getInt("count");
            	statement.close();
            	statement =  connection.getStatement(DAOConstants.SQL_GET_ID_ABIT);
        		ResultSet abitSet = statement.executeQuery();
        		int ind = 1;
        		while (abitSet.next()) {
        		if (ind == count)
        			idAbit = abitSet.getInt("id_abit");        			
        		else
        			ind++;        		
        		}            	
            } 
            statement.close();
            statement = connection.getStatement(DAOConstants.SQL_ADD_LINK_USER_ABIT);
            statement.setInt(1, idUser);
            statement.setInt(2, idAbit);           
            statement.executeUpdate(); 

        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while adding abiturient to database.");
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }		
	}
	
	@Override
	public void deleteAbiturients(int[] ids) throws DAOException {
		
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            for (int i = 0; i < ids.length; i++)
            {
            	 statement = connection.getStatement(DAOConstants.SQL_SELECT_NAME_SURNAME_FROM_ABIT);
                 statement.setInt(1, ids[i]);
                 ResultSet set = statement.executeQuery();
                 if (set.next())
                 {
                 	
                 	LOGGER.info("Abiturient " + set.getString("surname_abit") + 
                     			" " +set.getString("name_abit") + " is deleted from system" );                 	
                 }
            	statement.close();
	            statement = connection.getStatement(DAOConstants.SQL_DEL_ABITS);
	            statement.setInt(1, ids[i]);
	            statement.executeUpdate();	            
            }          
        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while deleting abiturients from database.");
        }        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }		
	}

	@Override
	public void deleteAbiturientsFromLists(int[] ids) throws DAOException {		
		
        for (int i = 0; i < ids.length; i++)
        {
            setState(ids[i], false);   
        }	
	}	
}
