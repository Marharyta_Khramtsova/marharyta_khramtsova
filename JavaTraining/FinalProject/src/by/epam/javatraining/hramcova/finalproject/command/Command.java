package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.exception.CommandException;

public interface Command {
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	public String execute(HttpServletRequest request) throws CommandException;

}
