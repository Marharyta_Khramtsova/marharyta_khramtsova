package by.epam.javatraining.hramcova.finalproject.service;

import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.daoimpl.AbiturientDAOImpl;
import by.epam.javatraining.hramcova.finalproject.daoimpl.FacultyDAOImpl;
import by.epam.javatraining.hramcova.finalproject.daoimpl.SubjectDAOImpl;
import by.epam.javatraining.hramcova.finalproject.daoimpl.UserDAOImpl;

public interface Service {
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	static UserDAOImpl userDAO = new UserDAOImpl();
	
	static FacultyDAOImpl facultyDAO = new FacultyDAOImpl();
	
	static AbiturientDAOImpl abiturientDAO = new AbiturientDAOImpl();
	
	static SubjectDAOImpl subjectDAO = new SubjectDAOImpl();
	
	
	

}
