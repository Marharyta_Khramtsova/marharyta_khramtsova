package by.epam.javatraining.hramcova.finalproject.daoimpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatraining.hramcova.finalproject.dao.ConnectionWrapper;
import by.epam.javatraining.hramcova.finalproject.dao.SubjectDAO;
import by.epam.javatraining.hramcova.finalproject.dto.Subject;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;

public class SubjectDAOImpl extends SubjectDAO{
	
	@Override
	public void addItem(Subject subject) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_ADD_SUBJ);
            statement.setString(1, subject.getName());
            statement.executeUpdate();
            
        } catch (SQLException e) {
        	e.printStackTrace();
            throw new DAOException("Error while getFac by name", e);
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
		
	}
	

	@Override
	public List<Subject> getAllItems() throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        
        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_GET_ALL_SUBJECTS);
            ResultSet set = statement.executeQuery();
            List<Subject> subjects = new ArrayList<Subject>();
            while (set.next()) {
            	Subject subject = new Subject();
            	subject.setId(set.getInt("id_subj"));
            	subject.setName(set.getString("name_subj"));
            	subjects.add(subject);
            }
            return subjects;           

        } catch (SQLException e) {
        	e.printStackTrace();
            throw new DAOException("DB problem.", e);
        }         
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}



	@Override
	public Subject getItemById(int id) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_GET_SUBJ_BY_ID);
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            Subject subject = new Subject();
            if (set.next())
            {
            	subject.setName(set.getString("name_subj"));
            }
            return subject;
          
        } catch (SQLException e) {
        	e.printStackTrace();
            throw new DAOException("Error while getSubj by name", e);
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}

}
