package by.epam.javatraining.hramcova.finalproject.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.AbiturientService;
import by.epam.javatraining.hramcova.finalproject.service.FacultyService;

public class DeleteFromListsCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) throws CommandException{
		try {
			String[] idsForDeleting = request.getParameterValues("isDeletedFromLists");
			int len = idsForDeleting.length;
			int[] ids = new int[len];		
			for (int i = 0; i < len; i++)
			{
				ids[i] = Integer.parseInt(idsForDeleting[i]);
			}			
			AbiturientService.deleteAbiturientsFromLists(ids);
			List<Abiturient> abiturients = FacultyService.getAllEnrolledAbitutientsForFaculty(
						Integer.parseInt(request.getParameter("faculty")));
			
			HttpSession session = request.getSession();
			session.setAttribute("enrolled", abiturients);
				return ConfigPagesManager.getProperty("lists");
		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
			throw new CommandException("Can not delete abiturient from lists!");
		}		
		
	}

}
