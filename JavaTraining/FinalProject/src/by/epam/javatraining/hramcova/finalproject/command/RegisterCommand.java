package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.resource.MessageManager;
import by.epam.javatraining.hramcova.finalproject.service.UserService;

public class RegisterCommand implements Command{
	
	final static Logger LOGGER = Logger.getRootLogger();

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		

		MessageManager manager = new MessageManager(request);
		
		if (!UserService.isRegisterFormValid(request))
		{
			HttpSession session = request.getSession();
			session.setAttribute("message", manager.getProperty("incorrectRegisterValues"));
			session.setAttribute("login", request.getParameter("login"));
			return ConfigPagesManager.getProperty("register");
		}
		else
		{
			if (!UserService.contains(request.getParameter("login")))
			{
				HttpSession session = request.getSession();
				session.setAttribute("message", manager.getProperty("loginExists"));
				session.setAttribute("login", request.getParameter("login"));
				return ConfigPagesManager.getProperty("register");
			}
			else {
				
					try {
						UserService.register(request);
					} catch (ServiceException e) {
						LOGGER.error(e.getMessage());
						throw new CommandException(
								"Error while registrating new user");
					}
					return ConfigPagesManager.getProperty("index");
				}
			
				
		}
	}

}
