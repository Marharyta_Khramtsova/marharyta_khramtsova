package by.epam.javatraining.hramcova.finalproject.dao;




import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;

public abstract class UserDAO extends DAOAbstract<User>{
	
	public abstract boolean contains(String login) throws DAOException;

}
