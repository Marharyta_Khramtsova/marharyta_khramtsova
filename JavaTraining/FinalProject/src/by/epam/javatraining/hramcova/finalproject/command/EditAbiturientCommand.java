package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.AbiturientService;

public class EditAbiturientCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		if ( request.getParameter("isEdited") != null)
		{
			int idAbit = Integer.parseInt(request.getParameter("isEdited"));
			Abiturient abiturient = AbiturientService.getAbiturient(idAbit);
			HttpSession session = request.getSession();
			session.setAttribute("abiturient", abiturient);
			return ConfigPagesManager.getProperty("edit_abiturient");
		}
		else
		{
			return ConfigPagesManager.getProperty("delete_edit");
		}
			
	}

}
