package by.epam.javatraining.hramcova.finalproject.dao;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.exception.PoolException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigDBManager;

public final class  ConnectionPool {
	
	final static Logger LOGGER = Logger.getRootLogger();

    private static ConnectionPool instance;

    private static final ReentrantLock LOCK = new ReentrantLock();

    private static final int POOL_SIZE = 10;
    private static final int TIME_WAIT = 1000;

    private static AtomicBoolean hasInstance = new AtomicBoolean(false);
    private static AtomicBoolean available = new AtomicBoolean(false);

    private BlockingQueue<ConnectionWrapper> freeConnections;
    private BlockingQueue<ConnectionWrapper> workingConnections;

    private static String url;
    private static String user;
    private static String password;

    static {
        url = ConfigDBManager.getProperty("url");
        user = ConfigDBManager.getProperty("user");
        password = ConfigDBManager.getProperty("password");
    }

    private ConnectionPool() {}

    public static ConnectionPool getInstance() {
        if (!hasInstance.get()) {
            try {
                if (LOCK.tryLock(TIME_WAIT, TimeUnit.MILLISECONDS)) {
                    if (instance == null) {
                        instance = new ConnectionPool();
                        hasInstance.set(true);
                    }
                    LOCK.unlock();
                }
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
            } 
        }
        return instance;
    }

    public void init() throws PoolException, ClassNotFoundException {
        try {
        	Class.forName("com.mysql.jdbc.Driver");
            freeConnections = new ArrayBlockingQueue<ConnectionWrapper>(POOL_SIZE);
            workingConnections = new ArrayBlockingQueue<ConnectionWrapper>(POOL_SIZE);
            for (int i = 0; i < POOL_SIZE; i++) {
                ConnectionWrapper connection = new ConnectionWrapper(DriverManager.getConnection(url, user, password));
                freeConnections.add(connection);
            }
            available.set(true);
        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new PoolException("Error while initializing pool");
        }
    }

    public ConnectionWrapper takeConnection(int waitTime) throws PoolException {
        ConnectionWrapper connection = null;
        try {
            if (available.get()) {
                connection = freeConnections.poll(waitTime, TimeUnit.SECONDS);
                if (connection != null)
                {
                    workingConnections.add(connection);
                    return connection;
                }
                else {
                    throw new PoolException("Connection timeout.");
                }
            }
            else {
                throw new PoolException("Pool is not initialized yet.");
            }
        } catch (InterruptedException e) {
        	LOGGER.error(e.getMessage());
            throw new PoolException("Error while connecting");
        }
    }

    public void releaseConnection(ConnectionWrapper connection) throws PoolException {
        try {
            workingConnections.remove(connection);
            freeConnections.put(connection);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            throw new PoolException("Error while disconnecting");
        }
    }

    private void clearConnectionQueue() throws SQLException {
        ConnectionWrapper connection;
        while ((connection = freeConnections.poll()) != null) {
            connection.closeConnection();
        }
        while ((connection = workingConnections.poll()) != null) {
            connection.closeConnection();
        }

    }

    public void closePool() throws PoolException {
        try {
            available.set(false);
            Thread.sleep(TIME_WAIT);
            if (instance != null) {
                instance.clearConnectionQueue();
                instance = null;
            }
        } catch (SQLException | InterruptedException e) {
            LOGGER.error(e.getMessage());
            throw new PoolException("Unable to close connection pool");
        }
    }
}