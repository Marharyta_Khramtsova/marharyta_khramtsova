package by.epam.javatraining.hramcova.finalproject.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.FacultyService;
import by.epam.javatraining.hramcova.finalproject.service.UserService;

public class AcceptApplicationCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		try {
				
			UserService.acceptApplication(request);
			List<Abiturient> abiturients = FacultyService.getAllNotEnrolledAbitutientsForFaculty(
					Integer.parseInt(request.getParameter("faculty")));				
			HttpSession session = request.getSession();
			session.setAttribute("notenrolled", abiturients);
			return ConfigPagesManager.getProperty("applications");
		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
			throw new CommandException("Can not accept application!");
		}
		
	}

}
