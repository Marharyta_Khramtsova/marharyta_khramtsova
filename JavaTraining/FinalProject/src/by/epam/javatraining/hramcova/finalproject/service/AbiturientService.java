package by.epam.javatraining.hramcova.finalproject.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.dto.Faculty;
import by.epam.javatraining.hramcova.finalproject.dto.Subject;
import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;

public class AbiturientService implements Service{	
	
	public static boolean isApplyFormValid (HttpServletRequest request) throws ServiceException {

        if (!Validator.isNameValid(request.getParameter("name"))) {
            return false;
        }
        else if (!Validator.isSurnameValid(request.getParameter("surname"))) {
        	return false;
        }
        else if (!Validator.isMarkValid(Integer.parseInt(request.getParameter("atestate")))) {
        	return false;
        }
        else
        {        	
        	try {       	
        		Faculty faculty = facultyDAO.getItemById(
        				Integer.parseInt(request.getParameter("faculty")));
				for (Subject subject : faculty.getSubjects())
				{
					int mark = Integer.parseInt(request.getParameter(String.valueOf(subject.getId())));
					if (!Validator.isMarkValid(mark))
						return false;
				}
			} catch (DAOException e) {
				LOGGER.error(e.getMessage());
				throw new ServiceException("Error with checking application form to validation");
			}
        }
        return true;      
    }
	
	public static void  apply(HttpServletRequest request) throws ServiceException {		
		
		try {
			HttpSession session = request.getSession();
			Abiturient abiturient = new Abiturient();			
			abiturient.setSurname(request.getParameter("surname"));
			abiturient.setName(request.getParameter("name"));
			Faculty faculty;
			faculty = facultyDAO.getItemById(Integer.parseInt(request.getParameter("faculty")));
			abiturient.setFaculty(faculty);
			abiturient.setAtestate(Integer.parseInt(request.getParameter("atestate")));	
			List<Subject> subjects = faculty.getSubjects();
			Map<Subject, Integer> mapToPut = new HashMap<Subject, Integer>();
			for (Subject subject : subjects)
			{
				int mark = Integer.parseInt(request.getParameter(String.valueOf(subject.getId())));
				mapToPut.put(subject, mark);					
			}
			abiturient.setMarks(mapToPut);		
			User user = (User)session.getAttribute("user");
			abiturientDAO.addItem(abiturient,user.getId());
			LOGGER.info("New abiturient " + abiturient.getSurname() + " "
			+ abiturient.getName() + " is added by " + user.getLogin() + ".");
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while adding new abiturient to database");
		} 		
	}


	public static List<Abiturient> getAllAbiturients() throws ServiceException
	{		
		List<Abiturient> abiturients;
		try {
			abiturients = abiturientDAO.getAllItems();			
			return abiturients;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting all faculties");
		}	
	}
	
	public static List<Abiturient> getAbiturientsToDelete(int idUser) throws ServiceException
	{		
		List<Abiturient> abiturients;
		try {
			abiturients = abiturientDAO.getAbiturientsToDelete(idUser);			
			return abiturients;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting abiturients for daleting");
		}	
	}
	
	public static void deleteAbiturients(int[] ids) throws ServiceException
	{		
		try {
			abiturientDAO.deleteAbiturients(ids);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while deleting abiturients");
		}	
	}
	
	public static void deleteAbiturientsFromLists(int[] ids) throws ServiceException
	{		
		try {
			abiturientDAO.deleteAbiturientsFromLists(ids);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while deleting abiturients");
		}	
	}
	public static Abiturient getAbiturient(int idAbit) throws ServiceException
	{		
		try {
			Abiturient abiturient = abiturientDAO.getItemById(idAbit);
			return abiturient;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting abiturient");
		}	
	}
}