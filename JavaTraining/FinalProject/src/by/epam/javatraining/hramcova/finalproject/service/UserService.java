package by.epam.javatraining.hramcova.finalproject.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.dto.enams.Lang;
import by.epam.javatraining.hramcova.finalproject.dto.enams.Role;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;

public class UserService implements Service{
	
	public static boolean isRegisterFormValid(HttpServletRequest request) throws ServiceException {

		String login = request.getParameter("login");
		if (!Validator.isLoginValid(login)) {
			return false;
		} else if (!Validator.isPasswordValid(request.getParameter("password"))) {
			return false;
		}
		
		return true;

	}
	
	public static boolean contains(String login) throws ServiceException {		
			try {
				if (userDAO.contains(login))
					return false;
			} catch (DAOException e) {
				LOGGER.error(e.getMessage());
				throw new ServiceException("Error: existing login");
			
		}
		return true;

	}

	public static void register(HttpServletRequest request) throws ServiceException {
		try {
			User user = new User();
			user.setLogin(request.getParameter("login"));
			user.setPassword(request.getParameter("password"));
			user.setInfo(request.getParameter("info"));
			user.setLang(Lang.EN);
			user.setRole(Role.USER);			
			userDAO.addItem(user);
			LOGGER.info("New user " + user.getLogin() + " is registered.");
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while creating new user");
		}	
	}

	
	public static boolean isLoginFormValid (HttpServletRequest request) {

	        if (!Validator.isLoginValid(request.getParameter("login"))) {
	            return false;
	        }
	        else if (!Validator.isPasswordValid(request.getParameter("password"))) {
	        	return false;
	        }
	        return true;
	      
	    }

	public static User login(HttpServletRequest request)
			throws ServiceException {
		try {
			User user = userDAO.getItem(request.getParameter("login"),
					request.getParameter("password"));
			if (user != null) {
				HttpSession session = request.getSession(true);
				session.setAttribute("user", user);
				LOGGER.info("User " + user.getLogin() + " is logged in.");
				return user;
			}
			else
				return null;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while authentificating user.");
		}

	}

	
	public static void acceptApplication(HttpServletRequest request) throws ServiceException {	
		
		try {
			String [] abitIds = request.getParameterValues("isApplied");		
			List<Abiturient> abiturients;
			abiturients = abiturientDAO.getAllItems();
			for (Abiturient abiturient : abiturients)
			{
				for (int i = 0; i < abitIds.length; i++)
				{
					if (Integer.parseInt(abitIds[i]) == abiturient.getId())
					{
						abiturient.setApplied(true);
						abiturientDAO.setApplied(abiturient.getId());					
					}
				}
			}
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while accepting application");
		}	
		
	}
	public static List<User> getAllUsers() throws ServiceException
	{		
		List<User> users;
		try {
			users = userDAO.getAllItems();			
			return users;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting all users");
		}	
	}
	
	public static void setAdmin(int idUser) throws ServiceException
	{		
		try {
			userDAO.setAdmin(idUser);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while access to database");
		}	
	}
	
	public static void setNotAdmin(int idUser) throws ServiceException
	{		
		try {
			userDAO.setNotAdmin(idUser);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while access to database");
		}	
	}
	
	public static void setLang(Lang lang, int idUser) throws ServiceException
	{		
		try {
			userDAO.setLang(lang, idUser);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while access to database");
		}	
	}
	
}
