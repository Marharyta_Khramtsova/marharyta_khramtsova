package by.epam.javatraining.hramcova.finalproject.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.dto.Faculty;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.FacultyService;

public class ShowListsCommand implements Command{	
	
	@Override
	public String execute(HttpServletRequest request) throws CommandException{
		HttpSession session = request.getSession();
		try {
			if (request.getParameter("faculty") != null)
			{				
				List<Abiturient> abiturients = FacultyService.getAllEnrolledAbitutientsForFaculty(
							Integer.parseInt(request.getParameter("faculty")));			
				session.setAttribute("enrolled", abiturients);
			}
			else
			{
				List<Faculty> faculties = FacultyService.getAllFaculties();				
				session.setAttribute("faculties", faculties);
				session.removeAttribute("enrolled");
			}
		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
			throw new CommandException("Can not show lists!");
		}
		return ConfigPagesManager.getProperty("lists");
	}

}
