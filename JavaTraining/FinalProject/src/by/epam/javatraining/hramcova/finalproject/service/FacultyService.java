package by.epam.javatraining.hramcova.finalproject.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.dto.Faculty;
import by.epam.javatraining.hramcova.finalproject.dto.Subject;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;

public class FacultyService implements Service{
	
	public static List<Faculty> getAllFaculties() throws ServiceException
	{		
		List<Faculty> faculties;
		try {
			faculties = facultyDAO.getAllItems();			
			return faculties;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting all faculties");
		}	
	}
	
	public static void addFaculty(HttpServletRequest request) throws ServiceException {		
		
		try {
			String nameFac = request.getParameter("faculty_name");
			int capacityFac = Integer.parseInt(request.getParameter("faculty_capacity"));
			String [] subjectsIds = request.getParameterValues("subject");
			Faculty faculty = new Faculty();
			faculty.setName(nameFac);
			faculty.setCapacity(capacityFac);
			List<Subject> subjectsToSet = new ArrayList<Subject>();	
			List<Subject> subjects;
			subjects = subjectDAO.getAllItems();
			for (Subject subject : subjects)
			{
				for (int i = 0; i < subjectsIds.length; i++)
				{
					if (Integer.parseInt(subjectsIds[i]) == subject.getId())
					{
						subjectsToSet.add(subject);
					}
				}
			}
			faculty.setSubjects(subjectsToSet);
			facultyDAO.addItem(faculty);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while adding new faculty");			
		}		
	}

	public static List<Abiturient> getAllNotEnrolledAbitutientsForFaculty(int idFac) throws ServiceException
	{
		try {
			List<Abiturient> abiturients = facultyDAO.getAllNotEnrolledAbitutientsForFaculty(idFac);
			return abiturients;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting not enrolled abiturients");
		}
		
	}
	
	public static List<Abiturient> getAllEnrolledAbitutientsForFaculty(int idFac) throws ServiceException
	{
		try {
			List<Abiturient> abiturients = facultyDAO.getAllEnrolledAbitutientsForFaculty(idFac, true);
			return abiturients;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting enrolled abiturients");
		}
		
	}
	
	public static List<Abiturient> getAllAcceptedApplicationsForFaculty(int idFac) throws ServiceException
	{
		try {
			List<Abiturient> abiturients = facultyDAO.getAllEnrolledAbitutientsForFaculty(idFac, false);
			return abiturients;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting accepted applications");
		}
		
	}

}
