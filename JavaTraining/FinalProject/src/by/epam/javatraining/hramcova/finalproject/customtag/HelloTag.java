package by.epam.javatraining.hramcova.finalproject.customtag;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class HelloTag extends SimpleTagSupport {

	private String login;

	public void setLogin(String msg) {
		this.login = msg;
	}

	

	public void doTag() throws JspException, IOException {
		if (login != null) {
			
			StringWriter sw = new StringWriter();
			JspWriter out = getJspContext().getOut();

			getJspBody().invoke(sw);
			out.print(sw.toString());
			out.println(login);

		}
	}

}
