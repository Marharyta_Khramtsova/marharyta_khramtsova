package by.epam.javatraining.hramcova.finalproject.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Abiturient implements Serializable{
	
	
	private static final long serialVersionUID = 1L;

	private int id;
	
	private String surname;
	
	private String name;
	
	private Faculty faculty;
	
	private int atestate;
	
	private Map<Subject, Integer> marks;
	
	private int sum;
	
	private boolean isApplied;	
	
	public boolean isApplied() {
		return isApplied;
	}

	public void setApplied(boolean isApplied) {
		this.isApplied = isApplied;
	}

	public Abiturient() {
		marks = new HashMap<Subject, Integer>();		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public int getAtestate() {
		return atestate;
	}

	public void setAtestate(int atestate) {
		this.atestate = atestate;
	}

	public Map<Subject, Integer> getMarks() {
		return marks;
	}

	public void setMarks(Map<Subject, Integer> marks) {
		this.marks = marks;
	}
	
	public int getSum() {
		return sum;		
	}
	
	public void setSum(int sum) {
		this.sum = sum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + atestate;
		result = prime * result + ((faculty == null) ? 0 : faculty.hashCode());
		result = prime * result + id;
		result = prime * result + (isApplied ? 1231 : 1237);
		result = prime * result + ((marks == null) ? 0 : marks.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + sum;
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Abiturient other = (Abiturient) obj;
		if (atestate != other.atestate)
			return false;
		if (faculty == null) {
			if (other.faculty != null)
				return false;
		} else if (!faculty.equals(other.faculty))
			return false;
		if (id != other.id)
			return false;
		if (isApplied != other.isApplied)
			return false;
		if (marks == null) {
			if (other.marks != null)
				return false;
		} else if (!marks.equals(other.marks))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sum != other.sum)
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Abiturient: surname=" + surname + ", name="
				+ name + ", faculty=" + faculty + ", atestate=" + atestate
				+ ", marks=" + marks + ", sum=" + sum + ", isApplied="
				+ isApplied + ".";
	}
}
