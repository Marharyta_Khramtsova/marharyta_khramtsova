package by.epam.javatraining.hramcova.finalproject.service;

public class Validator {
	public static boolean isLoginValid(String login)
	{
		if (login != null && !login.isEmpty() && login.length() >= 6)
		{			
			return true;		
		}
		else
			return false;
		
	}
	
	public static boolean isPasswordValid(String pass)
	{
		if (pass != null && !pass.isEmpty() && pass.length() >= 6)
			return true;
		else
			return false;
	}
	
	public static boolean isSurnameValid(String surname)
	{
		if (surname != null && !surname.isEmpty())
			return true;
		else
			return false;
	}
	
	public static boolean isNameValid(String name)
	{
		if (name != null && !name.isEmpty())
			return true;
		else
			return false;
	}
	
	public static boolean isMarkValid(int mark)
	{
		if (mark > 0 && mark <= 100)
			return true;
		else
			return false;
	}

}
