package by.epam.javatraining.hramcova.finalproject.resource;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

public class MessageManager {
	 
	    private static ResourceBundle resourceBundle;
	    
	    public MessageManager(HttpServletRequest request) {
	        String locale = (String) request.getSession().getAttribute("locale");
	        if (locale == null) {
	            resourceBundle = ResourceBundle.getBundle("message", new Locale("ru", "RUS"));
	        }
	        else {
	            String[] localeArr = locale.split("_");
	            resourceBundle = ResourceBundle.getBundle("message", new Locale(localeArr[0], localeArr[1]));
	        }
	    }

	    public String getProperty(String key) {
	        return resourceBundle.getString(key);
	    }

}
