package by.epam.javatraining.hramcova.finalproject.resource;

import java.util.ResourceBundle;

public class ConfigPagesManager {
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.pages");
	private ConfigPagesManager() { }
	    public static String getProperty(String key) {
	        return resourceBundle.getString(key);
	    }
}