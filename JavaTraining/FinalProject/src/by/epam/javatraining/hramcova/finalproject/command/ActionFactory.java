package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {
	
	public Command defineCommand(HttpServletRequest request)
	{
		Command currentCommand;
		String command = request.getParameter("command");
		if (command == null)
			return new EmptyCommand();
		CommandEnum currentCommandEnum = CommandEnum.valueOf(command.toUpperCase());
		currentCommand = currentCommandEnum.getCurrentCommand();
		return currentCommand;
		
	}

}
