package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;






import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.dto.enams.Lang;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.service.UserService;

public class ChangeLanguageCommand implements Command{
	
	private String language;	

	public ChangeLanguageCommand(String language) {
		super();
		this.language = language;
	}

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		HttpSession session = request.getSession();
		session.setAttribute("locale", language);
		User user = (User) session.getAttribute("user");
		if (user != null)
		{
			if (request.getParameter("lang").toUpperCase().equals("EN"))
				UserService.setLang(Lang.EN, user.getId());
			else
				UserService.setLang(Lang.RU, user.getId());
		}
		return null;
	}

}
