package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.UserService;

public class LogoffCommand implements Command{
	
	@Override
	public String execute(HttpServletRequest request){
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		LOGGER.info("User " + user.getLogin() + " is logged off.");		
		return ConfigPagesManager.getProperty("index");
	}

}
