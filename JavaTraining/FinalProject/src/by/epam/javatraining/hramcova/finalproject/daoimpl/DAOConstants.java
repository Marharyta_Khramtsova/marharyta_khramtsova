package by.epam.javatraining.hramcova.finalproject.daoimpl;

public class DAOConstants {
	
	static final String SQL_ADD_ABIT ="INSERT INTO abiturient"
			+ " (surname_abit, name_abit, atestate, id_fac, is_enrolled)"
			+ " VALUES (?, ?, ?, ?, ?);";
	static final String SQL_GET_ABIT_BY_ID ="SELECT id_abit FROM abiturient"
			+ " WHERE name_abit = ?;";
	static final String SQL_SET_ABIT_MARKS ="INSERT INTO marks "
			+ "(id_abit,id_subj,mark) VALUES (?,?,?);";
	static final String SQL_GET_ALL_ABITURIENTS = "SELECT * FROM abiturient";
	static final String SQL_GET_ID_ABIT = "SELECT id_abit FROM abiturient";
	static final String SQL_GET_COUNT_OF_ABIT = "SELECT count(*) AS count FROM abiturient";
	static final String SQL_UPDATE_ABITURIENT_STATE = "UPDATE abiturient"
			+ " SET is_enrolled = ? WHERE id_abit = ?";
	
	static final String SQL_GET_ALL_FACULTIES = "SELECT * FROM faculty";
	static final String SQL_GET_SUBJECTS_BY_FAC_ID = "select subject.id_subj,name_subj"
			+ " from subject inner join link_fac_subj"
			+ " on subject.id_subj = link_fac_subj.id_subj where id_fac = ?;";
	static final String SQL_GET_FAC_BY_ID ="SELECT * FROM faculty"
			+ " WHERE id_fac = ?;";
	static final String SQL_GET_ALL_ABITURIENTS_BY_FAC = "SELECT * FROM abiturient"
			+ " WHERE id_fac = ? AND is_enrolled = ?";
	static final String SQL_GET_MARKS_BY_ABIT ="SELECT subject.id_subj, name_subj, mark "
			+ "FROM marks inner join subject "
			+ "on subject.id_subj = marks.id_subj "
			+ "WHERE id_abit = ?;";
	
	static final String SQL_ADD_FAC ="INSERT INTO faculty "
			+ "(name_fac,capacity) values (?,?);";
	
	static final String SQL_ADD_LINK ="INSERT INTO link_fac_subj "
			+ "(id_fac,id_subj) values (?,?);";
	
	static final String SQL_GET_ID_FAC_BY_NAME ="SELECT id_fac FROM faculty WHERE name_fac = ?";
	
	static final String SQL_GET_ALL_SUBJECTS = "SELECT * FROM subject";
	static final String SQL_GET_SUBJ_BY_ID ="SELECT name_subj FROM subject"
			+ " WHERE id_subj = ?;";
	static final String SQL_ADD_SUBJ ="INSERT INTO subject (name_subj) VALUES (?);";
	
	static final String SQL_GET_USER = "SELECT * FROM user WHERE login = ? AND pass = ?;";
	static final String SQL_ADD_USER ="INSERT INTO user (login, pass, info, lang, role)"
	+ " VALUES (?, ?, ?, ?, ?);";
	
	static final String SQL_ADD_LINK_USER_ABIT ="INSERT INTO link_user_abit "
			+ "(id_user,id_abit) values (?,?);";
	static final String SQL_GET_ABITURIENTS_BY_USER = "SELECT *"
			+ " FROM abiturient INNER JOIN link_user_abit"
			+ " ON abiturient.id_abit = link_user_abit.id_abit WHERE id_user = ?";
	static final String SQL_DEL_ABITS ="DELETE FROM abiturient WHERE id_abit = ?";
	static final String SQL_GET_SUM_ABIT ="SELECT sum(mark) + abiturient.atestate AS sum FROM marks,abiturient WHERE marks.id_abit = ? AND abiturient.id_abit = ?";
	static final String SQL_SELECT_LOGINS ="SELECT login FROM user";
	static final String SQL_SELECT_NAME_SURNAME_FROM_ABIT ="SELECT surname_abit,"
			+ " name_abit FROM abiturient WHERE id_abit = ?";
	static final String SQL_GET_ALL_USERS = "SELECT * FROM user";
	static final String SQL_SET_USER_ROLE = "UPDATE user SET role = ? WHERE id = ?";
	static final String SQL_SET_USER_LANG = "UPDATE user SET lang = ? WHERE id = ?";
	
	
	
}
