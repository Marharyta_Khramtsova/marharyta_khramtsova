package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;



import by.epam.javatraining.hramcova.finalproject.dto.Subject;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.SubjectService;

public class AddSubjectCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) throws CommandException{
		try {
			if (request.getParameter("subject_name") != null &&
					!request.getParameter("subject_name").equals(""))
			{
				Subject subject = new Subject();
				subject.setName(request.getParameter("subject_name"));				
				SubjectService.addSubject(subject);
						
			}
		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
			throw new CommandException("Can not add new subject!");
		}	
		return ConfigPagesManager.getProperty("add_subject");		
	}

}
