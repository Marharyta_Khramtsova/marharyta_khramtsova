package by.epam.javatraining.hramcova.finalproject.service;

import java.util.List;

import by.epam.javatraining.hramcova.finalproject.dto.Subject;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;

public class SubjectService implements Service{
	
	public static List<Subject> getAllSubjects() throws ServiceException {
		try {
			List<Subject> subjects = subjectDAO.getAllItems();
			return subjects;
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while getting all subjects");
		}		
	}
	
	public static void addSubject(Subject subject) throws ServiceException {		
		try {
			subjectDAO.addItem(subject);
		} catch (DAOException e) {
			LOGGER.error(e.getMessage());
			throw new ServiceException("Error while adding new subject");
		}
	
	}

}
