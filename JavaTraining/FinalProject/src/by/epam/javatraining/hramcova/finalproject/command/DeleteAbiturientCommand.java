package by.epam.javatraining.hramcova.finalproject.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.dto.enams.Role;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.AbiturientService;

public class DeleteAbiturientCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		try {
			if (request.getParameterValues("isDeleted") == null)
			{
				List<Abiturient> abiturients = null;
				HttpSession session = request.getSession();
				User user = (User) session.getAttribute("user");
				if (user.getRole() == Role.ADMIN)
				{					
					abiturients = AbiturientService.getAllAbiturients();					
				}
				else if (user.getRole() == Role.USER)
				{
					abiturients = AbiturientService.getAbiturientsToDelete(user.getId());
				}
				session.setAttribute("abiturientsToDelete", abiturients);
				return ConfigPagesManager.getProperty("delete");
			}
			else 
			{
				String[] idsForDeleting = request.getParameterValues("isDeleted");
				int len = idsForDeleting.length;
				int[] ids = new int[len];
				
				for (int i = 0; i < len; i++)
				{
					ids[i] = Integer.parseInt(idsForDeleting[i]);
				}			
				AbiturientService.deleteAbiturients(ids);
				return ConfigPagesManager.getProperty("main");
			}
		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
			throw new CommandException("Can not delete abiturient!");
		}
	}

}
