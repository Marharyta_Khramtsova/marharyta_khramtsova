package by.epam.javatraining.hramcova.finalproject.daoimpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.dao.ConnectionWrapper;
import by.epam.javatraining.hramcova.finalproject.dao.UserDAO;
import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.dto.enams.Lang;
import by.epam.javatraining.hramcova.finalproject.dto.enams.Role;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;

public class UserDAOImpl extends UserDAO{
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	public User getItem(String login, String password) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_GET_USER);
            statement.setString(1, login);
            statement.setString(2, password);  
            ResultSet set = statement.executeQuery();
            if (set.next()) {
            	User userToReturn = new User();
            	userToReturn.setId(set.getInt("id"));
            	userToReturn.setLogin(set.getString("login"));
            	userToReturn.setPassword(set.getString("pass"));
            	userToReturn.setInfo(set.getString("info"));
            	if (set.getString("lang").equals("EN"))
            		userToReturn.setLang(Lang.EN);
            	else
                   	userToReturn.setLang(Lang.RU);
            	if (set.getString("role").equals("1")) //исправить
            		userToReturn.setRole(Role.USER);
            	else
                   	userToReturn.setRole(Role.ADMIN);            	
            	return userToReturn;                
            } else {
                return null;
            }      

        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error with getting user from database");
        }         
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }		
	}

	@Override
	public void addItem(User user) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_ADD_USER);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getInfo());
            statement.setString(4, user.getLang().toString());
            statement.setString(5,String.valueOf(1)); // исправить
            statement.executeUpdate(); 

        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while adding user to database.", e);
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}

	@Override
	public List<User> getAllItems() throws DAOException {
		ConnectionWrapper connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			statement = connection
					.getStatement(DAOConstants.SQL_GET_ALL_USERS);
			ResultSet set = statement.executeQuery();
			List<User> users = new ArrayList<User>();
			while (set.next()) {
				User user = new User();
				user.setId(set.getInt("id"));
				user.setLogin(set.getString("login"));
				user.setPassword(set.getString("pass"));
				user.setInfo(set.getString("info"));
				if (set.getString("lang").equals("EN"))
            		user.setLang(Lang.EN);
            	else
            		user.setLang(Lang.RU);
            	if (set.getString("role").equals("1")) 
            		user.setRole(Role.USER);
            	else
            		user.setRole(Role.ADMIN);				
				users.add(user);
			}
			return users;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(
					"Error while getting users from database.");
		} finally {
			connection.closeStatement(statement);
			returnConnection(connection);
		}
	}

	@Override
	public User getItemById(int id) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	public void setAbitToUser(int idAbit, int idUser) throws DAOException
	{
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_ADD_LINK_USER_ABIT);
            statement.setInt(1, idUser);
            statement.setInt(2, idAbit);           
            statement.executeUpdate(); 

        } catch (SQLException e) {
        	e.printStackTrace();
            throw new DAOException("Error", e);
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}

	@Override
	public boolean contains(String login) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_SELECT_LOGINS);
            ResultSet set = statement.executeQuery();
            while (set.next())
            {
            	if (login.equals(set.getString("login")))
            		return true;
            }
            return false;

        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while access to database", e);
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}
	
	public void setAdmin(int idUser) throws DAOException {
		setState(Role.ADMIN, idUser);
		
	}
	
	public void setNotAdmin(int idUser) throws DAOException {
		setState(Role.USER, idUser);
		
	}
	
	private void setState(Role role, int idUser) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_SET_USER_ROLE);
            if (role == Role.USER)
            	statement.setInt(1, 1);
            else
            	statement.setInt(1, 2);
            statement.setInt(2, idUser);
            statement.executeUpdate();      

        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while access to database", e);
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}
	
	public void setLang(Lang lang, int idUser) throws DAOException {
		ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(DAOConstants.SQL_SET_USER_LANG);
            if (lang == Lang.EN)
            	statement.setString(1, "EN");
            else
            	statement.setString(1, "RU");
            statement.setInt(2, idUser);
            statement.executeUpdate();      

        } catch (SQLException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException("Error while access to database", e);
        }
        
		finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
	}

	

}
