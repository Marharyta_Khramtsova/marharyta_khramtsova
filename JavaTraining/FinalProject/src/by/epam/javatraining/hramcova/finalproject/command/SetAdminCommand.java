package by.epam.javatraining.hramcova.finalproject.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.UserService;

public class SetAdminCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		if (request.getParameter("idUser") != null)
		{
			UserService.setAdmin(Integer.parseInt(request.getParameter("idUser")));
		}
		HttpSession session = request.getSession();
		List<User> users = UserService.getAllUsers();
		users.remove((User)session.getAttribute("user"));		
		session.setAttribute("users", users);
		return ConfigPagesManager.getProperty("users");
	}

}
