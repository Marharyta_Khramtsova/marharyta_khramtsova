package by.epam.javatraining.hramcova.finalproject.resource;

import java.util.ResourceBundle;

public class ConfigDBManager {
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.db");
	private ConfigDBManager() { }
	    public static String getProperty(String key) {
	        return resourceBundle.getString(key);
	    }

}
