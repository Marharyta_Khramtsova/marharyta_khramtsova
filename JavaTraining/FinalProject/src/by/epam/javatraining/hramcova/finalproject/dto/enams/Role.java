package by.epam.javatraining.hramcova.finalproject.dto.enams;

public enum Role {
	
	USER,
	ADMIN;

}
