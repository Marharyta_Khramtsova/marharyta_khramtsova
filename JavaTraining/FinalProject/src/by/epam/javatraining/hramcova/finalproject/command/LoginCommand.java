package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.User;
import by.epam.javatraining.hramcova.finalproject.dto.enams.Lang;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.UserService;

public class LoginCommand implements Command{
	
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		if (!UserService.isLoginFormValid(request))
			return ConfigPagesManager.getProperty("login");
		else
		{
			try {
				User user = UserService.login(request);
				if (user != null)
				{
					HttpSession session = request.getSession();
					session.setAttribute("user", user);
					if (user.getLang() == Lang.EN)
						session.setAttribute("locale", "en_US");
					else
						session.setAttribute("locale", "ru_RUS");
				}
			} catch (ServiceException e) {
				LOGGER.error(e.getMessage());
				throw new CommandException("Error while logging user");
			}
			return ConfigPagesManager.getProperty("main");							
		}
	}

}
