package by.epam.javatraining.hramcova.finalproject.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.command.ActionFactory;
import by.epam.javatraining.hramcova.finalproject.command.Command;
import by.epam.javatraining.hramcova.finalproject.dao.ConnectionPool;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.PoolException;

/**
 * Servlet implementation class Servlet
 */
@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet() {
    }
    
    @Override
    public void init() throws ServletException
    {       
        try {
            ConnectionPool.getInstance().init();
        } catch (PoolException e) { 
        	LOGGER.error(e.getMessage());
            throw new ExceptionInInitializerError(e);
        } catch (ClassNotFoundException e) {
        	LOGGER.error(e.getMessage());
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			process(request, response);
		
	}
	
	 private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
	        String page = null;
	        ActionFactory client = new ActionFactory();
	        Command command = client.defineCommand(request);
	        try {
	            page = command.execute(request);
	            if (page != null) {
	                request.getRequestDispatcher(page).forward(request, response);	               
	            }
	        } catch (CommandException e) {
	            LOGGER.error(e.getMessage());
	            request.getRequestDispatcher("error").forward(request, response);
	        }
	    }
	 
	 @Override
	    public void destroy()
	    {
	        try {
				ConnectionPool.getInstance().closePool();
			} catch (PoolException e) {				
				LOGGER.error(e.getMessage());
			}
	    }


}
