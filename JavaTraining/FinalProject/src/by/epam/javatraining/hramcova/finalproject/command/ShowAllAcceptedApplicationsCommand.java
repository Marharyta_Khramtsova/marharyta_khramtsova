package by.epam.javatraining.hramcova.finalproject.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.FacultyService;

public class ShowAllAcceptedApplicationsCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		List<Abiturient> abiturients;
		try {
			abiturients = FacultyService.getAllAcceptedApplicationsForFaculty(
					Integer.parseInt(request.getParameter("faculty")));
			session.setAttribute("enrolled", abiturients);
		}
		catch (ServiceException e) {
			LOGGER.error("Can not get accepted abiturients!");
			e.printStackTrace();
		}			
	
	return ConfigPagesManager.getProperty("lists");
	}

}
