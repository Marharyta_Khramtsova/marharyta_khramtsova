package by.epam.javatraining.hramcova.finalproject.daoimpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.dao.ConnectionWrapper;
import by.epam.javatraining.hramcova.finalproject.dao.FacultyDAO;
import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.dto.AbiturientComparator;
import by.epam.javatraining.hramcova.finalproject.dto.Faculty;
import by.epam.javatraining.hramcova.finalproject.dto.Subject;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;

public class FacultyDAOImpl extends FacultyDAO {

	final static Logger LOGGER = Logger.getRootLogger();

	@Override
	public void addItem(Faculty faculty) throws DAOException {
		ConnectionWrapper connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			statement = connection.getStatement(DAOConstants.SQL_ADD_FAC);
			statement.setString(1, faculty.getName());
			statement.setInt(2, faculty.getCapacity());
			statement.executeUpdate();
			statement.close();

			statement = connection
					.getStatement(DAOConstants.SQL_GET_ID_FAC_BY_NAME);
			statement.setString(1, faculty.getName());
			ResultSet set = statement.executeQuery();
			if (set.next()) {
				for (Subject subject : faculty.getSubjects()) {
					statement = connection
							.getStatement(DAOConstants.SQL_ADD_LINK);
					statement.setInt(1, set.getInt("id_fac"));
					statement.setInt(2, subject.getId());
					statement.executeUpdate();
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException("Error while adding new faculty to database", e);
		}

		finally {
			connection.closeStatement(statement);
			returnConnection(connection);
		}

	}

	@Override
	public List<Faculty> getAllItems() throws DAOException {
		ConnectionWrapper connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			statement = connection
					.getStatement(DAOConstants.SQL_GET_ALL_FACULTIES);
			ResultSet set = statement.executeQuery();
			List<Faculty> faculties = new ArrayList<Faculty>();
			while (set.next()) {
				Faculty faculty = new Faculty();
				faculty.setId(set.getInt("id_fac"));
				faculty.setName(set.getString("name_fac"));
				faculty.setCapacity(set.getInt("capacity"));
				statement = connection
						.getStatement(DAOConstants.SQL_GET_SUBJECTS_BY_FAC_ID);
				statement.setString(1, String.valueOf(faculty.getId()));
				ResultSet setSubj = statement.executeQuery();
				List<Subject> subjects = new ArrayList<Subject>();
				while (setSubj.next()) {
					subjects.add(new Subject(setSubj.getInt("id_subj"), setSubj
							.getString("name_subj")));
				}
				faculty.setSubjects(subjects);
				faculties.add(faculty);
			}
			return faculties;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(
					"Error while getting faculties from database.", e);
		} finally {
			connection.closeStatement(statement);
			returnConnection(connection);
		}
	}

	@Override
	public Faculty getItemById(int idFac) throws DAOException {
		ConnectionWrapper connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			statement = connection.getStatement(DAOConstants.SQL_GET_FAC_BY_ID);
			statement.setInt(1, idFac);
			ResultSet setFac = statement.executeQuery();
			Faculty faculty = new Faculty();
			faculty.setId(idFac);
			if (setFac.next()) {
				faculty.setName(setFac.getString("id_fac"));
				faculty.setCapacity(setFac.getInt("capacity"));
			}
			statement.close();
			statement = connection
					.getStatement(DAOConstants.SQL_GET_SUBJECTS_BY_FAC_ID);
			statement.setString(1, String.valueOf(idFac));
			ResultSet setSubj = statement.executeQuery();
			List<Subject> subjects = new ArrayList<Subject>();
			while (setSubj.next()) {
				subjects.add(new Subject(setSubj.getInt("id_subj"), setSubj
						.getString("name_subj")));
			}
			faculty.setSubjects(subjects);
			return faculty;

		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw new DAOException(
					"Error while getting faculty from database.", e);
		} finally {
			connection.closeStatement(statement);
			returnConnection(connection);
		}
	}

	public List<Abiturient> getAllEnrolledAbitutientsForFaculty(int idFac, boolean isInList)
			throws DAOException {
		return getAllAbitutientsForFaculty(idFac, true, isInList);
	}

	public List<Abiturient> getAllNotEnrolledAbitutientsForFaculty(int idFac)
			throws DAOException {
		return getAllAbitutientsForFaculty(idFac, false, false);
	}

	private List<Abiturient> getAllAbitutientsForFaculty(int idFac,
			boolean isEnrolled, boolean isInList) throws DAOException {
		ConnectionWrapper connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			statement = connection
					.getStatement(DAOConstants.SQL_GET_ALL_ABITURIENTS_BY_FAC);
			statement.setInt(1, idFac);
			statement.setBoolean(2, isEnrolled);
			ResultSet set = statement.executeQuery();
			List<Abiturient> abiturients = new ArrayList<Abiturient>();
			int capacity = 0;
			String nameFac = null;
			while (set.next()) {

				Abiturient abiturient = new Abiturient();
				abiturient.setId(set.getInt("id_abit"));
				abiturient.setSurname(set.getString("surname_abit"));
				abiturient.setName(set.getString("name_abit"));
				abiturient.setAtestate(set.getInt("atestate"));
				statement = connection
						.getStatement(DAOConstants.SQL_GET_FAC_BY_ID);
				statement.setInt(1, idFac);
				ResultSet setFac = statement.executeQuery();
				if (setFac.next()) {
					capacity = setFac.getInt("capacity");
					nameFac = setFac.getString("name_fac");
					statement.close();
					statement = connection
							.getStatement(DAOConstants.SQL_GET_SUBJECTS_BY_FAC_ID);
					statement.setString(1, String.valueOf(idFac));
					ResultSet setSubj = statement.executeQuery();
					List<Subject> subjects = new ArrayList<Subject>();
					while (setSubj.next()) {
						subjects.add(new Subject(setSubj.getInt("id_subj"),
								setSubj.getString("name_subj")));
					}
					abiturient.setFaculty(new Faculty(idFac, nameFac, capacity, subjects));
				}
				statement.close();
				statement = connection
						.getStatement(DAOConstants.SQL_GET_MARKS_BY_ABIT);
				statement.setInt(1, abiturient.getId());
				ResultSet setMarks = statement.executeQuery();
				Map<Subject, Integer> mapMarks = new HashMap<Subject, Integer>();
				while (setMarks.next()) {
					mapMarks.put(new Subject(setMarks.getInt("id_subj"),
							setMarks.getString("name_subj")), setMarks
							.getInt("mark"));
				}
				abiturient.setMarks(mapMarks);
				statement.close();
				statement = connection
						.getStatement(DAOConstants.SQL_GET_SUM_ABIT);
				statement.setInt(1, abiturient.getId());
				statement.setInt(2, abiturient.getId());
				ResultSet setSum = statement.executeQuery();
				while (setSum.next()) {
					abiturient.setSum(setSum.getInt("sum"));
				}
				abiturients.add(abiturient);
			}
			Collections.sort(abiturients, new AbiturientComparator());
			if (isEnrolled == true && isInList == true) {
				abiturients = abiturients.subList(0,
						Math.min(capacity, abiturients.size()));
			}
			return abiturients;

		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw new DAOException("Error while getting all abirutients for faculty.", e);
		} finally {
			connection.closeStatement(statement);
			returnConnection(connection);
		}
	}
}
