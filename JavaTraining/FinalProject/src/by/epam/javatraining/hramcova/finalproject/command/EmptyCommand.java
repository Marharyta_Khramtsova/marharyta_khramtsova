package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;


import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;

public class EmptyCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("locale") == null)
			session.setAttribute("locale", "ru_RUS");
		return ConfigPagesManager.getProperty("index");
	}

}
