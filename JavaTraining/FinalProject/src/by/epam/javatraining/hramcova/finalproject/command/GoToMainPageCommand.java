package by.epam.javatraining.hramcova.finalproject.command;

import javax.servlet.http.HttpServletRequest;


import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;

public class GoToMainPageCommand implements Command{

	@Override
	public String execute(HttpServletRequest request){
		
		return  ConfigPagesManager.getProperty("main");
	}

}
