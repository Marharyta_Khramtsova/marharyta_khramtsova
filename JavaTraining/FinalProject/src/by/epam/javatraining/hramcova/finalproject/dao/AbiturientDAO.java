package by.epam.javatraining.hramcova.finalproject.dao;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.exception.DAOException;

public abstract class AbiturientDAO extends DAOAbstract<Abiturient>{
	
	
	public abstract void addItem(Abiturient item, int idUser)  throws DAOException;
	
	public abstract  void deleteAbiturients(int[] ids) throws DAOException;
	
	public abstract void deleteAbiturientsFromLists(int[] ids) throws DAOException;

}
