package by.epam.javatraining.hramcova.finalproject.dao;

import java.util.List;

import org.apache.log4j.Logger;

import by.epam.javatraining.hramcova.finalproject.exception.DAOException;
import by.epam.javatraining.hramcova.finalproject.exception.PoolException;

public abstract class DAOAbstract<T> {
	
	final static Logger LOGGER = Logger.getRootLogger();
	
	private static final int WAIT_TIME_SEC = 10;	
	
	//abstract public T getItem(String login, String password) throws DAOException;
	
	abstract public T getItemById(int id) throws DAOException;
	
	abstract public void addItem(T item) throws DAOException;
	
	abstract public List<T> getAllItems() throws DAOException;	

    public ConnectionWrapper getConnection() throws DAOException {
        try {
            return ConnectionPool.getInstance().takeConnection(WAIT_TIME_SEC);
        } catch (PoolException e) {
        	LOGGER.error(e.getMessage());
            throw new DAOException(e);
        }
    }

    public void returnConnection(ConnectionWrapper connection) throws DAOException {
        try {
			ConnectionPool.getInstance().releaseConnection(connection);
		} catch (PoolException e) {
			LOGGER.error(e.getMessage());
			throw new DAOException(e);
		}
    }

}
