package by.epam.javatraining.hramcova.finalproject.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.Faculty;
import by.epam.javatraining.hramcova.finalproject.dto.Subject;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.AbiturientService;
import by.epam.javatraining.hramcova.finalproject.service.FacultyService;
import by.epam.javatraining.hramcova.finalproject.service.SubjectService;

public class ApplyCommand implements Command{
	
		
	@Override
	public String execute(HttpServletRequest request) throws CommandException{		
		
		try {
			if (!AbiturientService.isApplyFormValid(request))
			{
				List<Faculty> faculties = FacultyService.getAllFaculties();	
				List<Subject> subjects = SubjectService.getAllSubjects();
				HttpSession session = request.getSession();
				session.setAttribute("faculties", faculties);
				session.setAttribute("subjects", subjects);
				return ConfigPagesManager.getProperty("apply");
			}
			else
			{			
				AbiturientService.apply(request);
				return ConfigPagesManager.getProperty("applications");				
			}
		}
		catch (ServiceException e) {
			LOGGER.error(e.getMessage());
			throw new CommandException("Error while creating new abiturient");
		}
	}

}
