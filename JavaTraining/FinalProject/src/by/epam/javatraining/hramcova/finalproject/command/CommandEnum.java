package by.epam.javatraining.hramcova.finalproject.command;

public enum CommandEnum {
	
	LOGIN
	{
		{
			this.command = new LoginCommand();
		}
	},
	LOGOFF
	{
		{
			this.command = new LogoffCommand();
		}
	},
	REGISTER
	{
		{
			this.command = new RegisterCommand();
		}
		
	},
	EMPTY
	{
		{
			this.command = new EmptyCommand();
		}
		
	},
	SHOWLISTS
	{
		{
			this.command = new ShowListsCommand();
		}
	},
	APPLY
	{
		{
			this.command = new ApplyCommand();
		}
		
	},
	SHOWAPPLICATIONS
	{
		{
			this.command = new ShowApplicationsCommand();
		}
		
	},
	SHOWALLACCEPTEDAPPLICATIONS
	{
		{
			this.command = new ShowAllAcceptedApplicationsCommand();
		}
		
	},
	ADDSUBJECT
	{
		{
			this.command = new AddSubjectCommand();
		}
		
	},
	ADDFACULTY
	{
		{
			this.command = new AddFacultyCommand();
		}
		
	},
	GOTOMAINPAGE
	{
		{
			this.command = new GoToMainPageCommand();
		}
		
	},
	ACCEPTAPPLICATION
	{
		{
			this.command = new AcceptApplicationCommand();
		}
		
	},
	DELETEABITURIENT
	{
		{
			this.command = new DeleteAbiturientCommand();
		}
		
	},
	EDITABITURIENT
	{
		{
			this.command = new EditAbiturientCommand();
		}
		
	},
	DELETEFROMLISTS
	{
		{
			this.command = new DeleteFromListsCommand();
		}
		
	},
    EN {
        {
            this.command = new ChangeLanguageCommand("en_US");
        }
    },
    RU {
        {
            this.command = new ChangeLanguageCommand("ru_RUS");
        }
    },
    SHOWUSERS {
        {
            this.command = new ShowUsersCommand();
        }
    },
    SETADMIN {
        {
            this.command = new SetAdminCommand();
        }
    },
    SETNOTADMIN {
        {
            this.command = new SetNotAdminCommand();
        }
    };
	
	Command command;
	public Command getCurrentCommand()
	{
		return command;
	}

}
