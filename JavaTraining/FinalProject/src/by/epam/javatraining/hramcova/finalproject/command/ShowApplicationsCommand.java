package by.epam.javatraining.hramcova.finalproject.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.javatraining.hramcova.finalproject.dto.Abiturient;
import by.epam.javatraining.hramcova.finalproject.dto.Faculty;
import by.epam.javatraining.hramcova.finalproject.exception.CommandException;
import by.epam.javatraining.hramcova.finalproject.exception.ServiceException;
import by.epam.javatraining.hramcova.finalproject.resource.ConfigPagesManager;
import by.epam.javatraining.hramcova.finalproject.service.FacultyService;

public class ShowApplicationsCommand implements Command{

	@Override
	public String execute(HttpServletRequest request) throws CommandException{
		HttpSession session = request.getSession();
		try {
			if (request.getParameter("faculty") != null)
			{			
				
				List<Abiturient> abiturients = FacultyService.getAllNotEnrolledAbitutientsForFaculty(
							Integer.parseInt(request.getParameter("faculty")));			
				session.setAttribute("notenrolled", abiturients);			
			}
			else
			{		
				List<Faculty> faculties = FacultyService.getAllFaculties();				
				session.setAttribute("faculties", faculties);
				session.removeAttribute("notenrolled");
			}
		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
			throw new CommandException("Can not show applications!");
		}
		return ConfigPagesManager.getProperty("applications");
	}
}
