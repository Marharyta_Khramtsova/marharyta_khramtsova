<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<script type="text/javascript" src="resources/js/jquery-2.1.1.min.js"></script>
<link rel="stylesheet" href="resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/script.js"></script>
<title>Main page</title>
</head>
<body class="container">
	<div class="panel panel-default panel">
		<div class="panel-heading">
			<h3 class="panel-title">Plants</h3>
		</div>
		<div class="panel-body my-panel-body">
			<table>
			<tr>
			<th>Name</th>
			<th>Soil</th>
			<th>Origin</th>
			<th>Stalk Color</th>
			<th>Petals Color</th>
			<th>Average Size(cm)</th>
			<th>Temperature(degree)</th>
			<th>Lighting</th>
			<th>Watering(times a week)</th>
			<th>Multiplying</th>
			<th>Image</th>
			
			</tr>
				<c:forEach items="${list}" var="item">

					<tr>
						<td>${item.name}</td>
						<td>${item.soil}</td>
						<td>${item.origin}</td>
						<td>${item.visualParameters.stalkColor}</td>
						<td>${item.visualParameters.petalsColor}</td>
						<td>${item.visualParameters.averageSize.value}</td>
						<td>${item.growingTips.temperature.value}</td>
						<td>${item.growingTips.lighting}</td>
						<td>${item.growingTips.watering.value}</td>
						<td>${item.multiplying}</td>
						<td><img src="${item.image.value}" alt=item_image" width="70" height="70"></td>

					</tr>

				</c:forEach>

			</table>
		</div>
	</div>
	<div class="shadowtext"> Used parser:
		${parser}
	</div>
	
	<form>
	<div class="buttons">
    <button type="submit" name="dom" value="dom" class="btn btn-info">DOM</button>
    <button type="submit" name="sax" value="sax" class="btn btn-info">SAX</button>
    <button type="submit" name="stax" value="stax" class="btn btn-info">StAX</button>
	</div>
	</form>



</body>
</html>