package by.epam.javatraining.hramcova.task5.stax;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Vector;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;









import by.epam.javatraining.hramcova.task5.exception.ValidateException;
import by.epam.javatraining.hramcova.task5.flower.AverageSize;
import by.epam.javatraining.hramcova.task5.flower.Flower;
import by.epam.javatraining.hramcova.task5.flower.GrowingTips;
import by.epam.javatraining.hramcova.task5.flower.Image;
import by.epam.javatraining.hramcova.task5.flower.Item;
import by.epam.javatraining.hramcova.task5.flower.Temperature;
import by.epam.javatraining.hramcova.task5.flower.VisualParameters;
import by.epam.javatraining.hramcova.task5.flower.Watering;
import by.epam.javatraining.hramcova.task5.validator.Validator;

public class StAXParser {
	
	
	public static Flower parseXML(String fileName)
			throws XMLStreamException, MalformedURLException, IOException, ValidateException {

		File file = new File(fileName);
		if (Validator.validateXml(file))
		{
			URI uri = file.toURI();
			InputStream input = uri.toURL().openStream();
			Flower flower = new Flower();
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLStreamReader reader = factory.createXMLStreamReader(input);
			int event = reader.getEventType();
			String currentElement = null;
			byte id = 0;
			String name = "";
			String soil = "";
			String origin = "";
			String stalkColor = "";
			String petalsColor = "";
			int averageSize = 0;
			int temperature = 0;
			int watering = 0;
			String lighting = "";
			String multiplying = "";
			String image = "";

			while (true) {
				
				
				switch (event) {
				case XMLStreamConstants.START_ELEMENT: {
					currentElement = reader.getLocalName();
					if (currentElement.equals("item"))
						id = (Byte.parseByte(reader.getAttributeValue(0)));
					break;
				}
				case XMLStreamConstants.CHARACTERS: {
					if (!reader.isWhiteSpace())
					 {
						switch (currentElement) {
						case "name": {
							name = (reader.getText());
							break;
						}
						case "soil": {
							soil = reader.getText();
							break;
						}
						case "origin": {
							origin = reader.getText();						
							break;
						}
						case "stalk_color": {
							stalkColor = reader.getText();
							break;
						}
						case "petals_color": {
							petalsColor = reader.getText();
							break;
						}
						case "average_size": {
							averageSize = (Integer.parseInt(reader.getText()));
							break;
						}
						case "temperature": {
							temperature = (Integer.parseInt(reader.getText()));
							break;
						}
						case "lighting": {
							lighting = reader.getText();
							break;
						}
						case "watering": {
							watering = (Integer.parseInt(reader.getText()));
							break;
						}
						
						case "multiplying": {
							multiplying = reader.getText();
							break;
						}
						
						case "image": {
							image = reader.getText();
							break;
						}

						}
					}
					break;

				}
				case XMLStreamConstants.END_ELEMENT: {
					if (reader.getLocalName().equals("item")) {
						flower.add(new Item(name, soil, origin,
								new VisualParameters(stalkColor, petalsColor,
										new AverageSize(averageSize)),
								new GrowingTips(new Temperature(temperature), lighting, new Watering(watering)),
								multiplying, new Image(image),id));					
					
					} else
						currentElement = "";

					break;

				}

				}
				if (!reader.hasNext()) {
					break;
				}
				event = reader.next();

			}
			reader.close();
			return flower;
		}
		else 
			throw new ValidateException("Not valid!");
	}

}
