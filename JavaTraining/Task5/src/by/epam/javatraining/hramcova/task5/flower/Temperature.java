package by.epam.javatraining.hramcova.task5.flower;

public class Temperature {

	private int value;
	private String measure;
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String value) {
		this.measure = value;
	}
	
	public Temperature() {
		this.value = 10;
		this.measure = "degree_celsius";
	}

	public Temperature(int value, String measure) {
		
		this.value = value;
		this.measure = measure;
	}
	
	public Temperature(int value) {
			this();
			this.value = value;
		}
	
	

}