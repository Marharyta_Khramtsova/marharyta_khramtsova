package by.epam.javatraining.hramcova.task5.dom;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.javatraining.hramcova.task5.exception.ValidateException;
import by.epam.javatraining.hramcova.task5.flower.AverageSize;
import by.epam.javatraining.hramcova.task5.flower.Flower;
import by.epam.javatraining.hramcova.task5.flower.GrowingTips;
import by.epam.javatraining.hramcova.task5.flower.Image;
import by.epam.javatraining.hramcova.task5.flower.Item;
import by.epam.javatraining.hramcova.task5.flower.Temperature;
import by.epam.javatraining.hramcova.task5.flower.VisualParameters;
import by.epam.javatraining.hramcova.task5.flower.Watering;
import by.epam.javatraining.hramcova.task5.validator.Validator;

public class DOMParser {
	
	private static final Logger LOGGER = Logger.getRootLogger();
	
	public static Flower parseXML(String fileName) throws ValidateException, SAXException, IOException, ParserConfigurationException{
			
		Flower flower = new Flower();
        File xmlFile = new File(fileName);
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document doc;
        if (!Validator.validateXml(xmlFile))
         	throw new ValidateException("File is not valid!");
        	
        else
        {
        	doc = documentBuilder.parse(xmlFile);
	        doc.getDocumentElement().normalize();
	        NodeList flowerList = doc.getElementsByTagName("item");
	        for (int i = 0; i < flowerList.getLength(); i++) {
	        	Node node = flowerList.item(i);
	            if (node.getNodeType() == Node.ELEMENT_NODE) {
	            	
	            	Element itemElem = (Element) node;  
	                String name = itemElem.getElementsByTagName("name").item(0).getTextContent();
	                String soil = itemElem.getElementsByTagName("soil").item(0).getTextContent();
	                String origin = itemElem.getElementsByTagName("origin").item(0).getTextContent();
	                    
	                Element params = (Element) itemElem.getElementsByTagName("visual_parameters").item(0);
	                String stalkColor = params.getElementsByTagName("stalk_color").item(0).getTextContent();
	                String petalsColor = params.getElementsByTagName("petals_color").item(0).getTextContent();
                    String measure1 = params.getAttribute("measure");
                    int averageSizeInt = Integer.parseInt(params.getElementsByTagName("average_size").item(0).getTextContent());
                    AverageSize averageSize = new AverageSize(averageSizeInt,measure1);	                    
                    VisualParameters visualParameters = new VisualParameters(stalkColor, petalsColor,averageSize);
                    
                    Element tips = (Element) itemElem.getElementsByTagName("growing_tips").item(0);
                    Element tempElem = (Element) tips.getElementsByTagName("temperature").item(0);
                    String measure2 = tempElem.getAttribute("measure");
    	            int temperatureInt = Integer.parseInt(tempElem.getTextContent());
    	            Temperature temperature = new Temperature(temperatureInt, measure2);
                    String lighting = tips.getElementsByTagName("lighting").item(0).getTextContent();
                    Element waterElem = (Element) tips.getElementsByTagName("watering").item(0);
                    String measure3 = waterElem.getAttribute("measure");
                    String frequency = waterElem.getAttribute("frequency");
                    int wateringInt = Integer.parseInt(waterElem.getTextContent());               
                    Watering watering = new Watering(wateringInt, measure3, frequency);
                    GrowingTips growingTips = new GrowingTips(temperature, lighting, watering);
                    
                    String multiplying = itemElem.getElementsByTagName("multiplying").item(0).getTextContent();
                    
                    Element imageElem = (Element) itemElem.getElementsByTagName("image").item(0);
                    String type = imageElem.getAttribute("type");
                    String imageFile = imageElem.getTextContent();
                    Image image = new Image(imageFile, type);
                    
                    byte id = Byte.parseByte(itemElem.getAttribute("id"));	                    
                    Item item = new Item(name, soil, origin, visualParameters, growingTips, multiplying, image, id);
                    flower.add(item);                  
	
	                }
	            }
	            LOGGER.debug("DOM-parsing is correct!");
	            }

        
        
        return flower;
    }

}
