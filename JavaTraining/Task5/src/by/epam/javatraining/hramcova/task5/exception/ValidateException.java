package by.epam.javatraining.hramcova.task5.exception;

public class ValidateException extends Exception{
	
	public ValidateException() {		
	}
	
	public ValidateException(String str)
	{
		super(str);
	}

}
