package by.epam.javatraining.hramcova.task5.flower;


public class VisualParameters {

	private String stalkColor;
	private String petalsColor;
	private AverageSize averageSize;

	public String getStalkColor() {
		return stalkColor;
	}

	public void setStalkColor(String value) {
		this.stalkColor = value;
	}

	public String getPetalsColor() {
		return petalsColor;
	}

	public void setPetalsColor(String value) {
		this.petalsColor = value;
	}

	public AverageSize getAverageSize() {
		return averageSize;
	}

	public void setAverageSize(
			AverageSize value) {
		this.averageSize = value;
	}
	
	public VisualParameters() {
	}			

	public VisualParameters(String stalkColor, String petalsColor,
			AverageSize averageSize) {
		this.stalkColor = stalkColor;
		this.petalsColor = petalsColor;
		this.averageSize = averageSize;
	}
	public VisualParameters(VisualParameters parameters) {
		this.stalkColor = parameters.stalkColor;
		this.petalsColor = parameters.petalsColor;
		this.averageSize = parameters.averageSize;
	}
}
