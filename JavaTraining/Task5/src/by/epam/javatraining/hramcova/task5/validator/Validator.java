package by.epam.javatraining.hramcova.task5.validator;



import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;


public class Validator {
	
	static File schema = new File("E:\\Epam\\Tasr5_WEB_XML\\WebContent\\resources\\xml\\plants.xsd");
	
	public static boolean validateXml(File xml) {
	    try {
	        SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
	                .newSchema(new StreamSource(schema))
	                .newValidator()
	                .validate(new StreamSource(xml));
	    } 
	    catch (IOException e)
	    {
	    	e.printStackTrace();
	    } catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return true;
	}
}
