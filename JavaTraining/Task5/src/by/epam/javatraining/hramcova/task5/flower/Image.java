package by.epam.javatraining.hramcova.task5.flower;

public class Image {

	private String value;
	private String type;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String value) {
		this.type = value;
	}
	
	public Image() {
		this.value = "";
		this.type = "jpg";		
	}

	public Image(String value, String type) {
		
		this.value = value;
		this.type = type;
	}	

	public Image(String value) {
		this();
		this.value = value;
	}	

}
