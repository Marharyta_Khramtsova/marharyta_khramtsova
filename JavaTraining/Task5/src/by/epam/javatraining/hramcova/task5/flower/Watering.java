package by.epam.javatraining.hramcova.task5.flower;

public class Watering {

	private int value;
	private String measure;
	private String frequency;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String value) {
		this.measure = value;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String value) {
		this.frequency = value;
	}
	
	public Watering() {
		this.value = 1;
		this.measure = "l";
		this.frequency = "week";
		
	}

	public Watering(int value, String measure, String frequency) {
		
		this.value = value;
		this.measure = measure;
		this.frequency = frequency;
	}
	
	public Watering(int value) {
		this();
		this.value = value;
		
	}
	
	

}
