package by.epam.javatraining.hramcova.task5.sax;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.javatraining.hramcova.task5.flower.AverageSize;
import by.epam.javatraining.hramcova.task5.flower.Flower;
import by.epam.javatraining.hramcova.task5.flower.GrowingTips;
import by.epam.javatraining.hramcova.task5.flower.Image;
import by.epam.javatraining.hramcova.task5.flower.Item;
import by.epam.javatraining.hramcova.task5.flower.Temperature;
import by.epam.javatraining.hramcova.task5.flower.VisualParameters;
import by.epam.javatraining.hramcova.task5.flower.Watering;

public class SAXParser extends DefaultHandler{
	
	private String currentElement = "";
	private Flower flower;
	private Item item;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;
	
	private static final Logger LOGGER = Logger.getRootLogger();
	
	
	public static Flower parseXML(String fileName) {
		SAXParserFactory factory = SAXParserFactory.newInstance();
        javax.xml.parsers.SAXParser parser;
        SAXParser ans = new SAXParser();
		try {			
			parser = factory.newSAXParser();
			parser.parse(new File(fileName), ans);
		}
		catch(SAXException | IOException | ParserConfigurationException e)
		{
			e.printStackTrace();
		}
		return ans.flower;
	}

	public SAXParser() {
		flower = new Flower();
		item = new Item();
		visualParameters = new VisualParameters();
		growingTips = new GrowingTips();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		currentElement = qName;
		
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		switch(qName)
		{
		case "visual_parameters":
			item.setVisualParameters(new VisualParameters(visualParameters));
			break;
		case "growing_tips":
			item.setGrowingTips(new GrowingTips(growingTips));
			break;
		
		case "item":
			LOGGER.debug(item);
			flower.add(new Item(item));
			break;
		}
		currentElement = "";
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		LOGGER.debug(currentElement);
		switch (currentElement)
		{
		case "name":
			item.setName(new String(ch, start, length));
			break;
		case "soil":
			item.setSoil(new String(ch, start, length));
			break;
		case "origin":
			item.setOrigin(new String(ch, start, length));
			break;
		case "stalk_color":
			visualParameters.setStalkColor(new String(ch, start, length));
			break;
		case "petals_color":
			visualParameters.setPetalsColor(new String(ch, start, length));
			break;
		case "average_size":
			visualParameters.setAverageSize(new AverageSize(Integer.parseInt(new String(ch, start, length))));
			break;
		case "temperature":
			growingTips.setTemperature(new Temperature(Integer.parseInt(new String(ch, start, length))));
			break;
		case "lighting":
			growingTips.setLighting(new String(ch, start, length));
			break;
		case "watering":
			growingTips.setWatering(new Watering(Integer.parseInt(new String(ch, start, length))));
			break;
		case "multiplying":
			item.setMultiplying(new String(ch, start, length));
			break;
		case "image":
			item.setImage(new Image(new String(ch, start, length)));
			break;
		
			
		
		}
	}
	
	

}
