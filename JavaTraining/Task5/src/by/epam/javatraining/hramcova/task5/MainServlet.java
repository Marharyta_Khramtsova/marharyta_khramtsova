package by.epam.javatraining.hramcova.task5;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import by.epam.javatraining.hramcova.task5.dom.DOMParser;
import by.epam.javatraining.hramcova.task5.exception.ValidateException;
import by.epam.javatraining.hramcova.task5.flower.Flower;
import by.epam.javatraining.hramcova.task5.sax.SAXParser;
import by.epam.javatraining.hramcova.task5.stax.StAXParser;
import by.epam.javatraining.hramcova.task5.validator.Validator;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getRootLogger();
	private static String xmlName;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainServlet() {
    	ResourceBundle bundle = ResourceBundle.getBundle("config");
    	xmlName = bundle.getString("xmlName");
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Flower flower = null;
			String parser = "StAX";
			if (request.getParameter("dom") != null)
			{
				flower = DOMParser.parseXML(xmlName);
				parser = "DOM";
			}
			else if (request.getParameter("sax") != null)
			{
				flower = SAXParser.parseXML(xmlName);
				parser = "SAX";
			}
			else 
			{
				parser = "StAX";
				flower = StAXParser.parseXML(xmlName);
			}
			HttpSession session = request.getSession(true);
			session.setAttribute("list", flower);
			session.setAttribute("parser", parser);
			request.getRequestDispatcher("pages/main_page.jsp").forward(request, response);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
