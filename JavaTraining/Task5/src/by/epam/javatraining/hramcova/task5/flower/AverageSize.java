package by.epam.javatraining.hramcova.task5.flower;


public class AverageSize {
	
	 private int value;
	 private String measure;
	 
	 public void setMeasure(String measure) {
		this.measure = measure;
	}
	 public void setValue(int value) {
		this.value = value;
	}
	 public String getMeasure() {
		return measure;
	}
	 public int getValue() {
		return value;
	}
	 
	 public AverageSize() {
		 this.measure = "cm";
	}
	public AverageSize(int value, String measure) {
		
		this.value = value;
		this.measure = measure;
	}
	
public AverageSize(int value) {
		this();
		this.value = value;
	}
	 
	 

     
   
}
