package by.epam.javatraining.hramcova.task5.flower;

public class GrowingTips {

	private Temperature temperature;
	private String lighting;
	private Watering watering;

	public Temperature getTemperature() {
		return temperature;
	}

	public void setTemperature(Temperature value) {
		this.temperature = value;
	}

	public String getLighting() {
		return lighting;
	}

	public void setLighting(String value) {
		this.lighting = value;
	}

	public Watering getWatering() {
		return watering;
	}

	public void setWatering(Watering value) {
		this.watering = value;
	}
	
	public GrowingTips() {
	}

	public GrowingTips(Temperature temperature, String lighting,
			Watering watering) {
		
		this.temperature = temperature;
		this.lighting = lighting;
		this.watering = watering;
	}	
	public GrowingTips(GrowingTips tips) {
		
		this.temperature = tips.temperature;
		this.lighting = tips.lighting;
		this.watering = tips.watering;
	}	

}
