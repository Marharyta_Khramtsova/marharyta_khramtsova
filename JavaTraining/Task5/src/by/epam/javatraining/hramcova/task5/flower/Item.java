package by.epam.javatraining.hramcova.task5.flower;


public class Item{

	private String name;
	private String soil;
	private String origin;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;
	private String multiplying;
	private Image image;
	private byte id;

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getSoil() {
		return soil;
	}

	public void setSoil(String value) {
		this.soil = value;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String value) {
		this.origin = value;
	}
	
	public VisualParameters getVisualParameters() {
		return visualParameters;
	}

	public void setVisualParameters(VisualParameters visualParameters) {
		this.visualParameters = visualParameters;
	}

	public GrowingTips getGrowingTips() {
		return growingTips;
	}

	public void setGrowingTips(GrowingTips growingTips) {
		this.growingTips = growingTips;
	}

	public String getMultiplying() {
		return multiplying;
	}

	public void setMultiplying(String multiplying) {
		this.multiplying = multiplying;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public byte getId() {
		return id;
	}

	public void setId(byte id) {
		this.id = id;
	}

	public Item() {
	}
	
	
	public Item(String name, String soil, String origin,
			VisualParameters visualParameters, GrowingTips growingTips,
			String multiplying, Image image, byte id) {
		
		this.name = name;
		this.soil = soil;
		this.origin = origin;
		this.visualParameters = visualParameters;
		this.growingTips = growingTips;
		this.multiplying = multiplying;
		this.image = image;
		this.id = id;
	}

	@Override
	public String toString() {
		return "Item [name=" + name + ", soil=" + soil + ", origin=" + origin
				+ ", visualParameters=" + visualParameters + ", growingTips="
				+ growingTips + ", multiplying=" + multiplying + ", image="
				+ image + ", id=" + id + "]";
	}
	
	public Item(Item item)
	{
		this.id = item.id;
		this.growingTips = item.growingTips;
		this.image = item.image;
		this.multiplying = item.multiplying;
		this.name = item.name;
		this.origin = item.origin;
		this.soil = item.soil;
		this.visualParameters = item.visualParameters;
	}
	
}
