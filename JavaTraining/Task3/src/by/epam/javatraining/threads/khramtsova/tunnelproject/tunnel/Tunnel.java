package by.epam.javatraining.threads.khramtsova.tunnelproject.tunnel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.javatraining.threads.khramtsova.tunnelproject.train.Direction;
import by.epam.javatraining.threads.khramtsova.tunnelproject.train.Train;


public class Tunnel {
	
	private static final Logger LOGGER = Logger.getRootLogger();
	
	private int id;	
	private int capacity;	
	private int size;
	private Direction direction;
	private final Lock lock;
	
		
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Tunnel() {	
		lock = new ReentrantLock();
	}

	public Tunnel(int id, int capacity) {
		this.id = id;
		this.capacity = capacity;
		lock = new ReentrantLock();
	}
	
	public int getId() {
        return id;
    }	
    
    public int getCapacity() {
        return capacity;
    }   

    public Lock getLock() {
        return lock;
    }

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
    
    
    
    
	
	

}
