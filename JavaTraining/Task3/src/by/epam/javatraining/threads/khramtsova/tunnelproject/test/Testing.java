package by.epam.javatraining.threads.khramtsova.tunnelproject.test;

import by.epam.javatraining.threads.khramtsova.tunnelproject.crossing.Crossing;
import by.epam.javatraining.threads.khramtsova.tunnelproject.train.Direction;
import by.epam.javatraining.threads.khramtsova.tunnelproject.train.Train;

public class Testing {
	public static void main(String[] args) throws InterruptedException{
	
		Crossing crossing = new Crossing(2, 1000, 2);
		Train train1 = new Train("Train1", Direction.LEFT, crossing);
		Train train2 = new Train("Train2", Direction.RIGHT, crossing);
		Train train3 = new Train("Train3", Direction.LEFT, crossing);
		Train train4 = new Train("Train4", Direction.LEFT, crossing);
		Train train5 = new Train("Train5", Direction.RIGHT, crossing);
		
		new Thread(train1).start();
        new Thread(train2).start();
        new Thread(train3).start();
        new Thread(train4).start();
        new Thread(train5).start();

        Thread.sleep(4000);
        
        train1.stopThread();
        train2.stopThread();
        train3.stopThread();
        train4.stopThread();
        train5.stopThread();
	}

}
