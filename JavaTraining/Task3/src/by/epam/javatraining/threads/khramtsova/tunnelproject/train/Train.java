package by.epam.javatraining.threads.khramtsova.tunnelproject.train;


import org.apache.log4j.Logger;

import by.epam.javatraining.threads.khramtsova.tunnelproject.crossing.Crossing;
import by.epam.javatraining.threads.khramtsova.tunnelproject.crossing.CrossingException;
import by.epam.javatraining.threads.khramtsova.tunnelproject.tunnel.Tunnel;

public class Train implements Runnable{
	
	private static final Logger LOGGER = Logger.getRootLogger();
	
	private String name;
    private Direction direction;
    private Crossing crossing;
    
    private volatile boolean stopThread = false;    

	public Train() {
	}

	public Train(String name, Direction direction, Crossing crossing) {
		this.name = name;
		this.direction = direction;
		this.crossing = crossing;
	}
	
	
	
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public String getName() {
        return name;
    }

    public void stopThread() {
        stopThread = true;
    }

	@Override
	public void run() {
		try {
            while (!stopThread) {
            	outOfTunnel();
            	inTunnel();
            }
        } catch (InterruptedException | CrossingException e) {
            LOGGER.error("� ������� ��������� ������������ � �� ���������.", e);
        }
		
	}
	
	private void outOfTunnel() throws InterruptedException {
        Thread.sleep(2000);
    }
	
	 private void inTunnel() throws InterruptedException, CrossingException {

	        boolean isLockedTunnel = false;
	        Tunnel tunnel = null;
	        try {
	        	isLockedTunnel = crossing.lockTunnel(this);

	            if (isLockedTunnel) {
	            	tunnel = crossing.getTunnel(this);
	            	LOGGER.debug("����� " + name + " ������ � ������� �" + tunnel.getId());
	                go(crossing.getTime());
	            }
	            else {
	                //LOGGER.debug("������ " + name + " �������� �� ������ � ������� �" + tunnel.getId());
	            }
	        } finally {
	            if (isLockedTunnel) {
	                crossing.unlockTunnel(this);
	                LOGGER.debug("����� " + name + " ������ �� ������� �" + tunnel.getId());
	            }
	        }

	    }
	 private void go(int time) throws InterruptedException
	 {
		 Thread.sleep(time);
	 }

}
