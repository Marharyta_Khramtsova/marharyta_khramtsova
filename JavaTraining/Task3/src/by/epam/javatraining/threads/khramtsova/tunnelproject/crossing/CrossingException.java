package by.epam.javatraining.threads.khramtsova.tunnelproject.crossing;

public class CrossingException extends Exception{
	
	private static final long serialVersionUID = 1L;

    public CrossingException() {
    }
    
    public CrossingException(String message) {
        super(message);
    }

}
