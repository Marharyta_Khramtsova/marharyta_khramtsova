package by.epam.javatraining.threads.khramtsova.tunnelproject.crossing;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import by.epam.javatraining.threads.khramtsova.tunnelproject.train.Train;
import by.epam.javatraining.threads.khramtsova.tunnelproject.tunnel.Tunnel;

public class Crossing {
	
	private static final Logger LOGGER = Logger.getRootLogger();
	
	private int time;
	private BlockingQueue<Tunnel> tunnelsQueue;
	private Map<Train, Tunnel> usedTunnelsMap;		
	 
	 public int getTime() {
		return time;
	}
	 
	 public Crossing() {
	}

	public Crossing(int tunnelSize, int time, int capacity) {
	        
		 tunnelsQueue = new ArrayBlockingQueue<>(tunnelSize);
		 this.time = time;

	        for (int i = 0; i < tunnelSize; i++) {
	        	tunnelsQueue.add(new Tunnel(i,capacity));
	        }

	        usedTunnelsMap = new HashMap<>();
	        
	        LOGGER.debug("������� ������.");
	 }
	 
	 public boolean lockTunnel(Train train) {
	        try {
	        	//LOGGER.debug(tunnelsQueue);
	            Tunnel tunnel = tunnelsQueue.take();
	            //LOGGER.debug("������� �� ������� ������� �" + tunnel.getId());
	            if ( tunnel.getSize() != 0 && tunnel.getDirection() == train.getDirection())
	            {
	            	usedTunnelsMap.put(train, tunnel);
	            	tunnel.setSize(tunnel.getSize()+1);
	            	if (tunnel.getSize() < tunnel.getCapacity())
		            	tunnelsQueue.put(tunnel);
	            	return true;
	            }
	            else if (tunnel.getSize() == 0)
	            {
	            	usedTunnelsMap.put(train, tunnel);
	            	tunnel.setSize(tunnel.getSize()+1);
	            	tunnel.setDirection(train.getDirection());
	            	if (tunnel.getSize() < tunnel.getCapacity())
		            	tunnelsQueue.put(tunnel);
	            	return true;
	            }
	            
	        } catch (InterruptedException e) {
	        	 LOGGER.debug("������ " + train.getName() + " �������� �� ������ �� �������.");
	            return false;
	        }
	        return false;
	    }
	 
	 public boolean unlockTunnel(Train train) {
		 	Tunnel tunnel = null;
	        try { 	      
	        	tunnel = getTunnel(train);
	            tunnelsQueue.put(tunnel);
	            usedTunnelsMap.remove(train);   
	            tunnel.setSize(tunnel.getSize()-1);
	        }
	        catch (InterruptedException e)
	        {
	        	 LOGGER.debug("����� " + train.getName() + " �� ����"
	        	 		+ " ������� �� ������� �" + tunnel.getId() + ".");
	            return false;
	        }
	        catch (CrossingException e)
	        {
	        	 LOGGER.debug(e.getMessage());
	            return false;
	        }
	        return true;
	    }
	 
	 public Tunnel getTunnel(Train train) throws CrossingException {
	        Tunnel tunnel = usedTunnelsMap.get(train);
	        if (tunnel == null) {
	            throw new CrossingException("Try to use tunnel without blocking.");
	        }
	        return tunnel;
	    }
	 

}
