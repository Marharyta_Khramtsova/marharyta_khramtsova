package by.epam.javatraining.task2.exception;

public class NullableCharacterContentException extends Exception{
	
	public NullableCharacterContentException()
    {
    }

    public NullableCharacterContentException(String message)
    {
        super(message);
    }

    public NullableCharacterContentException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NullableCharacterContentException(Throwable cause)
    {
        super(cause);
    }

    public NullableCharacterContentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
