package by.epam.javatraining.task2.exception;

public class NullableListingContentException extends Exception{
	
	public NullableListingContentException()
    {
    }

    public NullableListingContentException(String message)
    {
        super(message);
    }

    public NullableListingContentException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NullableListingContentException(Throwable cause)
    {
        super(cause);
    }

    public NullableListingContentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
