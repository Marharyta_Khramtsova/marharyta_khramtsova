package by.epam.javatraining.task2.exception;

public class NullableTextContentException extends Exception{
	
	public NullableTextContentException()
    {
    }

    public NullableTextContentException(String message)
    {
        super(message);
    }

    public NullableTextContentException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NullableTextContentException(Throwable cause)
    {
        super(cause);
    }

    public NullableTextContentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
