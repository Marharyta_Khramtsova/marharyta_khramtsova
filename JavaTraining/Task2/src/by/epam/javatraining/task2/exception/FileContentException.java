package by.epam.javatraining.task2.exception;

public class FileContentException extends Exception{
	
	public FileContentException()
    {
    }

    public FileContentException(String message)
    {
        super(message);
    }

    public FileContentException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public FileContentException(Throwable cause)
    {
        super(cause);
    }

    public FileContentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
