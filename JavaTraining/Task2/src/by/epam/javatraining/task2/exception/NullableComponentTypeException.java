package by.epam.javatraining.task2.exception;

public class NullableComponentTypeException extends Exception{
	
	public NullableComponentTypeException()
    {
    }

    public NullableComponentTypeException(String message)
    {
        super(message);
    }

    public NullableComponentTypeException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NullableComponentTypeException(Throwable cause)
    {
        super(cause);
    }

    public NullableComponentTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
