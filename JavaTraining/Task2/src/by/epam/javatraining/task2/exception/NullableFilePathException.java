package by.epam.javatraining.task2.exception;

public class NullableFilePathException extends Exception{
	
	public NullableFilePathException()
    {
    }

    public NullableFilePathException(String message)
    {
        super(message);
    }

    public NullableFilePathException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NullableFilePathException(Throwable cause)
    {
        super(cause);
    }

    public NullableFilePathException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
