package by.epam.javatraining.task2.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import by.epam.javatraining.task2.structure.Component;
import by.epam.javatraining.task2.structure.Composite;
import by.epam.javatraining.task2.structure.Symbol;
import by.epam.javatraining.task2.textconstants.Letters;

public class Actions {
	
	public static void removeWords(Composite text, int len) 
    {
        for (Component paragraph : text.getChildComponents())
        {
            if (paragraph.getClass() == Composite.class)
            {
                for (Component sentence : ((Composite) paragraph).getChildComponents())
                {
                    if (sentence.getClass() == Composite.class)
                    {
                       removeWords(sentence, len);
                    }
                }
            }
        }
    }
	
	  private static void removeWords(Component sent, int len)
	    {
	        Composite compositeSent = (Composite) sent;
	        int n;
	        if ((n = compositeSent.getChildComponents().size()) > 1)
	        {
	        	ArrayList<Component> list = (ArrayList<Component>)compositeSent.getChildComponents();
	        	//Iterator<Component> it = list.iterator();
	        	//while (it.hasNext())
	        	for (int i = 0; i < n; i++)
	        	{
	        		Composite currentWord = (Composite) list.get(i);
		            if ((currentWord).getChildComponents().size() == len)
		            {
			            for (int j = 0; j < (currentWord).getChildComponents().size(); j++)
			            {
			            	 Symbol first = (Symbol) (currentWord).getChildComponents().get(0);
			            	 if (!Letters.isVowel(first.getSymbolValue()))
			            	 {
				                compositeSent.remove(i);
			            	 }
			            }
		            }
	        	}
	        }
	    }
	 
	  
	  public static int countOfSentencesWithSameWords(Composite text)
	  {
		  int res = 0;
		  for (Component paragraph : text.getChildComponents())
	        {
	            if (paragraph.getClass() == Composite.class)
	            {
	                for (Component sentence : ((Composite) paragraph).getChildComponents())
	                {
	                	boolean hasSame = false;
	                    if (sentence.getClass() == Composite.class)
	                    {
	                    	ArrayList<Component> words = (ArrayList<Component>)((Composite) sentence).getChildComponents();
	                    	for (Component word : words)
	                    	{
	                    		int count = 0;
	                    		for (Component word2 : words)
	                    		{
	                    			if (((Composite)word).equals((Composite)word2))
	                    				count++;
	                    				
	                    		}
	                    		if (count > 1)
	                    		{
	                    			hasSame = true;
	                    			break;
	                    		}
	                    	}
	        	        	
	                    }
	                    if (hasSame)
	                    	res++;
	                }
	            }
	        }
		  return res;
		  
	  }
	  
	  public static ArrayList<Component> alphPrint(Composite text)
	  {
		  for (Component paragraph : text.getChildComponents())
	        {
	            if (paragraph.getClass() == Composite.class)
	            {
	                for (Component sentence : ((Composite) paragraph).getChildComponents())
	                {
	                    if (sentence.getClass() == Composite.class)
	                    {
	                    	ArrayList<Component> words = (ArrayList<Component>)((Composite) sentence).getChildComponents();
	        	        	
	                    }
	                }
	            }
	        }
		  return null;
		  
	  }
	  
	  

}
