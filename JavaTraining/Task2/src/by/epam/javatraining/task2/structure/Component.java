package by.epam.javatraining.task2.structure;


public interface Component {
	
	public String textToString();

    public ComponentType getType();

}
