package by.epam.javatraining.task2.structure;

public enum ComponentType {
	
	TEXT,
	PARAGRAPH, 
	LISTING,
	SENTENCE, 
	WORD,
	SYMBOL
}
