package by.epam.javatraining.task2.structure;

import by.epam.javatraining.task2.exception.NullableListingContentException;
import by.epam.javatraining.task2.textconstants.TextConstants;


public class Listing implements Component{

	private String listingContent;

    public Listing(String listingContent) throws NullableListingContentException
    {
        if (listingContent == null)
        {
            throw new NullableListingContentException("wrong listing content");
        }
        this.listingContent = listingContent;
    }

    @Override
    public String textToString()
    {
        return TextConstants.EMPTY_STRING + TextConstants.LINE_SEPARATOR +
                listingContent + TextConstants.LINE_SEPARATOR + TextConstants.LINE_SEPARATOR;
    }

    @Override
    public ComponentType getType()
    {
        return ComponentType.LISTING;
    }

}
