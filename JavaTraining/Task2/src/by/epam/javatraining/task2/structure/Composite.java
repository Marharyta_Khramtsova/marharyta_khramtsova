package by.epam.javatraining.task2.structure;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import by.epam.javatraining.task2.exception.NullableComponentTypeException;
import by.epam.javatraining.task2.textconstants.TextConstants;

public class Composite implements Component{
	
	private List<Component> childComponents = new ArrayList<>();
    private ComponentType type;   
    

    public Composite(ComponentType type) throws NullableComponentTypeException
    {
        if (type == null)
        {
            throw new NullableComponentTypeException("null type");
        }
        this.type = type;
    }

    public List<Component> getChildComponents()
    {
        return new ArrayList<>(childComponents);
    }

    @Override
    public ComponentType getType()
    {
        return type;
    }

    public boolean add(Component component)
    {
        return childComponents.add(component);
    }

    public Component remove(int i)
    {
        return childComponents.remove(i);
    }


    @Override
    public String textToString()
    {
        String[] childStrings = new String[childComponents.size()];
        boolean first = true;
        String componentText;
        for (int i = 0; i < childComponents.size(); i++)
        {
            componentText = childComponents.get(i).textToString();
            switch (childComponents.get(i).getType())
            {
                case PARAGRAPH:
                    childStrings[i] = TextConstants.EMPTY_STRING + TextConstants.TABULATION +
                            componentText + TextConstants.LINE_SEPARATOR;
                    break;
                case SENTENCE:
                case WORD:
                    if (first)
                    {
                        childStrings[i] = componentText;
                        first = false;
                    }
                    else
                    {
                        childStrings[i] = TextConstants.EMPTY_STRING + TextConstants.SPACE +
                                componentText;
                    }
                    break;
                default:
                    childStrings[i] = componentText;
            }
        }
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < childStrings.length; i++)
        {
        	str.append(childStrings[i]);
        }
        return TextConstants.EMPTY_STRING + str;
    }

    @Override
    public String toString()
    {
        return textToString();
    }

	@Override
	public boolean equals(Object c) {
		return this.toString().equals(c.toString());
	}
    
    

	
    
    

	

}
