package by.epam.javatraining.task2.structure;

import by.epam.javatraining.task2.exception.NullableCharacterContentException;


public class Symbol implements Component{
	
	 private Character symbolValue;

	    public Symbol(Character symbolValue) throws NullableCharacterContentException
	    {
	        if (symbolValue == null)
	        {
	            throw new NullableCharacterContentException("null symbol");
	        }
	        this.symbolValue = symbolValue;

	    }
	    
	    

	    public Character getSymbolValue() {
			return symbolValue;
		}

		@Override
	    public String textToString()
	    {
	        return symbolValue.toString();
	    }

	    @Override
	    public ComponentType getType()
	    {
	        return ComponentType.SYMBOL;
	    }

}
