package by.epam.javatraining.task2.ioservice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import by.epam.javatraining.task2.exception.FileContentException;
import by.epam.javatraining.task2.exception.NullableFilePathException;
import by.epam.javatraining.task2.exception.NullableTextContentException;

public class IOUtilities {
	
	 public static String read(String path) throws FileContentException, NullableTextContentException, NullableFilePathException
	    {
	    	if (path == null)
	        {
	            throw new NullableFilePathException("null file path");
	        }
	    	File file = new File(path);
	        try (BufferedReader br = new BufferedReader(new FileReader(file)))
	        {
	            String line;
	            StringBuilder content = new StringBuilder();
	            while ((line = br.readLine()) != null)
	            {
	                content.append(line).append(System.getProperty("line.separator"));
	            }
	            return content.toString();
	        }
	        catch (IOException e)
	        {
	            throw new FileContentException(e.getMessage());
	        }
	    }
	 
	 public static void write(String path, String text) throws FileContentException, NullableFilePathException 
	    {
	    	if (path == null)
	        {
	            throw new NullableFilePathException("null path");
	        }
	        File file = new File(path);
	        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file)))
	        {
	            bw.write(text);
	        }
	        catch (IOException e)
	        {
	            throw new FileContentException(e.getMessage());
	        }
	    }

}
