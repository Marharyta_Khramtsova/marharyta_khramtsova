package by.epam.javatraining.task2.parsing;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.javatraining.task2.exception.NullableCharacterContentException;
import by.epam.javatraining.task2.exception.NullableComponentTypeException;
import by.epam.javatraining.task2.exception.NullableListingContentException;
import by.epam.javatraining.task2.structure.Component;
import by.epam.javatraining.task2.structure.ComponentType;
import by.epam.javatraining.task2.structure.Composite;
import by.epam.javatraining.task2.structure.Listing;
import by.epam.javatraining.task2.structure.Symbol;
import by.epam.javatraining.task2.textconstants.TextConstants;

public class Parser {
	
  
    public Composite parseText(String text) throws NullableComponentTypeException, NullableCharacterContentException, NullableListingContentException 
    {
        Composite composedText = new Composite(ComponentType.TEXT);
        String[] paragraphsWithListings = text.split(TextConstants.TABULATION);
        boolean escape = true;
        for (String paragraphWithListings : paragraphsWithListings)
        {
            if (escape)
            {
                escape = false;
                continue;
            }
            
            ArrayList<Component> list = parseParagraphWithListings(paragraphWithListings);
            for(Component comp : list)
            {
            	composedText.add(comp);
            }
        }
        return composedText;
    }
    
    
    private ArrayList<Component> parseParagraphWithListings(String paragraphWithListings) throws NullableComponentTypeException, NullableCharacterContentException, NullableListingContentException
    {
        ArrayList<Component> paragraphAndListings = new ArrayList<>();
        String paragraph = Pattern.compile(TextConstants.LISTING_REGEXP, Pattern.DOTALL).
                matcher(paragraphWithListings).replaceAll(TextConstants.EMPTY_STRING).trim();
        paragraphAndListings.add(parseParagraph(paragraph));
        Pattern pattern = Pattern.compile(TextConstants.LISTING_REGEXP, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(paragraphWithListings);
        while (matcher.find())
        {
            String listing = matcher.group();
            paragraphAndListings.add(new Listing(listing));
        }
        return paragraphAndListings;
    }
    
    
    private Composite parseParagraph(String paragraph) throws NullableComponentTypeException, NullableCharacterContentException 
    {
        Composite compositeParagraph = new Composite(ComponentType.PARAGRAPH);
        Pattern pattern = Pattern.compile(TextConstants.SENTENCE_REGEXP);
        Matcher matcher = pattern.matcher(paragraph);
        while (matcher.find())
        {
            String sentence = matcher.group();
            int last = sentence.length() - 1;
            if (sentence.charAt(last) == TextConstants.SPACE_CHAR)
            {
                compositeParagraph.add(parseSentence(sentence.substring(0, last - 1)));
                compositeParagraph.add(new Symbol(sentence.charAt(last - 1)));
            }
            else
            {
                compositeParagraph.add(parseSentence(sentence.substring(0, last)));
                compositeParagraph.add(new Symbol(sentence.charAt(last)));
            }
        }
        return compositeParagraph;
    }
    
    
    private Composite parseSentence(String sentence) throws NullableComponentTypeException, NullableCharacterContentException
    {
        Composite compositeSentence = new Composite(ComponentType.SENTENCE);
        String[] parsed = sentence.split(TextConstants.SPACE);
        for (String element : parsed)
        {
            compositeSentence.add(parseWord(element));
        }
        return compositeSentence;
    }
    
    private Composite parseWord(String element) throws NullableComponentTypeException, NullableCharacterContentException
    {
        Composite compositeWord = new Composite(ComponentType.WORD);
        char[] letters = element.toCharArray();
        for (char letter : letters)
        {
            compositeWord.add(new Symbol(letter));
        }
        return compositeWord;
    }

}
