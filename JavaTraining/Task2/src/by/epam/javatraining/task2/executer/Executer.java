package by.epam.javatraining.task2.executer;

import by.epam.javatraining.task2.action.Actions;
import by.epam.javatraining.task2.ioservice.IOUtilities;
import by.epam.javatraining.task2.parsing.Parser;
import by.epam.javatraining.task2.structure.Composite;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class Executer {
	
	private static final Logger LOGGER = Logger.getRootLogger();
	

		
	public static void main(String[] args)
    {				
        try
        {
            Parser parser = new Parser();
            Composite composite = parser.parseText(IOUtilities.read("data/text.txt"));
            IOUtilities.write("data/out.txt", composite.textToString());
            int count = Actions.countOfSentencesWithSameWords(composite);
            LOGGER.debug(count);
        }
        catch (Exception e)
        {
        	LOGGER.error(e.getMessage());
        }
    }

}
