package by.epam.javatraining.task2.textconstants;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Letters implements Iterable<Character>{
	private static List<Character> vowels;	
	
	public Letters() {
		vowels = new ArrayList<Character>();
		vowels.add('A');
		vowels.add('E');
		vowels.add('Y');
		vowels.add('U');
		vowels.add('I');
		vowels.add('O');	
		vowels.add('a');
		vowels.add('e');
		vowels.add('y');
		vowels.add('u');
		vowels.add('i');
		vowels.add('o');	
	}


	public static boolean isVowel(char c)
	{
		for (Character ch : vowels)
		{
			if (c == ch)
			{
				return true;
			}
		}
		return false;
	}


	@Override
	public Iterator<Character> iterator() {
		return this.vowels.iterator();
	}
}
