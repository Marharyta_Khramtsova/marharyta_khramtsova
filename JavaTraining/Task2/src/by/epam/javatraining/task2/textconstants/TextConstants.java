package by.epam.javatraining.task2.textconstants;

public class TextConstants {
	
	  private TextConstants()
	    {
	    }

	    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	    public static final String TABULATION = "    ";
	    public static final String SPACE = " ";
	    public static final Character SPACE_CHAR = ' ';
	    public static final String EMPTY_STRING = "";
	    public static final String LISTING_REGEXP = "//:.+\\*///:~";
	    public static final String SENTENCE_REGEXP = "[\\p{Upper}A-Z]((?!\\. ).)*[.?!]( |$)";

}
