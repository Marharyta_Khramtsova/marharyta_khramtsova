    The following program program  IfElseDemo, assigns a grade based on the value of a test score: an A for a score of 90% or above, a B for a score of 80% or above, and so on.

//: name.java
void applyBrakes() { 

 // the "if" clause: bicycle must be moving 

 if (isMoving){ 

 // the "then" clause: decrease current speed 

 currentSpeed--; 

 } 

}
*///:~

    
    The if-then-else statement provides a secondary path of execution when an "if" clause evaluates to false. You could use an if-then-else statement in the applyBrakes method to take some action if the brakes are applied when the bicycle is not in motion. In this case, the action is to simply print an error message stating that the bicycle has already stopped.
