package by.epam.javatraining.task1.builder;

import java.util.Set;

import by.epam.javatraining.task1.exceptions.AgeException;
import by.epam.javatraining.task1.exceptions.NameException;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.tarifs.BasicTarif;

public class PersonCreator {
	private PersonBuilder builder = new PersonBuilder();
 
	public Person createPerson(String name, int age, int count) throws AgeException, NameException {
		builder.create();
		builder.setName(name);
		builder.setAge(age);
		builder.generateTarifs(count);
		return builder.getPerson();
	}
}