package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.EasyToCommunicateTarif;

public class EasyToCommunicateTarifBuilder {
	
	private EasyToCommunicateTarif tarif;
	
	public void create() {
		tarif = new EasyToCommunicateTarif();
	}	

	public void setName(String name) {
		this.tarif.setName(name);
	}

	public void setAbonentCost(int abonentCost) {
		this.tarif.setAbonentCost(abonentCost);
	}

	public void setPriceToThisNet(int priceToThisNet) {
		this.tarif.setPriceToThisNet(priceToThisNet);
	}
	public void setPriceToOtherNets(int priceToOtherNets) {
		this.tarif.setPriceToOtherNets(priceToOtherNets);
	}

	public void setPriceForSMS(int priceForSMS) {
		this.tarif.setPriceForSMS(priceForSMS);
	}
	
	public void setFreeMinutesToAllNets(int freeMinutesToAllNets) {
		this.tarif.setFreeMinutesToAllNets(freeMinutesToAllNets);
	}
	
	public EasyToCommunicateTarif getEasyToCommunicateTarif()
	{
		return this.tarif;
	}

}
