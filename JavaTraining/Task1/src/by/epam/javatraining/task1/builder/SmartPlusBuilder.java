package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.Smart;
import by.epam.javatraining.task1.tarifs.SmartPlus;

public class SmartPlusBuilder{
	
	private SmartPlus smartPlus;
	
	public void create() {
		smartPlus = new SmartPlus();
	}
	
	public void setName(String name) {
		this.smartPlus.setName(name);
	}

	public void setAbonentCost(int abonentCost) {
		this.smartPlus.setAbonentCost(abonentCost);
	}

	public void setPriceToThisNet(int priceToThisNet) {
		this.smartPlus.setPriceToThisNet(priceToThisNet);
	}
	public void setPriceToOtherNets(int priceToOtherNets) {
		this.smartPlus.setPriceToOtherNets(priceToOtherNets);
	}

	public void setPriceForSMS(int priceForSMS) {
		this.smartPlus.setPriceForSMS(priceForSMS);
	}
	
	public void setPriceForMobileInternet(int priceForMobileInternet) {
		this.smartPlus.setPriceForMobileInternet(priceForMobileInternet);
	}
	
	public void setFreeMinutesToMTS(int freeMinutesToMTS) {
		this.smartPlus.setFreeMinutesToMTS(freeMinutesToMTS);
	}
	
	public Smart getSmartPlus()
	{
		return this.smartPlus;
	}
	
	

}
