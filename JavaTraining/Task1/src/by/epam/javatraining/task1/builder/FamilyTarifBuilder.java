package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.FamilyTarif;

public class FamilyTarifBuilder {
	
	private FamilyTarif familyTarif;
	
	public void create() {
		familyTarif = new FamilyTarif();
	}
	
	public void setName(String name) {
		this.familyTarif.setName(name);
	}

	public void setAbonentCost(int abonentCost) {
		this.familyTarif.setAbonentCost(abonentCost);
	}

	public void setPriceToThisNet(int priceToThisNet) {
		this.familyTarif.setPriceToThisNet(priceToThisNet);
	}
	public void setPriceToOtherNets(int priceToOtherNets) {
		this.familyTarif.setPriceToOtherNets(priceToOtherNets);
	}

	public void setPriceForSMS(int priceForSMS) {
		this.familyTarif.setPriceForSMS(priceForSMS);
	}
	
	public void setCountOfFavoriteNumbers(int countOfFavoriteNumbers) {
		this.familyTarif.setCountOfFavoriteNumbers(countOfFavoriteNumbers);
	}
	public void setPriceForFavoriteNumber(int priceForFavoriteNumber) {
		this.familyTarif.setPriceForFavoriteNumber(priceForFavoriteNumber);
	}
	
	public FamilyTarif getFamilyTarif()
	{
		return this.familyTarif;
	}
	
	

}
