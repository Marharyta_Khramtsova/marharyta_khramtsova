package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.operators.MTS;

public class MTSCreator {
	
	private static MTS mts;
	
	public static MTS createMTS() {
		mts = new MTS();
		SmartCreator smartCreator = new SmartCreator();
		SmartPlusCreator plusCreator = new SmartPlusCreator();
		mts.addTarif(smartCreator.createSmart("MTSSmart",
				50000, 0, 300, 150, 30000));
		mts.addTarif(plusCreator.createSmartPlus("MTSSmartPlus",
						70000, 0, 300, 120, 30000, 100));
		return mts;
	}

}
