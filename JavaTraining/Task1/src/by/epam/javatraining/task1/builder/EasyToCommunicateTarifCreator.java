package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.EasyToCommunicateTarif;

public class EasyToCommunicateTarifCreator {
	
	private EasyToCommunicateTarifBuilder builder = new EasyToCommunicateTarifBuilder();
	
	public EasyToCommunicateTarif createEasyToCommunicateTarif(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS, int freeMinutesToAllNets)
	{
		
		builder.create();
		builder.setAbonentCost(abonentCost);
		builder.setName(name);
		builder.setPriceForSMS(priceForSMS);
		builder.setPriceToOtherNets(priceToOtherNets);
		builder.setPriceToThisNet(priceToThisNet);
		builder.setFreeMinutesToAllNets(freeMinutesToAllNets);
		return builder.getEasyToCommunicateTarif();

	}

}
