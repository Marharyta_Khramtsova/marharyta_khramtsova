package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.operators.Life;

public class LifeCreator {
	
	private static Life life;
	
	public static Life createLife() {
		life = new Life();
		BuisnessTarifCreator buisnessTarifCreator = new BuisnessTarifCreator();
		life.addTarif(buisnessTarifCreator.createBuisnessTarif("Tarif for buisness",
						90000, 200, 300, 100, 100));
		EasyToCommunicateTarifCreator communicateTarifCreator = new EasyToCommunicateTarifCreator();
		life.addTarif(communicateTarifCreator.createEasyToCommunicateTarif("Tarif for easy communication",
						100000, 400, 400, 100, 500));
		return life;
	}

}
