package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.operators.Life;
import by.epam.javatraining.task1.operators.Velcom;

public class VelcomCreator {

	private static Velcom velcom;

	public static Velcom createVelcom() {
		velcom = new Velcom();
		FamilyTarifCreator familyTarifCreator = new FamilyTarifCreator();
		velcom.addTarif(familyTarifCreator.createFamilyTarif("Family tarif",
				45000, 200, 250, 100, 3, 100));
		return velcom;
	}

}
