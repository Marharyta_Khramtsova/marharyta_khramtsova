package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.FamilyTarif;

public class FamilyTarifCreator {
	
	private FamilyTarifBuilder builder = new FamilyTarifBuilder();
	
	public FamilyTarif createFamilyTarif(String name,int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS,
			int countOfFavoriteNumbers, int priceForFavoriteNumber)
	{
		
		builder.create();
		builder.setAbonentCost(abonentCost);
		builder.setName(name);		
		builder.setPriceForSMS(priceForSMS);
		builder.setPriceToOtherNets(priceToOtherNets);
		builder.setPriceToThisNet(priceToThisNet);
		builder.setCountOfFavoriteNumbers(countOfFavoriteNumbers);
		builder.setPriceForFavoriteNumber(priceForFavoriteNumber);
		return builder.getFamilyTarif();

	}

}
