package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.Smart;

public class SmartCreator {
	
	private SmartBuilder builder = new SmartBuilder();
	
	public Smart createSmart(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS, int priceForMobileInternet)
	{
		
		builder.create();
		builder.setAbonentCost(abonentCost);
		builder.setName(name);
		builder.setPriceForMobileInternet(priceForMobileInternet);
		builder.setPriceForSMS(priceForSMS);
		builder.setPriceToOtherNets(priceToOtherNets);
		builder.setPriceToThisNet(priceToThisNet);
		return builder.getSmart();

	}

}
