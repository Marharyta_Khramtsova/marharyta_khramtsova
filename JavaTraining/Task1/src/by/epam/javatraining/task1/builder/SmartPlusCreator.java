package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.Smart;

public class SmartPlusCreator{
	
	private SmartPlusBuilder plusBuilder = new SmartPlusBuilder();
	
	public Smart createSmartPlus(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS,
			int priceForMobileInternet, int freeMinutesToMTS)
	{
		
		plusBuilder.create();
		plusBuilder.setAbonentCost(abonentCost);
		plusBuilder.setName(name);
		plusBuilder.setPriceForMobileInternet(priceForMobileInternet);
		plusBuilder.setPriceForSMS(priceForSMS);
		plusBuilder.setPriceToOtherNets(priceToOtherNets);
		plusBuilder.setPriceToThisNet(priceToThisNet);
		plusBuilder.setFreeMinutesToMTS(freeMinutesToMTS);
		return plusBuilder.getSmartPlus();

	}

}
