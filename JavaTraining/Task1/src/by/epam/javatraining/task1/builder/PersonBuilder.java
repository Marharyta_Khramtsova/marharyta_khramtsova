package by.epam.javatraining.task1.builder;

import java.util.Set;
import java.util.TreeSet;

import by.epam.javatraining.task1.exceptions.AgeException;
import by.epam.javatraining.task1.exceptions.NameException;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.tarifs.BasicTarif;
import by.epam.javatraining.task1.tarifs.EasyToCommunicateTarif;

public class PersonBuilder {
	
	private Person person;	
	
	public void create() {
		person = new Person();
	}
 
	public void setName(String name) throws NameException {
		if ("".equals(name))
			throw new NameException("The person name can't be empty string!");
		this.person.setName(name);
	}
 
	public void setAge(int age) throws AgeException {
		if (age < 0)
			throw new AgeException("The person age can't by < 0!");
		this.person.setAge(age);
	}
	
	public void generateTarifs(int count) {
		SmartCreator smartCreator = new SmartCreator();
		SmartPlusCreator plusCreator = new SmartPlusCreator();
		FamilyTarifCreator familyTarifCreator = new FamilyTarifCreator();
		BuisnessTarifCreator buisnessTarifCreator = new BuisnessTarifCreator();
		EasyToCommunicateTarifCreator communicateTarifCreator = new EasyToCommunicateTarifCreator();
		BasicTarif currentTarif = new BasicTarif();
		
		Set<BasicTarif> tarifs = new TreeSet<BasicTarif>();
		for (int i = 0; i < count; i++)
		{
			int randNumber = (int)(Math.random()*5);
			switch (randNumber)
			{
			case 0:
				currentTarif = smartCreator.createSmart("MTSSmart",
						50000, 0, 300, 150, 30000);
				break;
			case 1:
				currentTarif = plusCreator.createSmartPlus("MTSSmartPlus",
						70000, 0, 300, 120, 30000, 100);
				break;
			case 2:
				currentTarif = familyTarifCreator.createFamilyTarif("Family tarif",
					45000, 200, 250, 100, 3, 100);
				break;
			case 3:
				currentTarif = buisnessTarifCreator.createBuisnessTarif("Tarif for buisness",
						80000, 200, 300, 100, 100);
				break;
			case 4:
				currentTarif = communicateTarifCreator.createEasyToCommunicateTarif("Tarif for easy communication",
						100000, 400, 400, 100, 500);
				break;
			}
			tarifs.add(currentTarif);
		}
		this.person.setTarifs(tarifs);
	}
 
	public Person getPerson() {
		return this.person;
	}


}
