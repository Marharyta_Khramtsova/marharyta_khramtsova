package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.BuisnessTarif;
import by.epam.javatraining.task1.tarifs.FamilyTarif;

public class BuisnessTarifCreator {
	
	private BuisnessTarifBuilder builder = new BuisnessTarifBuilder();
	
	public BuisnessTarif createBuisnessTarif(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS,
			int countOfInternationalMinutes)
	{
		
		builder.create();
		builder.setAbonentCost(abonentCost);
		builder.setName(name);		
		builder.setPriceForSMS(priceForSMS);
		builder.setPriceToOtherNets(priceToOtherNets);
		builder.setPriceToThisNet(priceToThisNet);
		builder.setcountOfInternationalMinutes(countOfInternationalMinutes);
		return builder.getBuisnessTarif();

	}

}
