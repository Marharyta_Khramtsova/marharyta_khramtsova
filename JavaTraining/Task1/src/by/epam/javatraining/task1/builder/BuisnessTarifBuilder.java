package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.BuisnessTarif;
import by.epam.javatraining.task1.tarifs.FamilyTarif;

public class BuisnessTarifBuilder {
	
	private BuisnessTarif buisnessTarif;
	
	public void create() {
		buisnessTarif = new BuisnessTarif();
	}
	
	public void setName(String name) {
		this.buisnessTarif.setName(name);
	}

	public void setAbonentCost(int abonentCost) {
		this.buisnessTarif.setAbonentCost(abonentCost);
	}

	public void setPriceToThisNet(int priceToThisNet) {
		this.buisnessTarif.setPriceToThisNet(priceToThisNet);
	}
	public void setPriceToOtherNets(int priceToOtherNets) {
		this.buisnessTarif.setPriceToOtherNets(priceToOtherNets);
	}

	public void setPriceForSMS(int priceForSMS) {
		this.buisnessTarif.setPriceForSMS(priceForSMS);
	}
	
	public void setcountOfInternationalMinutes(int countOfInternationalMinutes) {
		this.buisnessTarif.setcountOfInternationalMinutes(countOfInternationalMinutes);
	}
	
	
	
	public BuisnessTarif getBuisnessTarif()
	{
		return this.buisnessTarif;
	}

}
