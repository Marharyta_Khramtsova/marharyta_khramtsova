package by.epam.javatraining.task1.builder;

import by.epam.javatraining.task1.tarifs.Smart;

public class SmartBuilder {
	
	private Smart smart;
	
	public void create() {
		smart = new Smart();
	}	

	public void setName(String name) {
		this.smart.setName(name);
	}

	public void setAbonentCost(int abonentCost) {
		this.smart.setAbonentCost(abonentCost);
	}

	public void setPriceToThisNet(int priceToThisNet) {
		this.smart.setPriceToThisNet(priceToThisNet);
	}
	public void setPriceToOtherNets(int priceToOtherNets) {
		this.smart.setPriceToOtherNets(priceToOtherNets);
	}

	public void setPriceForSMS(int priceForSMS) {
		this.smart.setPriceForSMS(priceForSMS);
	}
	
	public void setPriceForMobileInternet(int priceForMobileInternet) {
		this.smart.setPriceForMobileInternet(priceForMobileInternet);
	}
	
	public Smart getSmart()
	{
		return this.smart;
	}

}
