package by.epam.javatraining.task1.exceptions;

public class AgeException extends Exception{

	public AgeException(String message) {
		super(message);		
	}
}
