package by.epam.javatraining.task1.exceptions;

public class NameException extends Exception{
	
	public NameException(String message) {
		super(message);		
	}

}
