package by.epam.javatraining.task1.tarifs;

public class EasyToCommunicateTarif extends LifeTarif {

	private int freeMinutesToAllNets;

	public int getFreeMinutesToAllNets() {
		return freeMinutesToAllNets;
	}

	public void setFreeMinutesToAllNets(int freeMinutesToAllNets) {
		this.freeMinutesToAllNets = freeMinutesToAllNets;
	}

	public EasyToCommunicateTarif() {

	}

	public EasyToCommunicateTarif(String name, int abonentCost,
			int priceToThisNet, int priceToOtherNets, int priceForSMS,
			int freeMinutesToAllNets) {
		super(name, abonentCost, priceToThisNet, priceToOtherNets, priceForSMS);
		this.freeMinutesToAllNets = freeMinutesToAllNets;
	}

	@Override
	public String toString() {
		return (super.toString() + " Free minutes to all nets = "
				+ this.freeMinutesToAllNets + '\n');

	}

}
