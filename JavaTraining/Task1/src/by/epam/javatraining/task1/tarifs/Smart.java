package by.epam.javatraining.task1.tarifs;


public class Smart extends MTSTarif{
	private int priceForMobileInternet;

	public int getPriceForMobileInternet() {
		return priceForMobileInternet;
	}

	public void setPriceForMobileInternet(int priceForMobileInternet) {
		this.priceForMobileInternet = priceForMobileInternet;
	}

	public Smart() {
				
	}

	public Smart(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS, int priceForMobileInternet) {
		super(name,  abonentCost, priceToThisNet, priceToOtherNets, priceForSMS);
		this.priceForMobileInternet = priceForMobileInternet;
	}

	@Override
	public String toString() {		
		return super.toString()+
				(" Price for mobile Internet = " + this.getPriceForMobileInternet() + '\n');
		
	}

	

}
