package by.epam.javatraining.task1.tarifs;


public class LifeTarif extends BasicTarif{
	
	public LifeTarif() {
	}

	public LifeTarif(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS) {
		super(name, abonentCost, priceToThisNet, priceToOtherNets, priceForSMS);		
	}
	

}
