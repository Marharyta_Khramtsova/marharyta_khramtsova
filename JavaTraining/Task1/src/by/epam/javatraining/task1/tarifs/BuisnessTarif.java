package by.epam.javatraining.task1.tarifs;

public class BuisnessTarif extends LifeTarif{
	
	private int countOfInternationalMinutes;

	public int getcountOfInternationalMinutes() {
		return countOfInternationalMinutes;
	}

	public void setcountOfInternationalMinutes(int countOfInternationalMinutes) {
		this.countOfInternationalMinutes = countOfInternationalMinutes;
	}
	
	
	
	public BuisnessTarif() {
		
	}

	public BuisnessTarif(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS, int countOfInternationalMinutes) {
		super(name,abonentCost, priceToThisNet, priceToOtherNets, priceForSMS);
		this.countOfInternationalMinutes = countOfInternationalMinutes;
	}

	@Override
	public String toString() {		
		return super.toString()+
				(" Count of international minutes = " + this.countOfInternationalMinutes + '\n');
		
	}

}
