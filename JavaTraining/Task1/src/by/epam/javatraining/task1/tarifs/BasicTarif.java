package by.epam.javatraining.task1.tarifs;

public class BasicTarif implements Comparable<BasicTarif>{
	
	private String name;
	
	private int abonentCost;
	
	private int priceToThisNet;
	
	private int priceToOtherNets;
	
	private int priceForSMS;

	public BasicTarif() {		
	}

	public BasicTarif(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS) {
		super();
		this.name = name;
		this.abonentCost = abonentCost;
		this.priceToThisNet = priceToThisNet;
		this.priceToOtherNets = priceToOtherNets;
		this.priceForSMS = priceForSMS;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAbonentCost() {
		return abonentCost;
	}

	public void setAbonentCost(int abonentCost) {
		this.abonentCost = abonentCost;
	}	

	public int getPriceToThisNet() {
		return priceToThisNet;
	}

	public void setPriceToThisNet(int priceToThisNet) {
		this.priceToThisNet = priceToThisNet;
	}

	public int getPriceToOtherNets() {
		return priceToOtherNets;
	}

	public void setPriceToOtherNets(int priceToOtherNets) {
		this.priceToOtherNets = priceToOtherNets;
	}

	public int getPriceForSMS() {
		return priceForSMS;
	}

	public void setPriceForSMS(int priceForSMS) {
		this.priceForSMS = priceForSMS;
	}

	
	@Override
	public String toString() {
		
		return 	"Tarif: '" + this.name +  
				"'\n Abonent cost = " + this.abonentCost + '\n'+
				" Cost for min to this net = " + this.priceToThisNet + '\n'+
				" Cost for min to other nets = " + this.priceToOtherNets + '\n'+
				" Cost for SMS = " + this.priceForSMS + '\n';
	}

	@Override
	public int compareTo(BasicTarif t) {
		return this.abonentCost-t.abonentCost;
	}
}
