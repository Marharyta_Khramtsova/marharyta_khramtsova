package by.epam.javatraining.task1.tarifs;

public class FamilyTarif extends VelcomTarif{
	
	private int countOfFavoriteNumbers;
	
	private int priceForFavoriteNumber;	
	
	public int getCountOfFavoriteNumbers() {
		return countOfFavoriteNumbers;
	}
	public void setCountOfFavoriteNumbers(int countOffavoriteNumbers) {
		this.countOfFavoriteNumbers = countOffavoriteNumbers;
	}
	public int getPriceForFavoriteNumber() {
		return priceForFavoriteNumber;
	}
	public void setPriceForFavoriteNumber(int priceForFavoriteNumber) {
		this.priceForFavoriteNumber = priceForFavoriteNumber;
	}
	public FamilyTarif() {
		
	}
	public FamilyTarif(String name,int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS,
			int countOfFavoriteNumbers, int priceForFavoriteNumber) {
		super(name, abonentCost, priceToThisNet, priceToOtherNets, priceForSMS);
		this.countOfFavoriteNumbers = countOfFavoriteNumbers;
		this.priceForFavoriteNumber = priceForFavoriteNumber;
	}
	
	@Override
	public String toString() {		
		return super.toString()+
				(" Count of favorite numbers = " + this.getCountOfFavoriteNumbers() + '\n' + 
						" Price for favorite number = " + this.getPriceForFavoriteNumber() + '\n');
		
	}

}
