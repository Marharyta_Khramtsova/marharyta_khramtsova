package by.epam.javatraining.task1.tarifs;

public class SmartPlus extends Smart{
	
	private int freeMinutesToMTS;	
	
	public int getFreeMinutesToMTS() {
		return freeMinutesToMTS;
	}
	public void setFreeMinutesToMTS(int freeMinutesToMTS) {
		this.freeMinutesToMTS = freeMinutesToMTS;
	}
	public SmartPlus() {
		
	}
	public SmartPlus(String name, int abonentCost, int priceToThisNet,
			int priceToOtherNets, int priceForSMS, int priceForMobileInternet,
			int freeMinutesToMTS) {
		super(name, abonentCost, priceToThisNet, priceToOtherNets, priceForSMS,
				priceForMobileInternet);
		this.freeMinutesToMTS = freeMinutesToMTS;
	}
	@Override
	public String toString() {
		return (super.toString() + 
				" Free minutes to MTS = " + this.getFreeMinutesToMTS() + '\n');
		
	}
}
