package by.epam.javatraining.task1.operators;


import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.tarifs.BasicTarif;


public abstract class MobileOperator implements Iterable<BasicTarif>{
	
	private Set<BasicTarif> tarifs;
	

	public MobileOperator() {
		tarifs = new TreeSet<BasicTarif>();
	}

	public MobileOperator(Set<BasicTarif> tarifs) {
		this.tarifs = tarifs;
	}

	public Set<BasicTarif> getTarifs() {
		return tarifs;
	}

	public void setTarifs(Set<BasicTarif> tarifs) {
		this.tarifs = tarifs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (BasicTarif tarif : tarifs)
		{
			builder.append(tarif.toString() + '\n' + '\n');
		}
		return builder.toString();
	}
	
	public abstract int getClientCount(Set<Person> persons);
	
	public abstract void addTarif(BasicTarif tarif);
	

	@Override
	public Iterator<BasicTarif> iterator() {
		return this.getTarifs().iterator();
	}
	

}
