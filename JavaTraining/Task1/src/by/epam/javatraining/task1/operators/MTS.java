package by.epam.javatraining.task1.operators;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import by.epam.javatraining.task1.builder.SmartCreator;
import by.epam.javatraining.task1.builder.SmartPlusCreator;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.tarifs.BasicTarif;
import by.epam.javatraining.task1.tarifs.MTSTarif;

public class MTS extends MobileOperator{
	
	
	
	public MTS() {
		
	}

	@Override
	public String toString() {
		return "Mobile operator: MTS" + '\n' + super.toString();
	}

	@Override
	public int getClientCount(Set<Person> persons) {
		int count = 0;
		for (Person person : persons)
		{
			if (person.hasOperator(OperatorEnum.MTS))
				count++;
		}
		return count;
	}

	@Override
	public void addTarif(BasicTarif tarif) {
		if (tarif instanceof MTSTarif)
		{
			Set<BasicTarif> tarifs = this.getTarifs();
			tarifs.add(tarif);
			this.setTarifs(tarifs);
		}
		
	}



	
}
