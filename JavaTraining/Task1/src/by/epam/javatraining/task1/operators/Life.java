package by.epam.javatraining.task1.operators;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import by.epam.javatraining.task1.builder.BuisnessTarifCreator;
import by.epam.javatraining.task1.builder.EasyToCommunicateTarifCreator;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.tarifs.BasicTarif;
import by.epam.javatraining.task1.tarifs.LifeTarif;
import by.epam.javatraining.task1.tarifs.MTSTarif;

public class Life extends MobileOperator{
	
	
	
	public Life() {
		
		}

	@Override
	public String toString() {
		return "Mobile operator: Life :)"+ '\n' + super.toString();
	}
	
	@Override
	public int getClientCount(Set<Person> persons) {
		int count = 0;
		for (Person person : persons)
		{
			if (person.hasOperator(OperatorEnum.LIFE))
				count++;
		}
		return count;
	}
	
	@Override
	public void addTarif(BasicTarif tarif) {
		if (tarif instanceof LifeTarif)
		{
			Set<BasicTarif> tarifs = this.getTarifs();
			tarifs.add(tarif);
			this.setTarifs(tarifs);
		}
		
	}
	

}
