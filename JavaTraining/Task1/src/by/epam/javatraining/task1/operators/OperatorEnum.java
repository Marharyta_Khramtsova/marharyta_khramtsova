package by.epam.javatraining.task1.operators;

public enum OperatorEnum {
	MTS("MTS"),
	VELCOM("Velcom"),
	LIFE("Life :)");
	
	String name;
	
	private OperatorEnum(String s) {
		this.name = s;
	}

}
