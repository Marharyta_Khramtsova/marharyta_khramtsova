package by.epam.javatraining.task1.operators;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import by.epam.javatraining.task1.builder.FamilyTarifCreator;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.tarifs.BasicTarif;
import by.epam.javatraining.task1.tarifs.MTSTarif;
import by.epam.javatraining.task1.tarifs.VelcomTarif;

public class Velcom extends MobileOperator{
	
	
	public Velcom() {
	
	}

	@Override
	public String toString() {
		return "Mobile operator: Velcom"+ '\n' + super.toString();
	}
	
	@Override
	public int getClientCount(Set<Person> persons) {
		int count = 0;
		for (Person person : persons)
		{
			if (person.hasOperator(OperatorEnum.VELCOM))
				count++;
		}
		return count;
	}
	
	@Override
	public void addTarif(BasicTarif tarif) {
		if (tarif instanceof VelcomTarif)
		{
			Set<BasicTarif> tarifs = this.getTarifs();
			tarifs.add(tarif);
			this.setTarifs(tarifs);
		}
		
	}

}
