package by.epam.javatraining.task1.controller;


import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import by.epam.javatraining.task1.builder.LifeCreator;
import by.epam.javatraining.task1.builder.MTSCreator;
import by.epam.javatraining.task1.builder.PersonCreator;
import by.epam.javatraining.task1.builder.VelcomCreator;
import by.epam.javatraining.task1.exceptions.AgeException;
import by.epam.javatraining.task1.exceptions.NameException;
import by.epam.javatraining.task1.operators.MobileOperator;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.printing.ConsolePrinter;
import by.epam.javatraining.task1.printing.FilePrinter;
import by.epam.javatraining.task1.printing.Printer;
import by.epam.javatraining.task1.service.Searcher;


public class Executer {
	
	private static Logger logger = null; 	
	

	public static Logger getLogger() {
		return logger;
	}

	public static void main(String[] args) {			
		
		logger = Logger.getLogger(Executer.class);
		 String log4jConfigFile = "E:/Epam/Task1/src/log4j.xml";
	        DOMConfigurator.configure(log4jConfigFile);
		logger.setLevel(Level.ERROR);

		
		MobileOperator mts = MTSCreator.createMTS();
		MobileOperator velcom = VelcomCreator.createVelcom();
		MobileOperator life = LifeCreator.createLife();
		Printer consolePrinter = new ConsolePrinter();
		Printer filePrinter = new FilePrinter();
		
		
		Vector<Printer> printers = new Vector<Printer>();
		printers.add(consolePrinter);
		printers.add(filePrinter);
		
		for (Printer printer : printers)
		{
			printer.print(mts);
			printer.print(velcom);
			printer.print(life);
		}
		
		
		PersonCreator creator = new PersonCreator();
		try {
			Person p1 = creator.createPerson("Peter", 10, 1);
			Person p2 = creator.createPerson("Sue", 43, 2);
			Person p3 = creator.createPerson("Tim", 15, 3);
			Set<Person> persons = new TreeSet<Person>();
			persons.add(p1);
			persons.add(p2);
			for (Printer printer : printers)
			{
				printer.printPerson(p1);
				printer.printPerson(p2);
				printer.printPerson(p3);
				
				printer.printCount(mts.getClientCount(persons), "MTS");
				printer.printCount(velcom.getClientCount(persons), "Velcom");
				printer.printCount(life.getClientCount(persons), "Life");
				
				int max = 50000;
				printer.printSearchResults(
						Searcher.searchByAbonentCost(mts, max), max, "MTS");
				printer.printSearchResults(
						Searcher.searchByAbonentCost(velcom, max), max, "Velcom");
				printer.printSearchResults(
						Searcher.searchByAbonentCost(life, max), max, "Life");
			}			
		} catch (AgeException | NameException e) {
			logger.error(e.getMessage());
			
		}
		((FilePrinter) filePrinter).close();

	}

}
