package by.epam.javatraining.task1.printing;



import by.epam.javatraining.task1.operators.MobileOperator;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.service.SearchResults;


public class ConsolePrinter implements Printer{
	

	@Override
	public void print(MobileOperator operator) {
		System.out.println(operator.toString());
		
	}

	@Override
	public void printCount(int count, String type) {
		System.out.println("The count of clients in " + type + " is " + count);
		
	}

	@Override
	public void printPerson(Person person) {
		System.out.println(person);
		
	}

	@Override
	public void printSearchResults(SearchResults tarifs, int max, String type) {
		System.out.println("Tarifs in " + type + " where abonent cost <= " + max + ": \n");
		if (tarifs.size() != 0)
			System.out.println(tarifs);
		else
			System.out.println("There is no one.");
		
	}

}
