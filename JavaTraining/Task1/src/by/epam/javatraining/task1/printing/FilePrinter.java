package by.epam.javatraining.task1.printing;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import by.epam.javatraining.task1.controller.Executer;
import by.epam.javatraining.task1.operators.MobileOperator;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.service.SearchResults;

public class FilePrinter implements Printer{
	
private PrintWriter out = null;
	
	public FilePrinter()
    {
		File file = new File("C:/output.txt");
		try
		{
		    if (!file.exists())
		    {
		    	file.createNewFile();
		    }
	
		    out = new PrintWriter(file.getAbsoluteFile());
		}
		catch (IOException e)
		{
		    Executer.getLogger().error(e.getMessage());
		}
    }

	@Override
	public void print(MobileOperator operator) {
		out.println(operator.toString());
		out.println();
		
	}

	@Override
	public void printCount(int count, String type) {
		out.println("The count of clients in " + type + " is " + count);
		out.println();
		
	}

	@Override
	public void printPerson(Person person) {
		out.println(person);
		out.println();
		
	}

	@Override
	public void printSearchResults(SearchResults tarifs, int max, String type) {
		out.println("Tarifs in " + type + " where abonent cost <= " + max + ": \n");
		if (tarifs.size() != 0)
		{
			out.println(tarifs);
			out.println();
		}
		else
		{
			out.println("There is no one.");
			out.println();
		}
		
	}
	
	public void close()
    {
		out.close();
    }

}
