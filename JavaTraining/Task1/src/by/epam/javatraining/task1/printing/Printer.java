package by.epam.javatraining.task1.printing;

import java.io.FileNotFoundException;
import java.util.Set;

import by.epam.javatraining.task1.operators.MobileOperator;
import by.epam.javatraining.task1.person.Person;
import by.epam.javatraining.task1.service.SearchResults;
import by.epam.javatraining.task1.tarifs.BasicTarif;

public interface Printer {
	
	public void print(MobileOperator operator);
	public void printCount(int count, String type);
	public void printPerson(Person person);
	public void printSearchResults(SearchResults tarifs, int max, String type);
	

}
