package by.epam.javatraining.task1.person;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import by.epam.javatraining.task1.operators.OperatorEnum;
import by.epam.javatraining.task1.tarifs.BasicTarif;
import by.epam.javatraining.task1.tarifs.LifeTarif;
import by.epam.javatraining.task1.tarifs.MTSTarif;
import by.epam.javatraining.task1.tarifs.VelcomTarif;


public class Person implements Comparable<Person>{
	
	private String name;
	
	private int age;
	
	private Set<BasicTarif> tarifs;		
	

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}

	public Set<BasicTarif> getTarifs() {
		return tarifs;
	}


	public void setTarifs(Set<BasicTarif> tarifs) {
		this.tarifs = tarifs;
	}


	public Person() {
		tarifs = new TreeSet<BasicTarif>();
	}	
	

	public Person(String name, int age, Set<BasicTarif> tarifs) {
		this.name = name;
		this.age = age;
		this.tarifs = tarifs;
	}



	@Override
	public String toString() {
		StringBuilder str =  new StringBuilder("Name: " + this.name + ". Age: " + age + '\n');
		for (BasicTarif tarif: tarifs)
			str.append(tarif.toString() + '\n');
		return str.toString();
	}

	public boolean hasOperator(OperatorEnum type)
	{
		
			switch(type)
			{
				case MTS:
				{
					for (BasicTarif tarif : tarifs)
					{
						if (tarif instanceof MTSTarif)
							return true;
					}
					break;
				}
				case VELCOM:
				{
					for (BasicTarif tarif : tarifs)
					{
						if (tarif instanceof VelcomTarif)
							return true;
					}
					break;
				}	
				case LIFE:
				{
					for (BasicTarif tarif : tarifs)
					{
						if (tarif instanceof LifeTarif)
							return true;
					}
					break;
				}	
			
			}
			return false;
		
	}


	@Override
	public int compareTo(Person p) {
		return this.name.compareTo(p.name);
	}
}
