package by.epam.javatraining.task1.service;


import by.epam.javatraining.task1.operators.MobileOperator;
import by.epam.javatraining.task1.tarifs.BasicTarif;

public class Searcher {
	
	public static SearchResults searchByAbonentCost(MobileOperator operator, int max)
	{
		SearchResults ans = new SearchResults();
		for (BasicTarif tarif : operator)
		{
			if (tarif.getAbonentCost() <= max)
				ans.add(tarif);
		}
		return ans;
	}

}
