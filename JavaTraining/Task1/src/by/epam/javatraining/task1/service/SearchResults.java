package by.epam.javatraining.task1.service;


import java.util.TreeSet;

import by.epam.javatraining.task1.tarifs.BasicTarif;

public class SearchResults extends TreeSet<BasicTarif>{

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (BasicTarif tarif : this)
		{
			builder.append(tarif.toString() + '\n');
		}
		return builder.toString();
	}
	
	

}
