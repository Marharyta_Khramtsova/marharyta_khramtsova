package by.epam.javatraining.task4;



import java.io.File;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;


public class Validator {
	
	static File schema = new File("plants.xsd");
	
	public static boolean validateXml(File xml) {
	    try {
	        SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
	                .newSchema(new StreamSource(schema))
	                .newValidator()
	                .validate(new StreamSource(xml));
	    } catch (Exception e) {
	        return false;
	    }
	    return true;
	}
}
