package by.epam.javatraining.task4;

import java.io.File;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;


public class Testing {
	
	private static final Logger LOGGER = Logger.getRootLogger();
	
	public static void main(String[] args) throws JAXBException {
		
		ResourceBundle bundle = ResourceBundle.getBundle("config");		
		checkValid(new File(bundle.getString("file")));		
		NewXMLCreator.createNewXMl(new File(bundle.getString("newFile")));
		checkValid(new File(bundle.getString("newFile")));			
	}
	public static void checkValid(File file)
	{
		if (Validator.validateXml(file))
			LOGGER.debug(file.getName() + " is Valid!");
		else
			LOGGER.debug(file.getName() + " is not valid!");
	}

}
