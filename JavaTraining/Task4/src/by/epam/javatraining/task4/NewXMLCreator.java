package by.epam.javatraining.task4;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import by.epam.javatraining.task4.Flower.Item;
import by.epam.javatraining.task4.Flower.Item.GrowingTips;
import by.epam.javatraining.task4.Flower.Item.GrowingTips.Watering;
import by.epam.javatraining.task4.Flower.Item.Image;
import by.epam.javatraining.task4.Flower.Item.VisualParameters;
import by.epam.javatraining.task4.Flower.Item.GrowingTips.Temperature;
import by.epam.javatraining.task4.Flower.Item.VisualParameters.AverageSize;

public class NewXMLCreator {
	
	private static final Logger LOGGER = Logger.getRootLogger();
	
	public static void createNewXMl(File file) throws JAXBException
	{
		JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Flower flower = (Flower)unmarshaller.unmarshal(new File("plants.xml"));
		List<Flower.Item> items = flower.getItem();
		LOGGER.debug("Original list");
		for (Flower.Item item : items)
		{
			LOGGER.debug(item.getName());
		}
		
		Flower.Item newItem = new Item();
		newItem.setId((byte)(items.size()+1));
		newItem.setName("sunflower");
		newItem.setSoil("podzolic");
		newItem.setOrigin("Central America");
		
		VisualParameters parameters = new VisualParameters();
		parameters.setStalkColor("green");
		parameters.setPetalsColor("yellow");
		AverageSize size = new AverageSize();
		size.setMeasure("cm");
		size.setValue(150);
		parameters.setAverageSize(size);
		newItem.setVisualParameters(parameters);
		
		Temperature temperature = new Temperature();
		temperature.setMeasure("degree_fahrenheit");
		temperature.setValue(100);
		GrowingTips tips = new GrowingTips();
		tips.setTemperature(temperature);
		tips.setLighting("true");
		Watering watering = new Watering();
		watering.setMeasure("l");
		watering.setFrequency("week");
		watering.setValue(5);
		tips.setWatering(watering);		
		newItem.setGrowingTips(tips);
		
		newItem.setMultiplying("cuttings");
		Image image = new Image();
		image.setType("png");
		image.setValue("sunflower.png");
		newItem.setImage(image);
		
		items.add(newItem);
		
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.marshal(flower, file);
		
		items = flower.getItem();
		LOGGER.debug("List after marshalling");
		for (Flower.Item item : items)
		{
			LOGGER.debug(item.getName());
		}
	}

}
