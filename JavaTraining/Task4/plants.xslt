<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:output method="html"/>	
	<xsl:template match="/flower">
		<link rel="stylesheet" href="Style.css"></link>

		<table border="1" style="font-size:20px;">
			<tr>
				<th>Name</th>
				<th>Soil</th>
				<th>Origin</th>
				<th>Stalk color</th>
				<th>Petals color</th>
				<th>Average size</th>
				<th>Temperature</th>
				<th>Lighting</th>
				<th>Watering</th>
			</tr>
			
			<xsl:for-each select="item">
				<tr>
					<td>
						<xsl:value-of select="name" />
					</td>
					<td style="text-align:center">
						<xsl:value-of select="soil" />
					</td>
					<td>
						<xsl:value-of select="origin" />
					</td>
					
						<xsl:for-each select="visual_parameters">
						<td> <xsl:value-of select="stalk_color"></xsl:value-of></td>
						<td> <xsl:value-of select="petals_color"></xsl:value-of></td>	
						<td> <xsl:value-of select="average_size"></xsl:value-of></td>						
						</xsl:for-each>		
						
						<xsl:for-each select="growing_tips">
						<td> <xsl:value-of select="temperature"></xsl:value-of></td>
						<td> <xsl:value-of select="lighting"></xsl:value-of></td>	
						<td> <xsl:value-of select="watering"></xsl:value-of></td>						
						</xsl:for-each>				        
				</tr>
			</xsl:for-each>
			<tr id="all">
				<td>Count of flowers:</td>
				<td colspan="9" style="text-align:right">
					<xsl:value-of select="count(item)" />
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
